'use strict';


customElements.define('compodoc-menu', class extends HTMLElement {
    constructor() {
        super();
        this.isNormalMode = this.getAttribute('mode') === 'normal';
    }

    connectedCallback() {
        this.render(this.isNormalMode);
    }

    render(isNormalMode) {
        let tp = lithtml.html(`
        <nav>
            <ul class="list">
                <li class="title">
                    <a href="index.html" data-type="index-link">Application for exam preparation</a>
                </li>

                <li class="divider"></li>
                ${ isNormalMode ? `<div id="book-search-input" role="search"><input type="text" placeholder="Type to search"></div>` : '' }
                <li class="chapter">
                    <a data-type="chapter-link" href="index.html"><span class="icon ion-ios-home"></span>Getting started</a>
                    <ul class="links">
                        <li class="link">
                            <a href="overview.html" data-type="chapter-link">
                                <span class="icon ion-ios-keypad"></span>Overview
                            </a>
                        </li>
                        <li class="link">
                            <a href="index.html" data-type="chapter-link">
                                <span class="icon ion-ios-paper"></span>README
                            </a>
                        </li>
                                <li class="link">
                                    <a href="dependencies.html" data-type="chapter-link">
                                        <span class="icon ion-ios-list"></span>Dependencies
                                    </a>
                                </li>
                    </ul>
                </li>
                    <li class="chapter modules">
                        <a data-type="chapter-link" href="modules.html">
                            <div class="menu-toggler linked" data-toggle="collapse" ${ isNormalMode ?
                                'data-target="#modules-links"' : 'data-target="#xs-modules-links"' }>
                                <span class="icon ion-ios-archive"></span>
                                <span class="link-name">Modules</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                        </a>
                        <ul class="links collapse " ${ isNormalMode ? 'id="modules-links"' : 'id="xs-modules-links"' }>
                            <li class="link">
                                <a href="modules/AccountModule.html" data-type="entity-link">AccountModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-AccountModule-ef97176fec788244455da3a3f8f10acc"' : 'data-target="#xs-components-links-module-AccountModule-ef97176fec788244455da3a3f8f10acc"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-AccountModule-ef97176fec788244455da3a3f8f10acc"' :
                                            'id="xs-components-links-module-AccountModule-ef97176fec788244455da3a3f8f10acc"' }>
                                            <li class="link">
                                                <a href="components/ChangePasswordComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ChangePasswordComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ChangeUserDetailsComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ChangeUserDetailsComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ManageAccountsComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ManageAccountsComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/AccountRoutingModule.html" data-type="entity-link">AccountRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/AppModule.html" data-type="entity-link">AppModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-AppModule-a88606c9f582e3b0b219eedd19ca8d2e"' : 'data-target="#xs-components-links-module-AppModule-a88606c9f582e3b0b219eedd19ca8d2e"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-AppModule-a88606c9f582e3b0b219eedd19ca8d2e"' :
                                            'id="xs-components-links-module-AppModule-a88606c9f582e3b0b219eedd19ca8d2e"' }>
                                            <li class="link">
                                                <a href="components/AppComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">AppComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/AppRoutingModule.html" data-type="entity-link">AppRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/AssignmentModule.html" data-type="entity-link">AssignmentModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-AssignmentModule-ce922fdd456a9474b11c3b62057ecdbc"' : 'data-target="#xs-components-links-module-AssignmentModule-ce922fdd456a9474b11c3b62057ecdbc"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-AssignmentModule-ce922fdd456a9474b11c3b62057ecdbc"' :
                                            'id="xs-components-links-module-AssignmentModule-ce922fdd456a9474b11c3b62057ecdbc"' }>
                                            <li class="link">
                                                <a href="components/AssignmentDetailsComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">AssignmentDetailsComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/AssignmentEditComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">AssignmentEditComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/AssignmentListComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">AssignmentListComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/NewAssignmentComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">NewAssignmentComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/AssignmentRoutingModule.html" data-type="entity-link">AssignmentRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/AuthModule.html" data-type="entity-link">AuthModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-AuthModule-46148326ee584a4ffc0ef8e8c28f628e"' : 'data-target="#xs-components-links-module-AuthModule-46148326ee584a4ffc0ef8e8c28f628e"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-AuthModule-46148326ee584a4ffc0ef8e8c28f628e"' :
                                            'id="xs-components-links-module-AuthModule-46148326ee584a4ffc0ef8e8c28f628e"' }>
                                            <li class="link">
                                                <a href="components/AuthComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">AuthComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ForgotPasswordComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ForgotPasswordComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/LoginComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">LoginComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/RegisterComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">RegisterComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ResetPasswordComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ResetPasswordComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/AuthRoutingModule.html" data-type="entity-link">AuthRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/CoreModule.html" data-type="entity-link">CoreModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/CourseModule.html" data-type="entity-link">CourseModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-CourseModule-042e3e393e5919437a50f178e8bb79f0"' : 'data-target="#xs-components-links-module-CourseModule-042e3e393e5919437a50f178e8bb79f0"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-CourseModule-042e3e393e5919437a50f178e8bb79f0"' :
                                            'id="xs-components-links-module-CourseModule-042e3e393e5919437a50f178e8bb79f0"' }>
                                            <li class="link">
                                                <a href="components/CourseListComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">CourseListComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/EditCourseDialogComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">EditCourseDialogComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/NewCourseComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">NewCourseComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/CourseRoutingModule.html" data-type="entity-link">CourseRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/DialogModule.html" data-type="entity-link">DialogModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-DialogModule-eda1653190f3bf5469dfa9c273e9db2e"' : 'data-target="#xs-components-links-module-DialogModule-eda1653190f3bf5469dfa9c273e9db2e"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-DialogModule-eda1653190f3bf5469dfa9c273e9db2e"' :
                                            'id="xs-components-links-module-DialogModule-eda1653190f3bf5469dfa9c273e9db2e"' }>
                                            <li class="link">
                                                <a href="components/ConfirmDialogComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ConfirmDialogComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/LoadSpinnerModule.html" data-type="entity-link">LoadSpinnerModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-LoadSpinnerModule-51f52e5a8a9c4249a000de909d1a7286"' : 'data-target="#xs-components-links-module-LoadSpinnerModule-51f52e5a8a9c4249a000de909d1a7286"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-LoadSpinnerModule-51f52e5a8a9c4249a000de909d1a7286"' :
                                            'id="xs-components-links-module-LoadSpinnerModule-51f52e5a8a9c4249a000de909d1a7286"' }>
                                            <li class="link">
                                                <a href="components/LoadSpinnerComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">LoadSpinnerComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/MainLayoutModule.html" data-type="entity-link">MainLayoutModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-MainLayoutModule-f7a594bea57ed85f875a2faf703e70c5"' : 'data-target="#xs-components-links-module-MainLayoutModule-f7a594bea57ed85f875a2faf703e70c5"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-MainLayoutModule-f7a594bea57ed85f875a2faf703e70c5"' :
                                            'id="xs-components-links-module-MainLayoutModule-f7a594bea57ed85f875a2faf703e70c5"' }>
                                            <li class="link">
                                                <a href="components/BreadcrumbNavComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">BreadcrumbNavComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/MainLayoutComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">MainLayoutComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/MenuComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">MenuComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/MenuItemComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">MenuItemComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/SidebarComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">SidebarComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/MainLayoutRoutingModule.html" data-type="entity-link">MainLayoutRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/MaterialModule.html" data-type="entity-link">MaterialModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/RoomModule.html" data-type="entity-link">RoomModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-RoomModule-0b517534f205bd6265ff3ffb09389b1e"' : 'data-target="#xs-components-links-module-RoomModule-0b517534f205bd6265ff3ffb09389b1e"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-RoomModule-0b517534f205bd6265ff3ffb09389b1e"' :
                                            'id="xs-components-links-module-RoomModule-0b517534f205bd6265ff3ffb09389b1e"' }>
                                            <li class="link">
                                                <a href="components/NewRoomComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">NewRoomComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/RoomDetailsComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">RoomDetailsComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/RoomEditComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">RoomEditComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/RoomListComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">RoomListComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/RoomRoutingModule.html" data-type="entity-link">RoomRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/SharedModule.html" data-type="entity-link">SharedModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-SharedModule-28ec7add83fa811385aa2b015de7364c"' : 'data-target="#xs-components-links-module-SharedModule-28ec7add83fa811385aa2b015de7364c"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-SharedModule-28ec7add83fa811385aa2b015de7364c"' :
                                            'id="xs-components-links-module-SharedModule-28ec7add83fa811385aa2b015de7364c"' }>
                                            <li class="link">
                                                <a href="components/DownloadOptionsDialogComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">DownloadOptionsDialogComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/GenerateAssignmentComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">GenerateAssignmentComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/RoomPlanComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">RoomPlanComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/RoomPlanLegendComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">RoomPlanLegendComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#pipes-links-module-SharedModule-28ec7add83fa811385aa2b015de7364c"' : 'data-target="#xs-pipes-links-module-SharedModule-28ec7add83fa811385aa2b015de7364c"' }>
                                            <span class="icon ion-md-add"></span>
                                            <span>Pipes</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="pipes-links-module-SharedModule-28ec7add83fa811385aa2b015de7364c"' :
                                            'id="xs-pipes-links-module-SharedModule-28ec7add83fa811385aa2b015de7364c"' }>
                                            <li class="link">
                                                <a href="pipes/FilesizePipe.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">FilesizePipe</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/TermModule.html" data-type="entity-link">TermModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-TermModule-93cee423a495121d305cd9f034e74bf3"' : 'data-target="#xs-components-links-module-TermModule-93cee423a495121d305cd9f034e74bf3"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-TermModule-93cee423a495121d305cd9f034e74bf3"' :
                                            'id="xs-components-links-module-TermModule-93cee423a495121d305cd9f034e74bf3"' }>
                                            <li class="link">
                                                <a href="components/DistributionAndInfoFormComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">DistributionAndInfoFormComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ManualPlacementFormComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ManualPlacementFormComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/NewTermComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">NewTermComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/SelectPlaceDialogComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">SelectPlaceDialogComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ShowErrorsDialogComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ShowErrorsDialogComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/StudentListFormComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">StudentListFormComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/StudentListTableComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">StudentListTableComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/TermDetailsComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">TermDetailsComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/TermEditComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">TermEditComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/TermInfoFormComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">TermInfoFormComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/TermListComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">TermListComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/TermRunInfoFormComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">TermRunInfoFormComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/TestScheduleTableComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">TestScheduleTableComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/TermRoutingModule.html" data-type="entity-link">TermRoutingModule</a>
                            </li>
                </ul>
                </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#components-links"' :
                            'data-target="#xs-components-links"' }>
                            <span class="icon ion-md-cog"></span>
                            <span>Components</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="components-links"' : 'id="xs-components-links"' }>
                            <li class="link">
                                <a href="components/GenerateAssignmentComponent.html" data-type="entity-link">GenerateAssignmentComponent</a>
                            </li>
                            <li class="link">
                                <a href="components/RoomPlanComponent.html" data-type="entity-link">RoomPlanComponent</a>
                            </li>
                            <li class="link">
                                <a href="components/RoomPlanLegendComponent.html" data-type="entity-link">RoomPlanLegendComponent</a>
                            </li>
                        </ul>
                    </li>
                        <li class="chapter">
                            <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#directives-links"' :
                                'data-target="#xs-directives-links"' }>
                                <span class="icon ion-md-code-working"></span>
                                <span>Directives</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                            <ul class="links collapse " ${ isNormalMode ? 'id="directives-links"' : 'id="xs-directives-links"' }>
                                <li class="link">
                                    <a href="directives/ClickStopPropagationDirective.html" data-type="entity-link">ClickStopPropagationDirective</a>
                                </li>
                            </ul>
                        </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#classes-links"' :
                            'data-target="#xs-classes-links"' }>
                            <span class="icon ion-ios-paper"></span>
                            <span>Classes</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="classes-links"' : 'id="xs-classes-links"' }>
                            <li class="link">
                                <a href="classes/Room.html" data-type="entity-link">Room</a>
                            </li>
                            <li class="link">
                                <a href="classes/TermRun.html" data-type="entity-link">TermRun</a>
                            </li>
                            <li class="link">
                                <a href="classes/TermRunRequest.html" data-type="entity-link">TermRunRequest</a>
                            </li>
                        </ul>
                    </li>
                        <li class="chapter">
                            <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#injectables-links"' :
                                'data-target="#xs-injectables-links"' }>
                                <span class="icon ion-md-arrow-round-down"></span>
                                <span>Injectables</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                            <ul class="links collapse " ${ isNormalMode ? 'id="injectables-links"' : 'id="xs-injectables-links"' }>
                                <li class="link">
                                    <a href="injectables/AccountService.html" data-type="entity-link">AccountService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/AssignmentService.html" data-type="entity-link">AssignmentService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/AuthService.html" data-type="entity-link">AuthService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/CourseService.html" data-type="entity-link">CourseService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/CsvParseService.html" data-type="entity-link">CsvParseService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/CustomMatPaginatorIntl.html" data-type="entity-link">CustomMatPaginatorIntl</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/CustomMatStepperIntl.html" data-type="entity-link">CustomMatStepperIntl</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/DialogService.html" data-type="entity-link">DialogService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/EditTermService.html" data-type="entity-link">EditTermService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/MenuItemsService.html" data-type="entity-link">MenuItemsService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/NewTermService.html" data-type="entity-link">NewTermService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/PasswordValidationService.html" data-type="entity-link">PasswordValidationService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/RoomService.html" data-type="entity-link">RoomService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/SnackBarService.html" data-type="entity-link">SnackBarService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/TermService.html" data-type="entity-link">TermService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/UserService.html" data-type="entity-link">UserService</a>
                                </li>
                            </ul>
                        </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#interceptors-links"' :
                            'data-target="#xs-interceptors-links"' }>
                            <span class="icon ion-ios-swap"></span>
                            <span>Interceptors</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="interceptors-links"' : 'id="xs-interceptors-links"' }>
                            <li class="link">
                                <a href="interceptors/BlobErrorInterceptor.html" data-type="entity-link">BlobErrorInterceptor</a>
                            </li>
                            <li class="link">
                                <a href="interceptors/ErrorInterceptor.html" data-type="entity-link">ErrorInterceptor</a>
                            </li>
                            <li class="link">
                                <a href="interceptors/JwtTokenInterceptor.html" data-type="entity-link">JwtTokenInterceptor</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#guards-links"' :
                            'data-target="#xs-guards-links"' }>
                            <span class="icon ion-ios-lock"></span>
                            <span>Guards</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="guards-links"' : 'id="xs-guards-links"' }>
                            <li class="link">
                                <a href="guards/AuthGuard.html" data-type="entity-link">AuthGuard</a>
                            </li>
                            <li class="link">
                                <a href="guards/TermDataGuard.html" data-type="entity-link">TermDataGuard</a>
                            </li>
                            <li class="link">
                                <a href="guards/UnsavedChangesGuard.html" data-type="entity-link">UnsavedChangesGuard</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#interfaces-links"' :
                            'data-target="#xs-interfaces-links"' }>
                            <span class="icon ion-md-information-circle-outline"></span>
                            <span>Interfaces</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? ' id="interfaces-links"' : 'id="xs-interfaces-links"' }>
                            <li class="link">
                                <a href="interfaces/AllCoursesInfoResponsePayload.html" data-type="entity-link">AllCoursesInfoResponsePayload</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/AllRoomsInfoResponsePayload.html" data-type="entity-link">AllRoomsInfoResponsePayload</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/AllTermsInfoResponsePayload.html" data-type="entity-link">AllTermsInfoResponsePayload</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/AssignmentInfo.html" data-type="entity-link">AssignmentInfo</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/BasicAssignmentInfo.html" data-type="entity-link">BasicAssignmentInfo</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/BasicCoursePayload.html" data-type="entity-link">BasicCoursePayload</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/BasicRoomPayload.html" data-type="entity-link">BasicRoomPayload</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/BasicTermPayload.html" data-type="entity-link">BasicTermPayload</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/BasicTermPayload-1.html" data-type="entity-link">BasicTermPayload</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/BasicTermRunPayload.html" data-type="entity-link">BasicTermRunPayload</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/BreadcrumbName.html" data-type="entity-link">BreadcrumbName</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/CategoryItems.html" data-type="entity-link">CategoryItems</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/CellPayload.html" data-type="entity-link">CellPayload</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/ConfirmDialogOptionsData.html" data-type="entity-link">ConfirmDialogOptionsData</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/DialogData.html" data-type="entity-link">DialogData</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/DownloadOptions.html" data-type="entity-link">DownloadOptions</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/EditAssignmentInfo.html" data-type="entity-link">EditAssignmentInfo</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/EditRoomResponsePayload.html" data-type="entity-link">EditRoomResponsePayload</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/EditTermInfoResponse.html" data-type="entity-link">EditTermInfoResponse</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/EditTermRoomPayload.html" data-type="entity-link">EditTermRoomPayload</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/ErrorPayload.html" data-type="entity-link">ErrorPayload</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/FileContent.html" data-type="entity-link">FileContent</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/FileDataPayload.html" data-type="entity-link">FileDataPayload</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/FreeRoomRequestPayload.html" data-type="entity-link">FreeRoomRequestPayload</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/FreeRoomResponsePayload.html" data-type="entity-link">FreeRoomResponsePayload</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/GeneratingFail.html" data-type="entity-link">GeneratingFail</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/LoggedUserInfo.html" data-type="entity-link">LoggedUserInfo</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/LoginResponsePayload.html" data-type="entity-link">LoginResponsePayload</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/MenuCategory.html" data-type="entity-link">MenuCategory</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/MessagePayload.html" data-type="entity-link">MessagePayload</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/NewAssignment.html" data-type="entity-link">NewAssignment</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/NewAssignmentInfo.html" data-type="entity-link">NewAssignmentInfo</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/NewRoomRequestPayload.html" data-type="entity-link">NewRoomRequestPayload</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/RoomDetailsResponsePayload.html" data-type="entity-link">RoomDetailsResponsePayload</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/RoomPlanInfoPayload.html" data-type="entity-link">RoomPlanInfoPayload</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/RoomPlanPayload.html" data-type="entity-link">RoomPlanPayload</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/SeatingPlanCell.html" data-type="entity-link">SeatingPlanCell</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/SeatingPlanPayload.html" data-type="entity-link">SeatingPlanPayload</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/TermDetailsResponsePayload.html" data-type="entity-link">TermDetailsResponsePayload</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/TermRunBasicRoomPayload.html" data-type="entity-link">TermRunBasicRoomPayload</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/TermRunDetailsPayload.html" data-type="entity-link">TermRunDetailsPayload</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/TermRunEditResponse.html" data-type="entity-link">TermRunEditResponse</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/TermRunPayload.html" data-type="entity-link">TermRunPayload</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/TermRunRequestPayload.html" data-type="entity-link">TermRunRequestPayload</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/TestSchedule.html" data-type="entity-link">TestSchedule</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/UserInfoPayload.html" data-type="entity-link">UserInfoPayload</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#miscellaneous-links"'
                            : 'data-target="#xs-miscellaneous-links"' }>
                            <span class="icon ion-ios-cube"></span>
                            <span>Miscellaneous</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="miscellaneous-links"' : 'id="xs-miscellaneous-links"' }>
                            <li class="link">
                                <a href="miscellaneous/enumerations.html" data-type="entity-link">Enums</a>
                            </li>
                            <li class="link">
                                <a href="miscellaneous/functions.html" data-type="entity-link">Functions</a>
                            </li>
                            <li class="link">
                                <a href="miscellaneous/variables.html" data-type="entity-link">Variables</a>
                            </li>
                        </ul>
                    </li>
                        <li class="chapter">
                            <a data-type="chapter-link" href="routes.html"><span class="icon ion-ios-git-branch"></span>Routes</a>
                        </li>
                    <li class="divider"></li>
                    <li class="copyright">
                        Documentation generated using <a href="https://compodoc.app/" target="_blank">
                            <img data-src="images/compodoc-vectorise.png" class="img-responsive" data-type="compodoc-logo">
                        </a>
                    </li>
            </ul>
        </nav>
        `);
        this.innerHTML = tp.strings;
    }
});