/**
 * This file is used to specify variables for production environment
 */
export const environment = {
  production: true,
  backendUrl: 'https://minerva3.fit.vutbr.cz:48700',
  jwtTokenPrefix: 'Bearer'
};
