import { BrowserModule } from '@angular/platform-browser';
import { APP_INITIALIZER, NgModule } from '@angular/core';

import { AppRoutingModule } from './app.routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CoreModule } from '@core/core.module';
import { LoadSpinnerModule } from '@shared/modules/load-spinner/load-spinner.module';
import { AuthModule } from './modules/authentication/auth.module';
import { MainLayoutModule } from './modules/main-layout/main-layout.module';
import { appInitializerProvider } from '@core/services/app.initializer';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    LoadSpinnerModule,
    AuthModule,
    MainLayoutModule,
    CoreModule,
    AppRoutingModule
  ],
  providers: [
    appInitializerProvider
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
