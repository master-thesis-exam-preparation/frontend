import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { RegisterRequestPayload } from '../../models/register.request.payload';
import { AuthService, SnackBarService } from '@core/services';
import { Router } from '@angular/router';
import { PasswordValidationService } from '@core/services/password-validation.service';

/**
 * Component for displaying the page to register new users to the application.
 */
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  constructor(private fb: FormBuilder,
              private authService: AuthService,
              private router: Router,
              private snackBarService: SnackBarService,
              private passwordValidationService: PasswordValidationService) {
    this.registerRequestPayload = new RegisterRequestPayload(
      '', '', '', '', '', ''
    );
  }

  /**
   * Gets value of the degrees before name input in the form.
   */
  get degreesBeforeName(): AbstractControl {
    return this.registerForm.get('degreesBeforeName');
  }

  /**
   * Gets value of the first name input in the form.
   */
  get firstName(): AbstractControl {
    return this.registerForm.get('firstName');
  }

  /**
   * Gets value of the last name input in the form.
   */
  get lastName(): AbstractControl {
    return this.registerForm.get('lastName');
  }

  /**
   * Gets value of the degrees behind name input in the form.
   */
  get degreesBehindName(): AbstractControl {
    return this.registerForm.get('degreesBehindName');
  }

  /**
   * Gets value of the e-mail input in the form.
   */
  get email(): AbstractControl {
    return this.registerForm.get('email');
  }

  /**
   * Gets value of the password input in the form.
   */
  get password(): AbstractControl {
    return this.registerForm.get('password');
  }

  /**
   * Gets value of the repeated password input in the form.
   */
  get confirmPassword(): AbstractControl {
    return this.registerForm.get('confirmPassword');
  }

  registerForm: FormGroup;
  registerRequestPayload: RegisterRequestPayload;
  hidePassword = true;
  hideConfirmPassword = true;

  isWaiting = false;

  /**
   * Sets validators to the form inputs.
   */
  ngOnInit(): void {
    this.registerForm = this.fb.group(
      {
        degreesBeforeName: ['', [
          Validators.maxLength(255)
        ]],
        firstName: ['', [
          Validators.required,
          Validators.maxLength(255)
        ]],
        lastName: ['', [
          Validators.required,
          Validators.maxLength(255)
        ]],
        degreesBehindName: ['', [
          Validators.maxLength(255)
        ]],
        email: ['', [
          Validators.required,
          Validators.email,
          Validators.maxLength(255),
          this.passwordValidationService.regexValidator(
            /^.*@((?:(?:stud)\.)?(?:(?:fit|feec|fce|fbm|fch|favu|fme|usi|fa)\.))?(?:vut|vutbr)\.cz$/,
            {isVutEmail: true}
          )
        ]],
        password: ['', [
          // 1. Password Field is Required
          Validators.required,
          // 2. check whether the entered password has a number
          this.passwordValidationService.regexValidator(/\d/, {hasNumber: true}),
          // 3. check whether the entered password has upper case letter
          this.passwordValidationService.regexValidator(/[A-Z]/, {hasCapitalCase: true}),
          // 4. check whether the entered password has a lower-case letter
          this.passwordValidationService.regexValidator(/[a-z]/, {hasSmallCase: true}),
          // 5. check whether the entered password has a special character
          this.passwordValidationService.regexValidator(/\W|_/, {hasSpecialCharacters: true}),
          // 6. Has a minimum length of 8 characters
          Validators.minLength(8)
        ]],
        confirmPassword: ['', [
          Validators.required
        ]],
      },
      {
        validators: [
          this.passwordValidationService.passwordMatchValidator
        ]
      }
    );
  }

  /**
   * Submit the form and send the register request.
   */
  onSubmit(): void {
    if (this.registerForm.valid) {
      this.registerRequestPayload.firstName = this.firstName.value;
      this.registerRequestPayload.lastName = this.lastName.value;
      this.registerRequestPayload.email = this.email.value;
      this.registerRequestPayload.password = this.password.value;
      this.registerRequestPayload.degreesBeforeName = this.degreesBeforeName.value;
      this.registerRequestPayload.degreesBehindName = this.degreesBehindName.value;

      this.isWaiting = true;
      this.authService.register$(this.registerRequestPayload)
        .subscribe({
          next: response => {
            this.snackBarService.openSuccessSnackBar(response);
            this.isWaiting = false;
            this.router.navigate(['/auth/login']);
          }
        });
    }
  }
}
