import { Component } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LoginRequestPayload } from '../../models/login.request.payload';
import { AuthService, SnackBarService } from '@core/services';
import { ActivatedRoute, Router } from '@angular/router';

/**
 * Component for displaying the page to log-in users to the application.
 */
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {

  loginForm: FormGroup;
  loginRequestPayload: LoginRequestPayload;
  hidePassword = true;

  constructor(private fb: FormBuilder,
              private authService: AuthService,
              private snackBarService: SnackBarService,
              private route: ActivatedRoute,
              private router: Router) {
    this.loginRequestPayload = new LoginRequestPayload('', '', false);

    this.loginForm = this.fb.group({
      email: ['', [
        Validators.required,
        Validators.email
      ]],
      password: ['', [
        Validators.required
      ]],
      rememberMe: false
    });
  }

  /**
   * Gets value of the e-mail input in the form.
   */
  get email(): AbstractControl {
    return this.loginForm.get('email');
  }

  /**
   * Gets value of the password input in the form.
   */
  get password(): AbstractControl {
    return this.loginForm.get('password');
  }

  /**
   * Gets value of the remember me checkbox in the form.
   */
  get rememberMe(): AbstractControl {
    return this.loginForm.get('rememberMe');
  }

  /**
   * Submit the form and send the login request.
   */
  onSubmit(): void {
    if (this.loginForm.valid) {
      const formValues = this.loginForm.value;
      this.loginRequestPayload.email = formValues.email;
      this.loginRequestPayload.password = formValues.password;
      this.loginRequestPayload.rememberMe = formValues.rememberMe;

      this.authService.login$(this.loginRequestPayload)
        .subscribe(
          () => {
            this.snackBarService.openSuccessSnackBar('Přihlášení proběhlo úspěšně');
            const returnUrl = this.route.snapshot.queryParamMap.get('returnUrl') || '/';
            this.router.navigate([returnUrl]);
          }
        );
    }
  }
}
