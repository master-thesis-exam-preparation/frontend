import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService, SnackBarService } from '@core/services';
import { Router } from '@angular/router';
import { ForgetPasswordRequestPayload } from '../../models/forget-password.request.payload';

/**
 * Component for displaying the page to reset forgot password.
 */
@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent implements OnInit {

  forgetPasswordForm: FormGroup;

  isWaiting = false;


  constructor(private fb: FormBuilder,
              private authService: AuthService,
              private snackBarService: SnackBarService,
              private router: Router) {
  }

  get email(): AbstractControl {
    return this.forgetPasswordForm.get('email');
  }

  /**
   * Sets validators to the form inputs.
   */
  ngOnInit(): void {
    this.forgetPasswordForm = this.fb.group({
      email: ['', [
        Validators.required,
        Validators.email
      ]]
    });
  }

  /**
   * Submit the form and send the password reset request.
   */
  onSubmit(): void {
    if (this.forgetPasswordForm.valid) {
      const formValues = this.forgetPasswordForm.value;

      this.isWaiting = true;
      this.authService.forgetPassword$(new ForgetPasswordRequestPayload(formValues.email))
        .subscribe({
          next: message => {
            this.snackBarService.openSuccessSnackBar(message);
            this.isWaiting = false;
            this.router.navigate(['/auth/login']);
          },
          error: () => {
            this.isWaiting = false;
          }
        });
    }
  }
}
