import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService, SnackBarService } from '@core/services';
import { ActivatedRoute, Router } from '@angular/router';
import { PasswordValidationService } from '@core/services/password-validation.service';
import { ResetPasswordRequestPayload } from '../../models/reset-password.request.payload';

/**
 * Component for displaying the page to set the new password.
 */
@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss']
})
export class ResetPasswordComponent implements OnInit {

  newPasswordForm: FormGroup;
  hidePassword = true;
  hideConfirmPassword = true;

  isWaiting = false;

  private resetPasswordPayload: ResetPasswordRequestPayload;


  constructor(private fb: FormBuilder,
              private authService: AuthService,
              private router: Router,
              private activatedRoute: ActivatedRoute,
              private snackBarService: SnackBarService,
              private passwordValidationService: PasswordValidationService) {
    this.resetPasswordPayload = new ResetPasswordRequestPayload();
  }

  /**
   * Gets value of the password input in the form.
   */
  get password(): AbstractControl {
    return this.newPasswordForm.get('password');
  }

  /**
   * Gets value of the repeated password input in the form.
   */
  get confirmPassword(): AbstractControl {
    return this.newPasswordForm.get('confirmPassword');
  }

  /**
   * Sets validators to the form inputs.
   */
  ngOnInit(): void {
    const paramMap = this.activatedRoute.snapshot.queryParamMap;
    if (!paramMap.has('token')) {
      this.snackBarService.openDangerSnackBar('Chybějící token');
      this.router.navigate(['/auth/login']);
    } else {
      this.resetPasswordPayload.token = paramMap.get('token');
    }

    this.newPasswordForm = this.fb.group(
      {
        password: ['', [
          // 1. Password Field is Required
          Validators.required,
          // 2. check whether the entered password has a number
          this.passwordValidationService.regexValidator(/\d/, {hasNumber: true}),
          // 3. check whether the entered password has upper case letter
          this.passwordValidationService.regexValidator(/[A-Z]/, {hasCapitalCase: true}),
          // 4. check whether the entered password has a lower-case letter
          this.passwordValidationService.regexValidator(/[a-z]/, {hasSmallCase: true}),
          // 5. check whether the entered password has a special character
          this.passwordValidationService.regexValidator(/\W|_/, {hasSpecialCharacters: true}),
          // 6. Has a minimum length of 8 characters
          Validators.minLength(8)
        ]],
        confirmPassword: ['', [
          Validators.required
        ]],
      },
      {
        validators: [
          this.passwordValidationService.passwordMatchValidator
        ]
      }
    );
  }

  /**
   * Submit the form and send the set a new password request.
   */
  onSubmit(): void {
    if (this.newPasswordForm.valid) {
      this.resetPasswordPayload.password = this.password.value;

      this.isWaiting = true;
      this.authService.resetPassword$(this.resetPasswordPayload)
        .subscribe({
          next: response => {
            this.snackBarService.openSuccessSnackBar(response);
            this.router.navigate(['/auth/login']);
            this.isWaiting = false;
          },
          error: () => {
            this.isWaiting = false;
          }
        });
    }
  }
}
