/**
 * Used to store information necessary to reset user password (token for validation and new password value).
 */
export interface ResetPasswordRequestPayload {
  token: string;
  password: string;
}

export class ResetPasswordRequestPayload {
  token: string;
  password: string;

  constructor() {
  }
}
