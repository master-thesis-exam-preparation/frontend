/**
 * Used to store information necessary to log-in user to application (e-mail address, password and remember me option value).
 */
export interface LoginRequestPayload {
  email: string;
  password: string;
  rememberMe: boolean;
}

export class LoginRequestPayload {
  email: string;
  password: string;
  rememberMe: boolean;

  constructor(email: string, password: string, rememberMe: boolean) {
    this.email = email;
    this.password = password;
    this.rememberMe = rememberMe;
  }
}
