/**
 * Used to store the email address to which the reset token should be sent.
 */
export interface ForgetPasswordRequestPayload {
  email: string;
}

export class ForgetPasswordRequestPayload {
  email: string;

  constructor(email: string) {
    this.email = email;
  }
}
