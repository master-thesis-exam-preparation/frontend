/**
 * Used to store information necessary to register user to application (e-mail address, password, name and degrees).
 */
export interface RegisterRequestPayload {
  degreesBeforeName: string;
  degreesBehindName: string;
  firstName: string;
  lastName: string;
  email: string;
  password: string;
}

export class RegisterRequestPayload {
  degreesBeforeName: string;
  degreesBehindName: string;
  firstName: string;
  lastName: string;
  email: string;
  password: string;


  constructor(degreesBeforeName: string,
              degreesBehindName: string,
              firstName: string,
              lastName: string,
              email: string,
              password: string) {
    this.degreesBeforeName = degreesBeforeName;
    this.degreesBehindName = degreesBehindName;
    this.firstName = firstName;
    this.lastName = lastName;
    this.email = email;
    this.password = password;
  }
}
