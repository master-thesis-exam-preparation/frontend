/**
 * Used to store access token value.
 */
export interface LoginResponsePayload {
  accessToken: string;
}
