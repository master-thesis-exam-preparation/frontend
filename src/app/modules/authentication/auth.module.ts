import { NgModule } from '@angular/core';
import { LoginComponent } from './compoments/login/login.component';
import { RegisterComponent } from './compoments/register/register.component';
import { ForgotPasswordComponent } from './compoments/forgot-password/forgot-password.component';
import { ResetPasswordComponent } from './compoments/reset-password/reset-password.component';
import { AuthComponent } from './auth.component';
import { AuthRoutingModule } from './auth-routing.module';
import { SharedModule } from '@shared/shared.module';
import { MatPasswordStrengthModule } from '@angular-material-extensions/password-strength';

@NgModule({
  declarations: [
    AuthComponent,
    LoginComponent,
    RegisterComponent,
    ForgotPasswordComponent,
    ResetPasswordComponent
  ],
  imports: [
    SharedModule,
    MatPasswordStrengthModule,
    AuthRoutingModule
  ]
})
export class AuthModule {
}
