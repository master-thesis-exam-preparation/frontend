import { Component } from '@angular/core';


/**
 * Main component of all authentication pages.
 */
@Component({
  selector: 'app-auth',
  templateUrl: 'auth.component.html',
  styleUrls: ['./auth.component.scss']
})
export class AuthComponent {

  constructor() {
  }

}
