import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '@core/services/guards/auth.guard';
import { Role } from '@core/role.enum';
import { UnsavedChangesGuard } from '@core/services/guards/unsaved-changes.guard';
import { NewAssignmentComponent } from './components/new-assignment/new-assignment.component';
import { AssignmentListComponent } from './components/assignment-list/assignment-list.component';
import { AssignmentEditComponent } from './components/assignment-edit/assignment-edit.component';
import { AssignmentDetailsComponent } from './components/assignment-details/assignment-details.component';


const assignmentRoutes: Routes = [
  {
    path: 'new',
    component: NewAssignmentComponent,
    canActivate: [AuthGuard],
    canDeactivate: [UnsavedChangesGuard],
    data: {
      roles: [Role.ADMIN, Role.EMPLOYEE]
    }
  },
  {
    path: ':id/edit',
    component: AssignmentEditComponent,
    canActivate: [AuthGuard],
    canDeactivate: [UnsavedChangesGuard],
    data: {
      roles: [Role.ADMIN, Role.EMPLOYEE]
    }
  },
  {
    path: ':id',
    component: AssignmentDetailsComponent,
    canActivate: [AuthGuard],
    data: {
      roles: [Role.ADMIN, Role.EMPLOYEE]
    }
  },
  {
    path: '',
    canActivate: [AuthGuard],
    data: {
      roles: [Role.ADMIN, Role.EMPLOYEE]
    },
    component: AssignmentListComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(assignmentRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class AssignmentRoutingModule {
}
