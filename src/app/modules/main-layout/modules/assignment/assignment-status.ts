export enum AssignmentStatus {
  UPLOADED = 'UPLOADED',
  LOCKED = 'LOCKED',
  GENERATED = 'GENERATED',
  EDITED = 'EDITED'
}
