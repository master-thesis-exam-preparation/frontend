import { NewAssignmentInfo } from './new-assignment-info.payload';
import { FileDataPayload } from './file-data.payload';

/**
 * Used to store basic information about assignment and list of information about assignments files.
 */
export interface EditAssignmentInfo extends NewAssignmentInfo {
  uploadedFiles: FileDataPayload[];
}
