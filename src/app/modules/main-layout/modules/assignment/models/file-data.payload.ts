/**
 * Used to store information about assignment files (type of file, name and size).
 */
export interface FileDataPayload {
  name?: string;
  type?: string;
  size?: number;
}
