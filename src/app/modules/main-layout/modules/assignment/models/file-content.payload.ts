/**
 * Used to store text content of assignment file.
 */
export interface FileContent {
  fileContent: string;
}
