/**
 * Used to store name of assignment and list of users that has access to assignment management.
 */
export interface NewAssignmentInfo {
  name: string;
  additionalAssignmentManagersEmails: string[];
}
