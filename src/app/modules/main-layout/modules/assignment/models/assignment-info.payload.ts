import { UserInfoPayload } from '@core/services/models/userInfo.payload';
import { BasicAssignmentInfo } from './basic-assignment-info';
import { FileDataPayload } from './file-data.payload';

/**
 * Used to store information about assignment (name of assignment, list of assignment files etc.).
 */
export interface AssignmentInfo extends BasicAssignmentInfo {
  files: FileDataPayload[];
  managers?: UserInfoPayload[];
}
