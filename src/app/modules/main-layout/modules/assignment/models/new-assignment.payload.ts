import { NewAssignmentInfo } from './new-assignment-info.payload';

/**
 * Used to store information about new assignment and its files.
 */
export interface NewAssignment {
  files: FormData;
  info: NewAssignmentInfo;
}
