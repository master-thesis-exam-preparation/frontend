import { AssignmentStatus } from '../assignment-status';

/**
 * Used to store name of assignment, his unique ID and his status.
 */
export interface BasicAssignmentInfo {
  assignmentId: number;
  name: string;
  status?: AssignmentStatus;
}
