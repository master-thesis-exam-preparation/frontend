import { AfterViewInit, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { BasicAssignmentInfo } from '../../models/basic-assignment-info';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { Subject } from 'rxjs';
import { DialogService } from '@core/services/dialog.service';
import { SnackBarService } from '@core/services';
import { Router } from '@angular/router';
import { ConfirmDialogOptionsData } from '@shared/modules/dialog/confirm-dialog/confirm-dialog.component';
import { AssignmentService } from '../../services/assignment.service';

/**
 * Component for displaying the page with list of all assignments.
 */
@Component({
  selector: 'app-assignment-list',
  templateUrl: './assignment-list.component.html',
  styleUrls: ['./assignment-list.component.scss']
})
export class AssignmentListComponent implements OnInit, AfterViewInit, OnDestroy {

  rowHeaderColumns: string[] = ['name', 'status', 'action'];
  assignmentsDataSource: MatTableDataSource<BasicAssignmentInfo> = new MatTableDataSource<BasicAssignmentInfo>();

  isLoading = true;

  /**
   * Sets MatSort to work even in ngIf
   * @param sort  MatSort to set
   */
  @ViewChild(MatSort, {static: false}) set sort(sort: MatSort) {
    this.assignmentsDataSource.sort = sort;
  }

  /**
   * Sets MatPaginator to work even in ngIf
   * @param paginator  MatPaginator to set
   */
  @ViewChild(MatPaginator, {static: false}) set paginator(paginator: MatPaginator) {
    this.assignmentsDataSource.paginator = paginator;
  }

  private ngUnsubscribe$ = new Subject();

  constructor(private assignmentService: AssignmentService,
              private dialogService: DialogService,
              private snackBarService: SnackBarService,
              private router: Router) {
  }

  /**
   * Get information about all assignments on component init.
   */
  ngOnInit(): void {
    this.setAssignmentsDataSource();
  }

  /**
   * Sets paginator and sort functionalities in table after component view init is done.
   */
  ngAfterViewInit(): void {
    this.assignmentsDataSource.paginator = this.paginator;
    this.assignmentsDataSource.sort = this.sort;
  }

  /**
   * Unsubscribes from everything before destroying the component.
   */
  ngOnDestroy(): void {
    this.ngUnsubscribe$.next();
    this.ngUnsubscribe$.complete();
  }

  /**
   * Takes filter value from input and apply it to list of rooms.
   * @param event  filter key press event
   */
  applyFilter(event: Event): void {
    const filterValue = (event.target as HTMLInputElement).value;
    this.assignmentsDataSource.filter = filterValue.trim().toLowerCase();

    if (this.assignmentsDataSource.paginator) {
      this.assignmentsDataSource.paginator.firstPage();
    }
  }

  showAssignmentsDetails(row: BasicAssignmentInfo): void {
    this.router.navigate(['assignments', row.assignmentId]);
  }

  /**
   * Shows confirmation dialog before deleting the assignment.
   * @param assignment  information about the assignment that should be deleted
   */
  showConfirmDialog(assignment: BasicAssignmentInfo): void {
    const options: ConfirmDialogOptionsData = {
      title: 'Odstranit šablonu zadání?',
      message: `Opravdu chcete odstranit šablonu "${assignment.name}"? Toto rozhodnutí nelze vzít zpět.`,
      cancelText: 'ZRUŠIT',
      cancelColor: 'primary',
      confirmText: 'ODSTRANIT',
      confirmColor: 'warn'
    };
    this.dialogService.openConfirmDialog$(options)
      .subscribe(confirmed => {
        if (confirmed) {
          this.deleteAssignment(assignment.assignmentId);
        }
      });
  }

  private deleteAssignment(assignmentId: number): void {
    this.assignmentService.deleteAssignment$(assignmentId)
      .subscribe(message => {
        this.assignmentsDataSource.data = this.assignmentsDataSource.data.filter(assignment => assignment.assignmentId !== assignmentId);
        this.snackBarService.openSuccessSnackBar(message);
      });
  }

  private setAssignmentsDataSource(): void {
    this.isLoading = true;
    this.assignmentService.getAllAssignmentsInfo$()
      .subscribe(allRoomsInfo => {
          this.assignmentsDataSource.data = allRoomsInfo;
          this.isLoading = false;
        }
      );
  }
}
