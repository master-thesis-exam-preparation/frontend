import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subject, throwError } from 'rxjs';
import { AssignmentInfo } from '../../models/assignment-info.payload';
import { AssignmentService } from '../../services/assignment.service';
import { ActivatedRoute, Router } from '@angular/router';
import { catchError } from 'rxjs/operators';
import { saveAs } from 'file-saver';
import { ConfirmDialogOptionsData } from '@shared/modules/dialog/confirm-dialog/confirm-dialog.component';
import { SnackBarService } from '@core/services';
import { DialogService } from '@core/services/dialog.service';

/**
 * Component for displaying the page with details of specific assignment.
 */
@Component({
  selector: 'app-assignment-details',
  templateUrl: './assignment-details.component.html',
  styleUrls: ['./assignment-details.component.scss']
})
export class AssignmentDetailsComponent implements OnInit, OnDestroy {

  assignmentDetails: AssignmentInfo;
  isLoading = true;

  selectedFileContent: string = null;
  selectedFileName: string = null;

  private ngUnsubscribe$ = new Subject();
  private assignmentId: number;

  constructor(private assignmentService: AssignmentService,
              private route: ActivatedRoute,
              private router: Router,
              private snackBarService: SnackBarService,
              private dialogService: DialogService) {
  }

  /**
   * Gets assignment id from router URL and sends request to get assignment details.
   */
  ngOnInit(): void {
    this.assignmentId = Number(this.route.snapshot.paramMap.get('id'));
    this.isLoading = true;
    this.getAssignmentDetails();
  }

  /**
   * Unsubscribes from everything before destroying the component.
   */
  ngOnDestroy(): void {
    this.ngUnsubscribe$.next();
    this.ngUnsubscribe$.complete();
  }

  /**
   * Determines whether uploaded file is TEX file and thus can be edited.
   * @param filename  name of the uploaded file (with extension)
   */
  isEditableFile(filename: string): boolean {
    const editableExtensions = ['tex', 'bib', 'cls', 'ptc', 'fd', 'bst', 'dtx', 'bbx', 'cbx', 'def', 'sty', 'ins'];
    return editableExtensions.some(extension => filename.endsWith(extension));
  }

  /**
   * Shows content of provided file in the text area.
   * @param filename  name of the file to be displayed in the text area
   */
  showContent(filename: string): void {
    this.assignmentService.getAssignmentFileContent$(this.assignmentId, filename)
      .subscribe({
        next: fileContent => {
          this.selectedFileContent = fileContent.fileContent;
          this.selectedFileName = filename;
        }
      });
  }

  /**
   * Downloads file.
   * @param filename  name of the file to be downloaded
   */
  downloadAssignmentFile(filename: string): void {
    this.assignmentService.downloadAssignmentFile$(this.assignmentId, filename)
      .subscribe({
        next: file => {
          saveAs(file, filename.replace(/^.*[\\\/]/, ''));
        }
      });
  }

  /**
   * Hides text area with file content.
   */
  hideSelectedFile(): void {
    this.selectedFileContent = null;
    this.selectedFileName = null;
  }

  /**
   * Shows confirmation dialog before deleting the assignment.
   * @param assignmentDetails  information about assignment that should be deleted.
   */
  showConfirmDialog(assignmentDetails: AssignmentInfo): void {
    const options: ConfirmDialogOptionsData = {
      title: 'Odstranit šablonu zadání?',
      message: `Opravdu chcete odstranit šablonu "${assignmentDetails.name}"? Toto rozhodnutí nelze vzít zpět.`,
      cancelText: 'ZRUŠIT',
      cancelColor: 'primary',
      confirmText: 'ODSTRANIT',
      confirmColor: 'warn'
    };
    this.dialogService.openConfirmDialog$(options)
      .subscribe(confirmed => {
        if (confirmed) {
          this.deleteAssignment();
        }
      });
  }

  private deleteAssignment(): void {
    this.assignmentService.deleteAssignment$(this.assignmentId)
      .subscribe(message => {
        this.snackBarService.openSuccessSnackBar(message);
        this.router.navigate(['/assignments']);
      });
  }

  private getAssignmentDetails(): void {
    this.assignmentService.getAssignmentDetails(this.assignmentId)
      .pipe(
        catchError(err => {
          this.router.navigate(['/assignments']);
          return throwError(err);
        })
      )
      .subscribe({
        next: assignmentDetails => {
          this.assignmentDetails = assignmentDetails;
          this.isLoading = false;
        }
      });
  }
}
