import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { AssignmentService } from '../../services/assignment.service';
import { ActivatedRoute } from '@angular/router';
import { AssignmentInfo } from '../../models/assignment-info.payload';

/**
 * Component for displaying the page with a form for editing existing assignment.
 */
@Component({
  selector: 'app-assignment-edit',
  templateUrl: './assignment-edit.component.html',
  styleUrls: ['./assignment-edit.component.scss']
})
export class AssignmentEditComponent implements OnInit {

  assignmentData$: Observable<AssignmentInfo>;

  constructor(private route: ActivatedRoute,
              private assignmentService: AssignmentService) {
  }

  /**
   * Gets assignment id from router URL and sends request to get assignment edit data.
   */
  ngOnInit(): void {
    const assignmentId = Number(this.route.snapshot.paramMap.get('id'));
    this.assignmentData$ = this.assignmentService.getAssignmentDetails(assignmentId);
  }
}
