import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AssignmentEditComponent } from './assignment-edit.component';
import { ActivatedRoute } from '@angular/router';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { SharedModule } from '@shared/shared.module';
import { NewAssignmentComponent } from '../new-assignment/new-assignment.component';
import { RouterTestingModule } from '@angular/router/testing';
import { AppModule } from '../../../../../../app.module';

describe('AssignmentEditComponent', () => {
  let component: AssignmentEditComponent;
  let fixture: ComponentFixture<AssignmentEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AssignmentEditComponent, NewAssignmentComponent],
      imports: [HttpClientTestingModule, RouterTestingModule, AppModule, SharedModule],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            snapshot: {
              paramMap: {
                get(): number {
                  return 1;
                }
              }
            }
          }
        }
      ]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AssignmentEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
