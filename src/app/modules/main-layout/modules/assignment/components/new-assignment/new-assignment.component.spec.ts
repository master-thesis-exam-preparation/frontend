import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NewAssignmentComponent } from './new-assignment.component';
import { FormBuilder } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { AppModule } from '../../../../../../app.module';
import { SharedModule } from '@shared/shared.module';

describe('NewAssignmentComponent', () => {
  let component: NewAssignmentComponent;
  let fixture: ComponentFixture<NewAssignmentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [NewAssignmentComponent],
      imports: [RouterTestingModule, HttpClientTestingModule, SharedModule, AppModule],
      providers: [FormBuilder]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NewAssignmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
