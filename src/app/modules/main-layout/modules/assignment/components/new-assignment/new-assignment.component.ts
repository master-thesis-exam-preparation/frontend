import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { AbstractControl, FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService, SnackBarService } from '@core/services';
import { UnsavedChangesGuard } from '@core/services/guards/unsaved-changes.guard';
import { AssignmentService } from '../../services/assignment.service';
import { debounceTime, map, take, takeUntil } from 'rxjs/operators';
import { CsvParseService } from '@core/services/csv-parse.service';
import { UserInfoPayload } from '@core/services/models/userInfo.payload';
import { UserService } from '@core/services/user.service';
import { AssignmentInfo } from '../../models/assignment-info.payload';
import { FileDataPayload } from '../../models/file-data.payload';
import { saveAs } from 'file-saver';

/**
 * Component for displaying the page with a form for creating new assignment.
 */
@Component({
  selector: 'app-new-assignment',
  templateUrl: './new-assignment.component.html',
  styleUrls: ['./new-assignment.component.scss']
})
export class NewAssignmentComponent implements OnInit, OnDestroy {

  /**
   * Determines whether form is for creating new assignment or editing existing one.
   */
  @Input() edit = false;

  /**
   * Information about existing assignment.
   */
  @Input() formFillData$: Observable<AssignmentInfo>;

  newAssignmentForm: FormGroup;

  oldAssignmentName: string;

  encodingList = ['UTF-8', 'Windows-1250', 'ISO-8859-2'];

  employeeList: UserInfoPayload[] = [];
  filteredEmployeeLists: UserInfoPayload[] = [];
  selectedEmployeeList: UserInfoPayload[] = [];
  isLoading = false;

  isUploading = false;

  private ngUnsubscribe$ = new Subject();
  private wasReset = false;
  previewedFile: File | FileDataPayload;
  private previewedFileIndex: number;
  private fstChange = true;
  private assignmentId: number;

  constructor(private fb: FormBuilder,
              private router: Router,
              private assignmentService: AssignmentService,
              private snackBarService: SnackBarService,
              private deactivateGuard: UnsavedChangesGuard,
              private fileService: CsvParseService,
              private userService: UserService,
              private authService: AuthService,
              private route: ActivatedRoute) {
  }

  /**
   * Gets assignmentFiles form array from the form.
   */
  get assignmentFiles(): FormArray {
    return this.newAssignmentForm.get('assignmentFiles') as FormArray;
  }

  /**
   * Gets value of the assignmentName input in the form.
   */
  get assignmentName(): AbstractControl {
    return this.newAssignmentForm.get('assignmentName');
  }

  /**
   * Gets value of the filePreview text area in the form.
   */
  get filePreview(): AbstractControl {
    return this.newAssignmentForm.get('filePreview');
  }

  /**
   * Gets value of the encoding select menu in the form.
   */
  get encoding(): AbstractControl {
    return this.newAssignmentForm.get('encoding');
  }

  /**
   * Gets values of the employees select menu in the form.
   */
  get employees(): AbstractControl {
    return this.newAssignmentForm.get('employees');
  }

  /**
   * Gets assignment file on specific index from assignmentFile form array.
   * @param index  index of the assignment file in form array
   */
  assignmentFile(index: number): AbstractControl {
    return this.assignmentFiles.controls[index].get('assignmentFile');
  }

  /**
   * Sets validators to the form inputs. Sets listeners for input changes.
   */
  ngOnInit(): void {
    if (this.edit && !this.formFillData$) {
      throw new TypeError('The input \'formFillData$\' is required');
    }

    this.loadEmployeesLists();
    this.createAssignmentForm();

    if (this.edit) {
      this.assignmentId = Number(this.route.snapshot.paramMap.get('id'));
      this.getAndSetFormData();
    }

    this.initFormValueChanges();
    this.initEncodingValueChanges();
    this.initFilePreviewValueChanges();
  }

  /**
   * Unsubscribe from everything before destroying the component.
   */
  ngOnDestroy(): void {
    this.ngUnsubscribe$.next();
    this.ngUnsubscribe$.complete();
  }

  /**
   * Adds new uploaded file to form.
   * @param file  file to upload
   */
  addNewAssignmentFile(file: File | FileDataPayload): void {
    if (this.isFile(file)) {
      for (let i = 0; i < this.assignmentFiles.length; i++) {
        if ((this.assignmentFile(i).value as File).name === file.name) {
          this.snackBarService.openDangerSnackBar(`"${file.name}" - Soubor s tímto názvem již byl jednou nahrán`);
          return;
        }
      }
      if (file.size > 10485760) {
        this.snackBarService.openDangerSnackBar(`"${file.name}" - Překročena maximální velikost souboru (max 10 MB)`);
        return;
      }
    }
    const newGroup = this.createNextNewFileForm(file);
    this.assignmentFiles.push(newGroup);
  }

  /**
   * Catches new file selected event. If this file is text file, reads in and sets its content to text area in form.
   * @param event  file selected event
   */
  onFileSelected(event: Event): void {
    const files = (event.target as HTMLInputElement).files;
    for (let i = 0; i < files.length; i++) {
      this.addNewAssignmentFile(files.item(i));
    }
    (event.target as HTMLInputElement).value = null;
  }

  /**
   * Removes assignment file from the assignmentFiles form array at specified index.
   * @param index  index on which should be assignment file deleted
   */
  removeAssignmentFile(index: number): void {
    this.assignmentFiles.removeAt(index);
    if (index === this.previewedFileIndex) {
      this.previewedFileIndex = null;
      this.previewedFile = null;
    }
  }

  /**
   * Submit the form and send the create new assignment request or edit existing assignment request.
   */
  onSubmit(): void {
    if (this.newAssignmentForm.valid) {
      if (this.assignmentFiles.length === 0) {
        this.snackBarService.openDangerSnackBar('Nebyl vložen žádný soubor');
        return;
      }

      if (this.assignmentFiles.length > 1) {
        let rootDirTexFile = false;
        for (let i = 0; i < this.assignmentFiles.length; i++) {
          if ((this.assignmentFile(i).value as File).name.endsWith('.tex')) {
            rootDirTexFile = true;
          }
        }
        if (!rootDirTexFile) {
          this.snackBarService.openDangerSnackBar('Kořenový adresář šablony zadání neobsahuje žádný .tex soubor');
          return;
        }
      }

      const assignment = new FormData();
      const uploadedFiles: FileDataPayload[] = [];
      for (let i = 0; i < this.assignmentFiles.length; i++) {
        if (this.isFile(this.assignmentFile(i).value)) {
          const file = this.assignmentFile(i).value as File;
          assignment.append('files', file, file.name);
        } else {
          uploadedFiles.push(this.assignmentFile(i).value);
        }
      }

      this.isUploading = true;

      let info;
      if (this.edit) {
        info = {
          name: this.assignmentName.value,
          additionalAssignmentManagersEmails: this.employees.value.map(employee => employee.email),
          uploadedFiles
        };
      } else {
        info = {
          name: this.assignmentName.value,
          additionalAssignmentManagersEmails: this.employees.value.map(employee => employee.email)
        };
      }
      assignment.append('info', new Blob([JSON.stringify(info)], {type: 'application/json'}));
      let obs;
      if (this.edit) {
        obs = this.assignmentService.editAssignment$(this.assignmentId, assignment);
      } else {
        obs = this.assignmentService.createNewAssignment$(assignment);
      }
      obs.subscribe({
        next: message => {
          this.snackBarService.openSuccessSnackBar(message);
          this.isUploading = false;
          this.deactivateGuard.canBeDeactivated = true;
          this.router.navigate(['/assignments']);
        },
        error: () => {
          this.isUploading = false;
        }
      });
    }
  }

  /**
   * Clears all form input values.
   */
  clearForm(): void {
    this.previewedFile = null;
    this.previewedFileIndex = null;
    this.newAssignmentForm.reset({
      encoding: this.encodingList[0]
    });
    this.assignmentFiles.clear();
    this.deactivateGuard.canBeDeactivated = true;
    this.wasReset = true;
  }

  /**
   * Determines whether uploaded file is TEX file and thus can be edited.
   * @param filename  name of the uploaded file (with extension)
   */
  isEditableFile(fileName: string): boolean {
    const editableExtensions = ['tex', 'bib', 'cls', 'ptc', 'fd', 'bst', 'dtx', 'bbx', 'cbx', 'def', 'sty', 'ins'];
    return editableExtensions.some(extension => fileName.endsWith(extension));
  }

  /**
   * Shows content of provided file in the text area.
   * @param file  file to be displayed in the text area
   * @param index  index of the file in assignmentFiles form array
   */
  showContent(file: File | FileDataPayload, index: number): void {
    let obs;
    this.previewedFile = file;
    this.previewedFileIndex = index;
    if (this.isFile(file)) {
      obs = this.fileService.readFile(file as File, this.encoding.value);
    } else {
      obs = this.assignmentService.getAssignmentFileContent$(this.assignmentId, file.name)
        .pipe(
          map(fileContent => fileContent.fileContent)
        );
    }
    obs.subscribe({
      next: fileText => {
        this.newAssignmentForm.patchValue({
          filePreview: fileText
        });
      }
    });
  }

  /**
   * Catches TAB key press in coursesCsv text area.
   * @param event  TAB key press event
   */
  onTabTextarea(event: any): void {
    this.fileService.onTab(event, this.filePreview);
  }

  /**
   * Filter employees on each key press in filter text input.
   * @param event  key press event
   */
  onKey(event: Event): void {
    const filterValue = (event.target as HTMLInputElement).value;
    this.filteredEmployeeLists = this.searchEmployee(filterValue);
  }

  compareFunction(o1: UserInfoPayload, o2: UserInfoPayload): boolean {
    return (o1.email === o2.email && o1.nameWithDegrees === o2.nameWithDegrees);
  }

  downloadAssignmentFile(file: File | FileDataPayload): void {
    if (this.isFile(file)) {
      saveAs(file, file.name);
    } else {
      this.assignmentService.downloadAssignmentFile$(1, file.name)
        .subscribe({
          next: responseFile => {
            saveAs(responseFile, file.name.replace(/^.*[\\\/]/, ''));
          }
        });
    }
  }

  isFile(file: File | FileDataPayload): boolean {
    return file instanceof File;
  }

  hideSelectedFile(): void {
    this.previewedFile = null;
    this.previewedFileIndex = null;
  }

  /**
   * Resets employees filter.
   * @param opened  true if select menu is opened
   */
  clearFilter(opened: boolean): void {
    if (!opened) {
      this.filteredEmployeeLists = this.employeeList;
    }
  }

  private loadEmployeesLists(): void {
    this.isLoading = true;
    this.userService.getAllEmployeesList$()
      .subscribe({
        next: employees => {
          this.authService.loggedUserInfo$
            .subscribe({
              next: loggedUserInfo => {
                const parsedEmployees: UserInfoPayload[] = employees
                  .filter(employee => employee.email !== loggedUserInfo.email);
                this.employeeList = parsedEmployees;
                this.filteredEmployeeLists = parsedEmployees;
                this.isLoading = false;
              }
            });
        }
      });
  }

  private createAssignmentForm(): void {
    this.newAssignmentForm = this.fb.group({
      assignmentFiles: this.fb.array([]),
      assignmentName: ['', [
        Validators.required,
        Validators.maxLength(50)
      ]],
      employees: [[]],
      filePreview: [''],
      encoding: [this.encodingList[0], [
        Validators.required
      ]]
    });
  }

  private createNextNewFileForm(file: File | FileDataPayload): FormGroup {
    return this.fb.group({
      assignmentFile: [file, []]
    });
  }

  private searchEmployee(value: string): UserInfoPayload[] {
    const filter = value.trim().toLowerCase();
    return this.employeeList.filter(employee => employee.email.toLowerCase().includes(filter) ||
      employee.nameWithDegrees.toLowerCase().includes(filter));
  }

  private initFormValueChanges(): void {
    this.newAssignmentForm.valueChanges
      .pipe(
        debounceTime(250),
        takeUntil(this.ngUnsubscribe$)
      )
      .subscribe({
        next: () => {
          if (this.wasReset || this.fstChange && this.edit) {
            this.wasReset = false;
            this.fstChange = false;
          } else {
            this.deactivateGuard.canBeDeactivated = false;
          }
        }
      });
  }

  private initEncodingValueChanges(): void {
    this.encoding.valueChanges
      .pipe(takeUntil(this.ngUnsubscribe$))
      .subscribe({
        next: () => {
          if (this.previewedFile) {
            this.showContent(this.previewedFile, this.previewedFileIndex);
          }
        }
      });
  }

  private initFilePreviewValueChanges(): void {
    this.filePreview.valueChanges
      .pipe(debounceTime(250))
      .subscribe({
        next: () => {
          const file: File | FileDataPayload = this.assignmentFile(this.previewedFileIndex).value;
          let editedFile;
          const fileOptions = {
            type: file.type
          };
          editedFile = new File([this.filePreview.value], file.name, fileOptions);
          this.assignmentFile(this.previewedFileIndex).setValue(editedFile);
        }
      });
  }

  private getAndSetFormData(): void {
    this.formFillData$
      .pipe(take(1))
      .subscribe({
        next: assignmentInfo => {
          this.oldAssignmentName = assignmentInfo.name;
          this.newAssignmentForm.patchValue({
            assignmentName: assignmentInfo.name,
            employees: assignmentInfo.managers
          });
          for (const assignmentFile of assignmentInfo.files) {
            this.addNewAssignmentFile(assignmentFile);
          }
        },
        error: () => this.router.navigate(['/rooms'])
      });
  }
}
