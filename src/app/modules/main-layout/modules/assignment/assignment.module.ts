import { NgModule } from '@angular/core';
import { SharedModule } from '@shared/shared.module';
import { AssignmentRoutingModule } from './assignment-routing.module';
import { NewAssignmentComponent } from './components/new-assignment/new-assignment.component';
import { AssignmentListComponent } from './components/assignment-list/assignment-list.component';
import { AssignmentEditComponent } from './components/assignment-edit/assignment-edit.component';
import { AssignmentDetailsComponent } from './components/assignment-details/assignment-details.component';


@NgModule({
  declarations: [
    NewAssignmentComponent,
    AssignmentListComponent,
    AssignmentEditComponent,
    AssignmentDetailsComponent
  ],
  imports: [
    SharedModule,
    AssignmentRoutingModule
  ]
})
export class AssignmentModule {
}
