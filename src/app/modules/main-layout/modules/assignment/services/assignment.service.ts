import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '@env';
import { MessagePayload } from '@core/services/models/message.payload';
import { map } from 'rxjs/operators';
import { BasicAssignmentInfo } from '../models/basic-assignment-info';
import { AssignmentInfo } from '../models/assignment-info.payload';
import { FileContent } from '../models/file-content.payload';

/**
 * Service for sending request related to assignments.
 */
@Injectable({
  providedIn: 'root'
})
export class AssignmentService {

  constructor(private http: HttpClient) {
  }

  /**
   * Sends request to create new assignment.
   * @param newAssignment  form data with information about assignment and its files
   */
  createNewAssignment$(newAssignment: FormData): Observable<string> {
    return this.http.post<MessagePayload>(`${environment.backendUrl}/internal/assignments`, newAssignment)
      .pipe(map(value => {
          return value.message;
        })
      );
  }

  /**
   * Send request to get information about all assignments.
   */
  getAllAssignmentsInfo$(): Observable<BasicAssignmentInfo[]> {
    return this.http.get<BasicAssignmentInfo[]>(`${environment.backendUrl}/internal/assignments`);
  }

  /**
   * Sends request to get detailed information about specific assignments.
   * @param assignmentId  unique ID of the assignment
   */
  getAssignmentDetails(assignmentId: number): Observable<AssignmentInfo> {
    return this.http.get<AssignmentInfo>(`${environment.backendUrl}/internal/assignments/${assignmentId}`);
  }

  /**
   * Sends request to edit existing assignment.
   * @param assignmentId  unique ID of the assignment which should be edited
   * @param assignmentModel  form data with information about assignment and its files.
   */
  editAssignment$(assignmentId: number, assignmentModel: FormData): Observable<string> {
    return this.http.put<MessagePayload>(`${environment.backendUrl}/internal/assignments/${assignmentId}`, assignmentModel)
      .pipe(
        map(message => message.message)
      );
  }

  /**
   * Sends request to get content of specified assignment file.
   * @param assignmentId  unique ID of the assignment whose file should be read
   * @param filePath  path to file
   */
  getAssignmentFileContent$(assignmentId: number, filePath: string): Observable<FileContent> {
    const params = new HttpParams().set('filePath', filePath);
    return this.http.get<FileContent>(`${environment.backendUrl}/internal/assignments/${assignmentId}/file`, {params});
  }

  /**
   * Sends request to download assignment file.
   * @param assignmentId  unique ID of the assignment whose file should be downloaded
   * @param filePath  path to file
   */
  downloadAssignmentFile$(assignmentId: number, filePath: string): Observable<Blob> {
    const params = new HttpParams().set('filePath', filePath);
    return this.http.get(`${environment.backendUrl}/internal/assignments/${assignmentId}/file/download`,
      {responseType: 'blob', params});
  }

  /**
   * Sends request to delete an assignment.
   * @param assignmentId  unique ID of assignments which should be deleted
   */
  deleteAssignment$(assignmentId: number): Observable<string> {
    return this.http.delete<MessagePayload>(`${environment.backendUrl}/internal/assignments/${assignmentId}`)
      .pipe(
        map(message => message.message)
      );
  }

  /**
   * Sends request to start generating of assignments.
   * @param termId  unique ID of term whose assignments should be generated
   * @param termRunId  unique ID of term run whose assignments should be generated
   * @param roomAbbreviation  abbreviation of the room whose assignments should be generated
   * @param testScheduleId  test schedule whose assignment should be generated
   */
  generateAssignment$(termId: number, termRunId: number, roomAbbreviation: string, testScheduleId: number): Observable<string> {
    let params = new HttpParams();
    if (termId) {
      params = params.append('termId', String(termId));
    }
    if (termRunId) {
      params = params.append('termRunId', String(termRunId));
    }
    if (roomAbbreviation) {
      params = params.append('roomAbbreviation', roomAbbreviation);
    }
    if (testScheduleId) {
      params = params.append('testScheduleId', String(testScheduleId));
    }
    return this.http.get<MessagePayload>(`${environment.backendUrl}/internal/assignments/generate`, {params})
      .pipe(
        map(message => message.message)
      );
  }

  /**
   * Sends request to cancel assignment generating.
   * @param termId  unique ID of the term whose assignment generation should be canceled.
   */
  cancelAssignmentGenerating$(termId: number): Observable<string> {
    const params = new HttpParams().append('termId', String(termId));
    return this.http.get<MessagePayload>(`${environment.backendUrl}/internal/assignments/cancel-generating`, {params})
      .pipe(
        map(message => message.message)
      );
  }
}
