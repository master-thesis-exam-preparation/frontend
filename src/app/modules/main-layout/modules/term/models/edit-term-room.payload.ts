import { SeatingPlanCell } from './seating-plan-cell.payload';

/**
 * Used to store information about room in edit term response.
 */
export interface EditTermRoomPayload {
  roomId: number;
  capacity: number;
  freeSeatIds: number[];
  freeSeatsAtTheEndOfRoom: number[];
  seatingPlan: SeatingPlanCell[][];
}
