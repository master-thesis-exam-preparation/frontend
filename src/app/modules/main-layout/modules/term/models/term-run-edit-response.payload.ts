import { BasicTermRunPayload } from './basic-term-run.payload';
import { EditTermRoomPayload } from './edit-term-room.payload';

/**
 * Used to store information to fill edit term run form.
 */
export interface TermRunEditResponse extends BasicTermRunPayload {
  rooms: EditTermRoomPayload[];
  assignments: number[];
}
