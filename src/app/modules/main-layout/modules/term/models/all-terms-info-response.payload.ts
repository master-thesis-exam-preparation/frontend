import { TermRunBasicRoomPayload } from './term-run-basic-room.payload';
import { BasicTermPayload } from './basic-term.payload';

/**
 * Used to store information about term run which will be returned as response to get information about all terms request.
 */
export interface AllTermsInfoResponsePayload extends BasicTermPayload {
  termId: number;
  managersEmails: string[];
  termRuns: TermRunBasicRoomPayload[];
}
