/**
 * Used to store information about the login and name with degrees of the logged-in user.
 */
export interface LoginNamePayload {
  login: string;
  nameAndDegrees: string;
}

export class LoginNamePayload implements LoginNamePayload {
  login: string;
  nameAndDegrees: string;


  constructor(login: string, nameAndDegrees: string) {
    this.login = login;
    this.nameAndDegrees = nameAndDegrees;
  }
}
