import { BasicTermRunPayload } from './basic-term-run.payload';
import { BasicRoomPayload } from '../../rooms/models/basic-room.payload';
import { BasicAssignmentInfo } from '../../assignment/models/basic-assignment-info';

/**
 * Used to store basic information about term run and information about selected rooms and assignments.
 */
export interface TermRunBasicRoomPayload extends BasicTermRunPayload {
  rooms: BasicRoomPayload[];
  assignments?: BasicAssignmentInfo[];
}
