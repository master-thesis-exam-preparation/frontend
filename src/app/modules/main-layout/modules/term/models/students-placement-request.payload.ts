/**
 * Used to store information about manually placed students.
 */
export interface StudentsPlacementRequestPayload {
  studentLogins: string[];
  termRunId: number;
  roomId: number;
  seatIds: number[];
}

export class StudentsPlacementRequestPayload implements StudentsPlacementRequestPayload {
  studentLogins: string[];
  termRunId: number;
  roomId: number;
  seatIds: number[];


  constructor(studentLogins: string[], termRunId: number, roomId: number, seatIds: number[]) {
    this.studentLogins = studentLogins;
    this.termRunId = termRunId;
    this.roomId = roomId;
    this.seatIds = seatIds;
  }
}
