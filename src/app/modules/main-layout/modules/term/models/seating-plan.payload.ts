import { SeatingPlanCell } from './seating-plan-cell.payload';
import { TestSchedule } from './test-schedule.payload';

/**
 * Used to store information about seating plan of the specific room plan.
 */
export interface SeatingPlanPayload {
  highlight: boolean;
  roomAbbreviation: string;
  seatingPlan: SeatingPlanCell[][];
  testSchedules?: TestSchedule[];
}
