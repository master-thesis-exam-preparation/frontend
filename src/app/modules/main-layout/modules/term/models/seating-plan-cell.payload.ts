import { CellPayload } from '../../rooms/models/cell.payload';
import { LoginNamePayload } from './login-name.payload';

/**
 * Used to store information about cell in seating plan.
 */
export interface SeatingPlanCell extends CellPayload {
  seatNumber?: number;
  student?: LoginNamePayload;
}
