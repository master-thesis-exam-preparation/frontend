import { FreeRoomResponsePayload } from '../../course/models/free-room-response.payload';
import { BasicTermRunPayload } from './basic-term-run.payload';
import { BasicAssignmentInfo } from '../../assignment/models/basic-assignment-info';

/**
 * Used to store basic information about term run and information about selected rooms and assignments.
 */
export interface TermRunPayload extends BasicTermRunPayload {
  rooms: FreeRoomResponsePayload[];
  assignments: BasicAssignmentInfo[];
  termRunGroupId: number;
}

/**
 * Used to store basic information about term run and information about selected rooms and assignments.
 */
export class TermRun implements TermRunPayload {
  termRunGroupId: number;
  id: number;
  startTime: number;
  endTime: number;
  rooms: FreeRoomResponsePayload[];
  assignments: BasicAssignmentInfo[];

  constructor() {
  }
}
