import { BasicAssignmentInfo } from '../../assignment/models/basic-assignment-info';

/**
 * Used to store information about where who sits, what is their assignment etc.
 */
export interface TestSchedule {
  login: string;
  nameAndDegrees: string;
  testScheduleId: number;
  seatNumber: number;
  orderNumber: number;
  assignment?: BasicAssignmentInfo;
  generated: boolean;
}
