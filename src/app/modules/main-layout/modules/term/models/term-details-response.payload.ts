import { BasicTermPayload } from './basic-term.payload';
import { UserInfoPayload } from '@core/services/models/userInfo.payload';
import { TermRunDetailsPayload } from './term-run-details.payload';
import { GeneratingFail } from './generating-fail.payload';

/**
 * Used to store more specific information about term.
 */
export interface TermDetailsResponsePayload extends BasicTermPayload {
  description: string;
  managers?: UserInfoPayload[];
  remainingGenerating?: number;
  generatingFails?: GeneratingFail[];
  termRuns: TermRunDetailsPayload[];
}
