/**
 * Used to store basic information about terms term run.
 */
export interface BasicTermRunPayload {
  id: number;
  startTime: number;
  endTime: number;
}
