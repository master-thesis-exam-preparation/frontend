import { BasicTermRunPayload } from './basic-term-run.payload';

/**
 * Used to store basic information about term run and information about selected rooms and assignments.
 */
export interface TermRunRequestPayload extends BasicTermRunPayload {
  rooms: number[];
  assignments: number[];
}

/**
 * Used to store basic information about term run and information about selected rooms and assignments.
 */
export class TermRunRequest implements TermRunRequestPayload {
  id: number;
  startTime: number;
  endTime: number;
  rooms: number[];
  assignments: number[];

  constructor(id: number, startTime: number, endTime: number, rooms: number[], assignments: number[]) {
    this.id = id;
    this.startTime = startTime;
    this.endTime = endTime;
    this.rooms = rooms;
    this.assignments = assignments;
  }
}
