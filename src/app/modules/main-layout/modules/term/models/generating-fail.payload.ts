import { TestSchedule } from './test-schedule.payload';

/**
 * Used to store information about error that occurs during assignment generating.
 */
export interface GeneratingFail {
  testSchedule: TestSchedule;
  errorCode: number;
  errorMessage: string;
  hasLogFile: boolean;
}
