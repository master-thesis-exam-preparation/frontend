import { LoginNamePayload } from '../login-name.payload';
import { UserInfoPayload } from '@core/services/models/userInfo.payload';
import { BasicTermPayload } from './basic-term.payload';
import { TermRunEditResponse } from '../term-run-edit-response.payload';

/**
 * Used to store information to fill edit term forms.
 */
export interface EditTermInfoResponse extends BasicTermPayload {
  studentsList: LoginNamePayload[];

  managers: UserInfoPayload[];

  termRuns: TermRunEditResponse[];
}
