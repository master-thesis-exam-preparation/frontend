import { SpacingBetweenStudents } from '../../spacing-between-students.enum';
import { StudentsDistribution } from '../../students-distribution.enum';
import { StudentsPlacementRequestPayload } from '../students-placement-request.payload';

/**
 * Used to store basic information about term (name, selected spacing between students, distribution of students etc.).
 */
export interface BasicTermPayload {
  termName: string;
  courseId: number;

  spacingBetweenStudents: SpacingBetweenStudents;

  studentsPlacements: StudentsPlacementRequestPayload[];

  distribution: StudentsDistribution;
  informationText: string;
}
