import { LoginNamePayload } from '../login-name.payload';
import { SpacingBetweenStudents } from '../../spacing-between-students.enum';
import { StudentsDistribution } from '../../students-distribution.enum';
import { TermRunRequest } from '../term-run.request';
import { StudentsPlacementRequestPayload } from '../students-placement-request.payload';
import { NewTermRequestPayload } from './new-term-request.payload';

/**
 * Used to store basic information about term and other data necessary to edit existing term.
 */
export interface EditTermRequestPayload extends NewTermRequestPayload {
  studentsList: LoginNamePayload[];
  oldStudentsList: LoginNamePayload[];

  addStudentsAtTheEnd: boolean;

  additionalTermManagersEmails: string[];

  sendEmail: boolean;
  emailMessage: string;
}

export class EditTermRequestPayload implements EditTermRequestPayload {
  termName: string;
  courseId: number;

  studentsList: LoginNamePayload[];
  oldStudentsList: LoginNamePayload[];

  addStudentsAtTheEnd: boolean;

  spacingBetweenStudents: SpacingBetweenStudents;
  termRuns: TermRunRequest[];

  studentsPlacements: StudentsPlacementRequestPayload[];

  distribution: StudentsDistribution;
  additionalTermManagersEmails: string[];
  informationText: string;
  sendEmail: boolean;
  emailMessage: string;


  constructor(termName: string, courseId: number, studentsList: LoginNamePayload[], oldStudentsList: LoginNamePayload[],
              addStudentsAtTheEnd: boolean, spacingBetweenStudents: SpacingBetweenStudents, termRuns: TermRunRequest[],
              studentsPlacements: StudentsPlacementRequestPayload[], distribution: StudentsDistribution,
              additionalTermManagersEmails: string[], informationText: string, sendEmail: boolean, emailMessage: string) {
    this.termName = termName;
    this.courseId = courseId;
    this.studentsList = studentsList;
    this.oldStudentsList = oldStudentsList;
    this.addStudentsAtTheEnd = addStudentsAtTheEnd;
    this.spacingBetweenStudents = spacingBetweenStudents;
    this.termRuns = termRuns;
    this.studentsPlacements = studentsPlacements;
    this.distribution = distribution;
    this.additionalTermManagersEmails = additionalTermManagersEmails;
    this.informationText = informationText;
    this.sendEmail = sendEmail;
    this.emailMessage = emailMessage;
  }
}
