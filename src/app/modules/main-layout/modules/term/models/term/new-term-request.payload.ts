import { LoginNamePayload } from '../login-name.payload';
import { SpacingBetweenStudents } from '../../spacing-between-students.enum';
import { StudentsDistribution } from '../../students-distribution.enum';
import { TermRunRequest } from '../term-run.request';
import { StudentsPlacementRequestPayload } from '../students-placement-request.payload';
import { BasicTermPayload } from './basic-term.payload';


/**
 * Used to store basic information about term and other data necessary to create new term.
 */
export interface NewTermRequestPayload extends BasicTermPayload {
  studentsList: LoginNamePayload[];

  termRuns: TermRunRequest[];

  additionalTermManagersEmails: string[];
  sendEmail: boolean;
  emailMessage: string;
}

export class NewTermRequestPayload implements NewTermRequestPayload {
  termName: string;
  courseId: number;

  studentsList: LoginNamePayload[];

  spacingBetweenStudents: SpacingBetweenStudents;
  termRuns: TermRunRequest[];

  studentsPlacements: StudentsPlacementRequestPayload[];

  distribution: StudentsDistribution;
  additionalTermManagersEmails: string[];
  informationText: string;
  sendEmail: boolean;
  emailMessage: string;


  constructor(termName: string, courseId: number, studentsList: LoginNamePayload[], spacingBetweenStudents: SpacingBetweenStudents,
              termRuns: TermRunRequest[], studentsPlacements: StudentsPlacementRequestPayload[], distribution: StudentsDistribution,
              additionalTermManagersEmails: string[], informationText: string, sendEmail: boolean, emailMessage: string) {
    this.termName = termName;
    this.courseId = courseId;
    this.studentsList = studentsList;
    this.spacingBetweenStudents = spacingBetweenStudents;
    this.termRuns = termRuns;
    this.studentsPlacements = studentsPlacements;
    this.distribution = distribution;
    this.additionalTermManagersEmails = additionalTermManagersEmails;
    this.informationText = informationText;
    this.sendEmail = sendEmail;
    this.emailMessage = emailMessage;
  }
}
