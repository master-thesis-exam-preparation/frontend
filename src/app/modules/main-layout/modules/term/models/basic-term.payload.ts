/**
 * Used to store basic information about term.
 */
export interface BasicTermPayload {
  courseAbbreviation: string;
  termName: string;
  academicYear: number;
  termDates: number[];
}
