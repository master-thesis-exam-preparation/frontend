import { LoginNamePayload } from './login-name.payload';
import { TermRunPayload } from './term-run.payload';
import { FreeRoomResponsePayload } from '../../course/models/free-room-response.payload';
import { CellPayload } from '../../rooms/models/cell.payload';

/**
 * Used to store information about manually placed students.
 */
export interface StudentsPlacement {
  students: LoginNamePayload[];
  termRun: TermRunPayload;
  room: FreeRoomResponsePayload;
  seats: CellPayload[];
}

export class StudentsPlacement implements StudentsPlacement {
  students: LoginNamePayload[];
  termRun: TermRunPayload;
  room: FreeRoomResponsePayload;
  seats: CellPayload[];

  constructor(students: LoginNamePayload[], termRun: TermRunPayload, room: FreeRoomResponsePayload, seats: CellPayload[]) {
    this.students = students;
    this.termRun = termRun;
    this.room = room;
    this.seats = seats;
  }
}
