import { BasicTermRunPayload } from './basic-term-run.payload';
import { SeatingPlanPayload } from './seating-plan.payload';
import { BasicAssignmentInfo } from '../../assignment/models/basic-assignment-info';

/**
 * Used to store more specific information about term run.
 */
export interface TermRunDetailsPayload extends BasicTermRunPayload {
  seatingPlans: SeatingPlanPayload[];
  assignments?: BasicAssignmentInfo[];
  highlight: boolean;
}
