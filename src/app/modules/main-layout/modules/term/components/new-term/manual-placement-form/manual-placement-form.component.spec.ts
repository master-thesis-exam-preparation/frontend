import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ManualPlacementFormComponent } from './manual-placement-form.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { SharedModule } from '@shared/shared.module';
import { SelectPlaceDialogComponent } from './select-place-dialog/select-place-dialog.component';

describe('ManualPlacementFormComponent', () => {
  let component: ManualPlacementFormComponent;
  let fixture: ComponentFixture<ManualPlacementFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ManualPlacementFormComponent, SelectPlaceDialogComponent],
      imports: [HttpClientTestingModule, SharedModule],
      providers: [FormBuilder]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ManualPlacementFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
