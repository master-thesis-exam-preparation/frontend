import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CourseService } from '../../../../course/services/course.service';
import { AllCoursesInfoResponsePayload } from '../../../../course/models/all-courses-info-response.payload';
import { NewTermService } from '../../../services/new-term.service';

/**
 * Component for displaying the page with a step of a new term form for specifying new term name and course from which the exam takes place.
 */
@Component({
  selector: 'app-term-info-form',
  templateUrl: './term-info-form.component.html',
  styleUrls: ['./term-info-form.component.scss']
})
export class TermInfoFormComponent implements OnInit {

  termInfoForm: FormGroup;

  courses: AllCoursesInfoResponsePayload[];
  isLoading: boolean;

  filteredCourses: AllCoursesInfoResponsePayload[];

  constructor(private newTermService: NewTermService,
              private fb: FormBuilder,
              private courseService: CourseService) {
  }

  /**
   * Gets value of the termName input in the form.
   */
  get termName(): AbstractControl {
    return this.termInfoForm.get('termName');
  }

  /**
   * Gets value of the course select menu in the form.
   */
  get course(): AbstractControl {
    return this.termInfoForm.get('course');
  }

  /**
   * Sets validators to the form inputs. Gets list of courses.
   */
  ngOnInit(): void {
    this.isLoading = true;
    this.courseService.getCurrentAcademicYearCoursesInfo$()
      .subscribe({
        next: courses => {
          this.courses = courses;
          this.filteredCourses = courses;
          this.isLoading = false;
        }
      });

    this.termInfoForm = this.fb.group({
      termName: ['', [
        Validators.required,
        Validators.maxLength(255)
      ]],
      course: ['', [
        Validators.required
      ]]
    });
  }

  /**
   * Filter courses on each key press in filter text input.
   * @param event  key press event
   */
  onKey(event: Event): void {
    const filterValue = (event.target as HTMLInputElement).value;
    this.filteredCourses = this.search(filterValue);
  }

  /**
   * Resets courses filter.
   * @param opened  true if select menu is opened
   */
  clearFilter(opened: boolean): void {
    if (!opened) {
      this.filteredCourses = this.courses;
    }
  }

  private search(value: string): AllCoursesInfoResponsePayload[] {
    const filter = value.trim().toLowerCase();
    return this.courses.filter(course => course.name.toLowerCase().startsWith(filter) ||
      course.abbreviation.toLowerCase().startsWith(filter));
  }
}
