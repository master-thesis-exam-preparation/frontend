import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { LoginNamePayload } from '../../../../models/login-name.payload';
import { CellPayload } from '../../../../../rooms/models/cell.payload';
import { SeatingPlanCell } from '../../../../models/seating-plan-cell.payload';

/**
 * Used to store information for select place dialog.
 */
interface DialogData {
  studentsList: LoginNamePayload[];
  roomPlanCells: SeatingPlanCell[][];
  selectedSeats: SeatingPlanCell[];
  edit: boolean;
}

/**
 * Component for displaying the select place in room plan popup window.
 */
@Component({
  selector: 'app-select-place-dialog',
  templateUrl: './select-place-dialog.component.html',
  styleUrls: ['./select-place-dialog.component.scss']
})
export class SelectPlaceDialogComponent implements OnInit {

  oldSelectedSeats: CellPayload[] = [];
  newSelectedSeats: CellPayload[] = [];

  roomPlanCells: CellPayload[][] = [[]];
  remainingStudents: number;
  clickFunctionEnabled = true;

  constructor(@Inject(MAT_DIALOG_DATA) public data: DialogData,
              private dialogRef: MatDialogRef<SelectPlaceDialogComponent>) {
  }

  /**
   * Sets dialog data on component init.
   */
  ngOnInit(): void {
    this.remainingStudents = this.data.studentsList.length;
    this.oldSelectedSeats = this.data.selectedSeats.slice();
    this.newSelectedSeats = this.data.selectedSeats.slice();
    this.setNumOfRemainingStudents();
  }

  /**
   * Closes dialog after submit.
   */
  onSubmit(): void {
    this.dialogRef.close(this.newSelectedSeats);
  }

  /**
   * Saves selected cells and calculates number of remaining students at each change.
   * @param selectedCells  list of selected cells
   */
  onSelectedSeatsChange(selectedCells: CellPayload[]): void {
    this.newSelectedSeats = selectedCells;
    this.setNumOfRemainingStudents();
  }

  private setNumOfRemainingStudents(): void {
    this.remainingStudents = this.data.studentsList.length - this.newSelectedSeats.length;
    this.clickFunctionEnabled = this.remainingStudents > 0;
  }
}
