import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { SnackBarService } from '@core/services';
import { Subject } from 'rxjs';
import { debounceTime, takeUntil } from 'rxjs/operators';
import { LoginNamePayload } from '../../../models/login-name.payload';
import { CsvParseService } from '@core/services/csv-parse.service';
import { NewTermService } from '../../../services/new-term.service';
import { EditTermService } from '../../../services/edit-term.service';
import { StudentsPlacement } from '../../../models/students-placement';

/**
 * Component for displaying the page with a step of a new term form for uploading students from input file.
 */
@Component({
  selector: 'app-student-list-form',
  templateUrl: './student-list-form.component.html',
  styleUrls: ['./student-list-form.component.scss']
})
export class StudentListFormComponent implements OnInit, OnDestroy {

  /**
   * Determines whether form is for specifying new student list or editing an existing one.
   */
  @Input() edit = false;

  /**
   * Outputs an event every time the student list value change.
   */
  @Output() studentPlacementsChange = new EventEmitter<{ placement: StudentsPlacement, index: number }>();

  constructor(private newTermService: NewTermService,
              private editTermService: EditTermService,
              private fb: FormBuilder,
              private snackBarService: SnackBarService,
              private csvParseService: CsvParseService) {
  }

  /**
   * Gets value of the studentCsv text area in the form.
   */
  get studentCsv(): AbstractControl {
    return this.studentListForm.get('studentCsv');
  }

  /**
   * Gets value of the encoding select menu in the form.
   */
  get encoding(): AbstractControl {
    return this.studentListForm.get('encoding');
  }

  /**
   * Gets value of the separator input in the form.
   */
  get separator(): AbstractControl {
    return this.studentListForm.get('separator');
  }

  /**
   * Gets same column subform from the form.
   */
  get sameColumn(): AbstractControl {
    return this.studentListForm.get('sameColumn');
  }

  /**
   * Gets value of the loginPosition input in the form.
   */
  get loginPosition(): AbstractControl {
    return this.sameColumn.get('loginPosition');
  }

  /**
   * Gets value of the nameAndDegreesPosition input in the form.
   */
  get nameAndDegreesPosition(): AbstractControl {
    return this.sameColumn.get('nameAndDegreesPosition');
  }

  /**
   * Gets separate column subform from the form.
   */
  get separateColumns(): AbstractControl {
    return this.studentListForm.get('separateColumns');
  }

  /**
   * Gets value of the namePositionSep input in the form.
   */
  get namePositionSep(): AbstractControl {
    return this.separateColumns.get('namePositionSep');
  }

  /**
   * Gets value of the surnamePositionSep input in the form.
   */
  get surnamePositionSep(): AbstractControl {
    return this.separateColumns.get('surnamePositionSep');
  }

  /**
   * Gets value of the loginPositionSep input in the form.
   */
  get loginPositionSep(): AbstractControl {
    return this.separateColumns.get('loginPositionSep');
  }

  studentListForm: FormGroup;
  acceptedFileTypes = ['text/csv', 'application/csv', 'text/comma-separated-values', '.csv'];
  encodingList = ['Windows-1250', 'ISO-8859-2', 'UTF-8'];
  isLoginAndNameInSameColumn = true;

  csvFile: File;
  csvLines: string[] = [];

  showPreview = false;
  addMoreStudents = false;
  addStudentsAtTheEnd = true;

  studentsList: LoginNamePayload[];
  existingStudentList: LoginNamePayload[] = [];

  private ngUnsubscribe$ = new Subject();

  private static sameColumnsNumberValidator(formGroup: AbstractControl): ValidationErrors | null {
    const loginPosition: AbstractControl = formGroup.get('loginPosition');
    const nameAndDegreesPosition: AbstractControl = formGroup.get('nameAndDegreesPosition');

    return CsvParseService.sameNumberValidatorTwo(loginPosition, nameAndDegreesPosition);
  }

  private static separateColumnsNumberValidator(formGroup: AbstractControl): ValidationErrors | null {
    const loginPosition: AbstractControl = formGroup.get('loginPositionSep');
    const namePosition: AbstractControl = formGroup.get('namePositionSep');
    const surnamePosition: AbstractControl = formGroup.get('surnamePositionSep');

    return CsvParseService.sameNumberValidatorThree(loginPosition, namePosition, surnamePosition);
  }

  /**
   * Sets validators to the form inputs. Sets listeners for input changes.
   */
  ngOnInit(): void {
    this.studentListForm = this.fb.group({
      studentCsv: ['', [
        Validators.required
      ]],
      encoding: ['Windows-1250', [
        Validators.required
      ]],
      separator: ['', [
        // this.allowWhitespaceValidator,
        Validators.required,
        Validators.maxLength(20)
      ]],
      sameColumn: this.fb.group({
        loginPosition: ['', [
          Validators.required,
          Validators.min(1),
          Validators.max(1000)
        ]],
        nameAndDegreesPosition: ['', [
          Validators.required,
          Validators.min(1),
          Validators.max(1000)
        ]],
      }, {
        validators: [
          StudentListFormComponent.sameColumnsNumberValidator
        ]
      }),
      separateColumns: this.fb.group({
        namePositionSep: ['', [
          Validators.required,
          Validators.min(1),
          Validators.max(1000)
        ]],
        surnamePositionSep: ['', [
          Validators.required,
          Validators.min(1),
          Validators.max(1000)
        ]],
        loginPositionSep: ['', [
          Validators.required,
          Validators.min(1),
          Validators.max(1000)
        ]]
      }, {
        validators: [
          StudentListFormComponent.separateColumnsNumberValidator
        ]
      }),
    });

    this.separateColumns.disable();

    this.initFormValueChanges();
    this.initEncodingValueChanges();
    this.initStudentCsvValueChanges();

    if (this.edit) {
      this.editTermService.getExistingStudentList$()
        .pipe(takeUntil(this.ngUnsubscribe$))
        .subscribe({
          next: studentList => {
            this.existingStudentList = studentList;
          }
        });
    }
  }

  /**
   * Unsubscribes from everything before destroying the component.
   */
  ngOnDestroy(): void {
    this.ngUnsubscribe$.next();
    this.ngUnsubscribe$.complete();
  }

  /**
   * Catches new file selected event. If this file is text file, reads in and sets its content to text area in form.
   * @param event  file selected event
   */
  onFileSelected(event: Event): void {
    const file: File = (event.target as HTMLInputElement).files[0];
    if (file) {
      this.csvFile = file;
      this.readFile();
    }
    (event.target as HTMLInputElement).value = null;
  }

  /**
   * Switches subform for login and name in same column for subform for login and name in separate columns.
   */
  onIsLoginAndNameInSameColumnChanges(): void {
    this.isLoginAndNameInSameColumn = !this.isLoginAndNameInSameColumn;
    if (this.isLoginAndNameInSameColumn) {
      this.sameColumn.enable();
      this.separateColumns.disable();
    } else {
      this.separateColumns.enable();
      this.sameColumn.disable();
    }
  }

  /**
   * Catches TAB key press in separator input.
   * @param event  TAB key press event
   */
  onTabSeparator(event: any): void {
    this.csvParseService.onTab(event, this.separator);
  }

  /**
   * Catches TAB key press in coursesCsv text area.
   * @param event  TAB key press event
   */
  onTabTextarea(event: any): void {
    this.csvParseService.onTab(event, this.studentCsv);
  }

  /**
   * Displays form for adding new students to existing term.
   */
  onAddMoreStudentsCheckBoxChanges(): void {
    this.addMoreStudents = !this.addMoreStudents;
    if (this.addMoreStudents) {
      this.studentListForm.updateValueAndValidity();
      this.newTermService.setStudentList(this.newTermService.studentListValue);
    } else {
      this.newTermService.setStudentList([]);
    }
  }

  private readFile(): void {
    this.csvParseService.readFile(this.csvFile, this.encoding.value)
      .subscribe({
        next: fileText => {
          this.studentListForm.patchValue({
            studentCsv: fileText
          });
        }
      });
  }

  private initEncodingValueChanges(): void {
    this.encoding.valueChanges
      .pipe(takeUntil(this.ngUnsubscribe$))
      .subscribe({
        next: () => {
          if (this.csvFile) {
            this.readFile();
          }
        }
      });
  }

  private initStudentCsvValueChanges(): void {
    this.studentCsv.valueChanges
      .pipe(takeUntil(this.ngUnsubscribe$))
      .subscribe({
        next: text => {
          this.csvLines = CsvParseService.splitTextToLines(text);
        }
      });
  }

  private initFormValueChanges(): void {
    this.studentListForm.valueChanges
      .pipe(
        debounceTime(250),
        takeUntil(this.ngUnsubscribe$)
      )
      .subscribe({
        next: formValues => {
          if (this.isLoginAndNameInSameColumn) {
            this.parseSameColumn(formValues);
          } else {
            this.parseSeparateColumns(formValues);
          }
        }
      });

    this.newTermService.getStudentList$()
      .pipe(takeUntil(this.ngUnsubscribe$))
      .subscribe({
        next: tableData => {
          this.studentsList = tableData;
        }
      });
  }

  private parseSameColumn(formValues: any): void {
    let studentLoginsAndNames: LoginNamePayload[];
    if (CsvParseService.isAllAttributesSet(formValues) && CsvParseService.isAllAttributesSet(formValues.sameColumn)) {
      studentLoginsAndNames = this.csvLines
        .map(line => {
          const parts = line.split(formValues.separator)
            .map(part => part.trim());
          return {
            login: parts[formValues.sameColumn.loginPosition - 1],
            nameAndDegrees: parts[formValues.sameColumn.nameAndDegreesPosition - 1]
          };
        });
    } else {
      studentLoginsAndNames = [];
    }
    this.newTermService.setStudentList(studentLoginsAndNames);
  }

  private parseSeparateColumns(formValues: any): void {
    let studentLoginsAndNames: LoginNamePayload[];
    if (CsvParseService.isAllAttributesSet(formValues) && CsvParseService.isAllAttributesSet(formValues.separateColumns)) {
      studentLoginsAndNames = this.csvLines
        .map(line => {
          const parts = line.split(formValues.separator)
            .map(part => part.trim());
          return {
            login: parts[formValues.separateColumns.loginPositionSep - 1],
            nameAndDegrees: parts[formValues.separateColumns.surnamePositionSep - 1] + ' '
              + parts[formValues.separateColumns.namePositionSep - 1]
          };
        });
    } else {
      studentLoginsAndNames = [];
    }
    this.newTermService.setStudentList(studentLoginsAndNames);
  }
}
