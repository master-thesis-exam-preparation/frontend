import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { SharedModule } from '@shared/shared.module';
import { StudentListFormComponent } from './student-list-form.component';
import { AppModule } from '../../../../../../../app.module';
import { StudentListTableComponent } from './student-list-table/student-list-table.component';

describe('StudentListFormComponent', () => {
  let component: StudentListFormComponent;
  let fixture: ComponentFixture<StudentListFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [StudentListFormComponent, StudentListTableComponent],
      imports: [HttpClientTestingModule, SharedModule, AppModule],
      providers: [FormBuilder]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StudentListFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
