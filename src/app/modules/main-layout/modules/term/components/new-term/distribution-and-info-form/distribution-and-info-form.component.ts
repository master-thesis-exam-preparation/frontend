import { Component, Input, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserService } from '@core/services/user.service';
import { StudentsDistribution } from '../../../students-distribution.enum';
import { AuthService } from '@core/services';
import { UserInfoPayload } from '@core/services/models/userInfo.payload';
import { take } from 'rxjs/operators';

/**
 * Component for displaying the page with a step of a new term form for specifying additional information about new term.
 */
@Component({
  selector: 'app-distribution-and-info-form',
  templateUrl: './distribution-and-info-form.component.html',
  styleUrls: ['./distribution-and-info-form.component.scss']
})
export class DistributionAndInfoFormComponent implements OnInit {

  /**
   * Determines whether form is for specifying information for new term or editing existing one.
   */
  @Input() edit = false;

  isLoading: boolean;

  distributionAndInfoForm: FormGroup;

  employeeList: UserInfoPayload[] = [];
  filteredEmployeeLists: UserInfoPayload[] = [];
  selectedEmployeeList: UserInfoPayload[] = [];

  distributionKeys: string[];

  constructor(private fb: FormBuilder,
              private userService: UserService,
              private authService: AuthService) {
    this.distributionKeys = Object.keys(StudentsDistribution);
  }

  /**
   * Gets value of the distribution select menu in the form.
   */
  get distribution(): AbstractControl {
    return this.distributionAndInfoForm.get('distribution');
  }

  /**
   * Gets values of the employees select menu in the form.
   */
  get employees(): AbstractControl {
    return this.distributionAndInfoForm.get('employees');
  }

  /**
   * Gets value of the termInformationText input in the form.
   */
  get termInformationText(): AbstractControl {
    return this.distributionAndInfoForm.get('termInformationText');
  }

  /**
   * Gets value of the sendEmail checkbox in the form.
   */
  get sendEmail(): AbstractControl {
    return this.distributionAndInfoForm.get('sendEmail');
  }

  /**
   * Gets value of the emailText input in the form.
   */
  get emailText(): AbstractControl {
    return this.distributionAndInfoForm.get('emailText');
  }

  /**
   * Sets validators to the form inputs. Gets list of employees and admins.
   */
  ngOnInit(): void {
    this.isLoading = true;
    this.userService.getAllEmployeesList$()
      .subscribe({
        next: employees => {
          this.authService.loggedUserInfo$
            .pipe(take(1))
            .subscribe({
              next: loggedUserInfo => {
                const parsedEmployees: UserInfoPayload[] = employees
                  .filter(employee => employee.email !== loggedUserInfo.email);
                this.employeeList = parsedEmployees;
                this.filteredEmployeeLists = parsedEmployees;
                this.isLoading = false;
              }
            });
        }
      });


    this.distributionAndInfoForm = this.fb.group({
      distribution: ['', [
        Validators.required,
      ]],
      employees: [[]],
      termInformationText: ['', [
        Validators.maxLength(65535)
      ]],
      sendEmail: true,
      emailText: ['', [
        Validators.maxLength(65535)
      ]]
    });
  }

  /**
   * Filter employees on each key press in filter text input.
   * @param event  key press event
   */
  onKey(event: Event): void {
    const filterValue = (event.target as HTMLInputElement).value;
    this.filteredEmployeeLists = this.searchEmployee(filterValue);
  }

  /**
   * Compare two users based on their e-mail addresses and names with degrees
   * @param o1  user 1
   * @param o2  user 2
   */
  compareFunction(o1: UserInfoPayload, o2: UserInfoPayload): boolean {
    return (o1.email === o2.email && o1.nameWithDegrees === o2.nameWithDegrees);
  }

  /**
   * Resets employees filter.
   * @param opened  true if select menu is opened
   */
  clearFilter(opened: boolean): void {
    if (!opened) {
      this.filteredEmployeeLists = this.employeeList;
    }
  }

  private searchEmployee(value: string): UserInfoPayload[] {
    const filter = value.trim().toLowerCase();
    return this.employeeList.filter(employee => employee.email.toLowerCase().includes(filter) ||
      employee.nameWithDegrees.toLowerCase().includes(filter));
  }
}
