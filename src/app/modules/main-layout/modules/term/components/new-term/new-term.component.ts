import { AfterViewInit, ChangeDetectorRef, Component, Input, OnChanges, OnDestroy, OnInit, SimpleChanges, ViewChild } from '@angular/core';
import { TermInfoFormComponent } from './term-info-form/term-info-form.component';
import { StudentListFormComponent } from './student-list-form/student-list-form.component';
import { TermRunInfoFormComponent } from './term-run-info-form/term-run-info-form.component';
import { FormArray, FormGroup } from '@angular/forms';
import { NewTermService } from '../../services/new-term.service';
import { SnackBarService } from '@core/services';
import { UnsavedChangesGuard } from '@core/services/guards/unsaved-changes.guard';
import { ManualPlacementFormComponent } from './manual-placement-form/manual-placement-form.component';
import { debounceTime, takeUntil } from 'rxjs/operators';
import { LoginNamePayload } from '../../models/login-name.payload';
import { DistributionAndInfoFormComponent } from './distribution-and-info-form/distribution-and-info-form.component';
import { ActivatedRoute, Router } from '@angular/router';
import { NewTermRequestPayload } from '../../models/term/new-term-request.payload';
import { TermRunRequest } from '../../models/term-run.request';
import { StudentsPlacementRequestPayload } from '../../models/students-placement-request.payload';
import { EditTermInfoResponse } from '../../models/term/edit-term-info-response.payload';
import { EditTermService } from '../../services/edit-term.service';
import { Subject } from 'rxjs';
import { StudentsPlacement } from '../../models/students-placement';
import { MatStepper } from '@angular/material/stepper';
import { EditTermRequestPayload } from '../../models/term/edit-term-request.payload';

/**
 * Component for displaying the page with a form for creating new term.
 */
@Component({
  selector: 'app-new-term',
  templateUrl: './new-term.component.html',
  styleUrls: ['./new-term.component.scss']
})
export class NewTermComponent implements OnInit, AfterViewInit, OnChanges, OnDestroy {

  /**
   * Determines whether form is for creating new term or editing an existing one.
   */
  @Input() edit = false;

  /**
   * Data about existing term.
   */
  @Input() formFillData: EditTermInfoResponse;

  @ViewChild(TermInfoFormComponent) termInfoFormComponent: TermInfoFormComponent;
  @ViewChild(StudentListFormComponent) studentListFormComponent: StudentListFormComponent;
  @ViewChild(TermRunInfoFormComponent) termRunInfoFormComponent: TermRunInfoFormComponent;
  @ViewChild(ManualPlacementFormComponent) manualPlacementFormComponent: ManualPlacementFormComponent;
  @ViewChild(DistributionAndInfoFormComponent) distributionAndInfoFormComponent: DistributionAndInfoFormComponent;

  @ViewChild('stepper') stepper: MatStepper;

  termInfoForm: FormGroup;
  studentListForm: FormGroup;
  termRunInfoForm: FormGroup;
  manualPlacementForm: FormGroup;
  distributionAndInfoForm: FormGroup;

  isLoading = false;

  isStudentListFormValid = false;
  isAllStudentsSeated = false;

  studentListErrorMessage: string = null;

  private isLoginsNotOk: boolean;
  private isNameNotOk: boolean;
  private ngUnsubscribe$ = new Subject();

  constructor(private newTermService: NewTermService,
              private editTermService: EditTermService,
              private changeDetector: ChangeDetectorRef,
              private snackBarService: SnackBarService,
              private router: Router,
              private route: ActivatedRoute,
              private deactivateGuard: UnsavedChangesGuard) {
  }

  /**
   * Sets that page cannot be left without confirmation.
   */
  ngOnInit(): void {
    this.deactivateGuard.canBeDeactivated = false;
  }

  /**
   * Check uploaded students list and if all students are seated after component view init.
   */
  ngAfterViewInit(): void {
    // view update after view init and change detection
    this.termInfoForm = this.termInfoFormComponent.termInfoForm;
    this.studentListForm = this.studentListFormComponent.studentListForm;
    this.termRunInfoForm = this.termRunInfoFormComponent.termRunInfoForm;
    this.manualPlacementForm = this.manualPlacementFormComponent.manualPlacementForm;
    this.distributionAndInfoForm = this.distributionAndInfoFormComponent.distributionAndInfoForm;
    this.changeDetector.detectChanges();

    this.newTermService.getStudentList$()
      .pipe(
        debounceTime(250),
        takeUntil(this.ngUnsubscribe$)
      )
      .subscribe({
        next: students => {
          this.checkIfStudentListStepIsOk(students);
        }
      });

    this.newTermService.getNumberOfRemainingStudents$()
      .pipe(takeUntil(this.ngUnsubscribe$))
      .subscribe({
        next: numberOfRemainingStudents => {
          this.isTermRunInfoStepComplete(numberOfRemainingStudents);
        }
      });
  }

  /**
   * Update everything everytime something on input change.
   * @param changes  changes in inputs
   */
  ngOnChanges(changes: SimpleChanges): void {
    if (changes.formFillData?.currentValue) {
      this.termInfoFormComponent.termName.setValue(this.formFillData.termName);
      this.termInfoFormComponent.course.setValue(this.formFillData.courseId);

      this.distributionAndInfoFormComponent.distribution.setValue(this.formFillData.distribution);
      this.distributionAndInfoFormComponent.employees.setValue(this.formFillData.managers);
      this.distributionAndInfoFormComponent.termInformationText.setValue(this.formFillData.informationText);

    }
  }

  /**
   * Unsubscribes from everything and clear term subjects content before destroying the component.
   */
  ngOnDestroy(): void {
    this.ngUnsubscribe$.next();
    this.ngUnsubscribe$.complete();
    this.editTermService.flushBehaviorSubjects();
    this.newTermService.flushBehaviorSubjects();
  }

  /**
   * Determines whether user can fo to the second step of he new term form.
   */
  nextStudentListButtonClick(): void {
    if (this.isLoginsNotOk) {
      this.snackBarService.openDangerSnackBar('Některý ze studentů nemá načtený login.\n' +
        'V náhledu zkontrolujte, zda všichni studenti mají správně nastavený login.');
    } else if (this.isNameNotOk) {
      this.snackBarService.openDangerSnackBar('Některý ze studentů nemá načtené jméno a příjmení.\n' +
        'V náhledu zkontrolujte, zda všichni studenti mají správně nastavené jméno a příjmení.');
    } else if ((this.studentListForm.invalid || !this.isStudentListFormValid) &&
      (this.studentListFormComponent.addMoreStudents || !this.edit)) {
      this.showFormErrorMessage(this.studentListForm);
    } else {
      this.stepper.selected.completed = true;
      this.stepper.next();
    }
  }

  /**
   * Determines whether user can go to the third step of the new term form.
   */
  nextTermInfoButtonClick(): void {
    if (this.termInfoForm.invalid) {
      this.showFormErrorMessage(this.termInfoForm);
    } else {
      this.stepper.selected.completed = true;
      this.stepper.next();
    }
  }

  /**
   * Determines whether user can go to the fourth step of the new term form.
   */
  nextTermRunInfoButtonClick(): void {
    if (!this.isAllStudentsSeated) {
      this.snackBarService.openDangerSnackBar('Nejsou usazeni všichni studenti');
    } else if (this.termRunInfoForm.invalid) {
      this.showFormErrorMessage(this.termRunInfoForm);
    } else {
      for (let i = 0; i < this.manualPlacementFormComponent.placementFormGroups.length; i++) {
        const selectedTermRun = this.newTermService
          .termRunsValue
          .filter(tr => this.manualPlacementFormComponent.selectedTermRuns[i]?.id === tr.id);
        if (selectedTermRun.length === 1) {
          const prevSelectedRoom = this.manualPlacementFormComponent.room(i).value;
          const prevSelectedSeats = this.manualPlacementFormComponent.selectedSeatsList[i];
          this.manualPlacementFormComponent.termRun(i).setValue(selectedTermRun[0]);
          const selectedRoom = selectedTermRun[0].rooms
            .filter(r => r.abbreviation === prevSelectedRoom.abbreviation && r.name === prevSelectedRoom.name);
          if (selectedRoom.length === 1) {
            this.manualPlacementFormComponent.room(i).setValue(selectedRoom[0]);
            if (prevSelectedSeats.length > 0) {
              this.manualPlacementFormComponent.selectedSeatsList[i] = prevSelectedSeats;
            }
          } else {
            this.manualPlacementFormComponent.selectedSeatsList[i] = [];
            this.manualPlacementFormComponent.updateStudentsPlacementSeats(i, []);
            this.manualPlacementFormComponent.placeButtonDisabled[i] = true;
          }
        } else {
          this.manualPlacementFormComponent.termRun(i).setValue(null);
          this.manualPlacementFormComponent.room(i).setValue(undefined);
          this.manualPlacementFormComponent.selectedSeatsList[i] = [];
          this.manualPlacementFormComponent.placeButtonDisabled[i] = true;
        }
      }
      this.stepper.selected.completed = true;
      this.stepper.next();
    }
  }

  /**
   * Determines whether user can go to the fifth step of the new term form.
   */
  nextManualPlacementButtonClick(): void {
    let isSameNumberOfStudentsAndSelectedSeats = true;
    for (let index = 0; index < this.manualPlacementFormComponent.selectedSeatsList.length; index++) {
      const cellList = this.manualPlacementFormComponent.selectedSeatsList[index];
      const cellListLength = cellList.length;
      if (cellListLength > 0) {
        isSameNumberOfStudentsAndSelectedSeats = this.manualPlacementFormComponent.students(index).value.length === cellListLength;
        if (!isSameNumberOfStudentsAndSelectedSeats) {
          this.snackBarService.openDangerSnackBar('U ' + (index + 1) + '. manuálního usazení byl zvolen větší/menší počet míst, ' +
            'než je počet zvolených studentů');
          return;
        }
      }
    }

    const allMandatorySeatsSelected = !this.manualPlacementFormComponent.mandatorySelectedSeats
      .some((mustBeSelected, index) => mustBeSelected && this.manualPlacementFormComponent.selectedSeatsList[index].length === 0);
    if (this.manualPlacementForm.invalid || !allMandatorySeatsSelected) {
      this.showFormErrorMessage(this.manualPlacementForm);
    } else {
      this.stepper.selected.completed = true;
      this.stepper.next();
    }
  }

  /**
   * Determines whether user can go to the last step of the new term form.
   */
  nextDistributionAndInfoButtonClick(): void {
    if (this.distributionAndInfoForm.invalid) {
      this.showFormErrorMessage(this.distributionAndInfoForm);
    } else {
      this.stepper.selected.completed = true;
      this.stepper.next();
    }
  }

  /**
   * Submit the form and send the create new term request or edit existing term request.
   */
  submitNewTermForm(): void {
    this.isLoading = true;
    const termName = this.termInfoFormComponent.termName.value;
    const courseId = this.termInfoFormComponent.course.value;

    const spacingBetweenStudents = this.newTermService.selectedSpacingValue;
    const termRuns = this.newTermService.termRunsValue
      .map(termRun => {
          const roomIds = termRun.rooms.map(room => room.roomId);
          const assignmentIds = termRun.assignments.map(assignment => assignment.assignmentId);
          return new TermRunRequest(termRun.id, termRun.startTime, termRun.endTime, roomIds, assignmentIds);
        }
      );

    const studentsPlacements = this.newTermService.studentPlacementsValue
      .map(sp => {
        const logins = sp.students.map(student => student.login);
        const cellIds = sp.seats.map(seat => seat.cellId);
        let roomId;
        if (sp.room) {
          roomId = sp.room.roomId;
        } else {
          roomId = null;
        }
        return new StudentsPlacementRequestPayload(logins, sp.termRun.id, roomId, cellIds);
      });
    const distribution = this.distributionAndInfoFormComponent.distribution.value;

    const additionalTermManagersEmails = this.distributionAndInfoFormComponent.employees.value
      .map(employee => employee.email);
    const informationText = this.distributionAndInfoFormComponent.termInformationText.value;
    const sendEmail = this.distributionAndInfoFormComponent.sendEmail.value;
    const emailMessage = this.distributionAndInfoFormComponent.emailText.value;

    const studentsList = this.newTermService.studentListValue.slice();

    let obs;
    if (this.edit) {
      const termId = Number(this.route.snapshot.paramMap.get('id'));
      const existingStudentList = this.editTermService.existingStudentListValue;
      const addStudentsAtTheEnd = this.studentListFormComponent.addStudentsAtTheEnd;
      const editTermRequestPayload = new EditTermRequestPayload(termName, courseId, studentsList, existingStudentList,
        addStudentsAtTheEnd, spacingBetweenStudents, termRuns, studentsPlacements, distribution, additionalTermManagersEmails,
        informationText, sendEmail, emailMessage);
      obs = this.newTermService.editTerm$(termId, editTermRequestPayload);
    } else {
      const newTermRequestPayload = new NewTermRequestPayload(termName, courseId, studentsList, spacingBetweenStudents, termRuns,
        studentsPlacements, distribution, additionalTermManagersEmails, informationText, sendEmail, emailMessage);
      obs = this.newTermService.createNewTerm$(newTermRequestPayload);
    }

    obs.subscribe({
      next: message => {
        this.snackBarService.openSuccessSnackBar(message);
        this.deactivateGuard.canBeDeactivated = true;
        this.router.navigate(['/terms']);
      },
      error: () => {
        this.isLoading = false;
      }
    });
  }

  /**
   * Determines whether user uploaded all necessary information for all students.
   * @param students  list of uploaded students
   */
  checkIfStudentListStepIsOk(students: LoginNamePayload[]): void {
    if (this.studentListFormComponent.existingStudentList.length > 0 && this.studentListFormComponent.addMoreStudents === false) {
      this.isStudentListFormValid = true;
    } else {
      this.isStudentListFormValid = this.isAllStudentsOk(students) && !this.checkIfDuplicateExists(students);
    }
  }

  /**
   * Update student placements after student placement change.
   * @param change  change in student placements
   */
  studentPlacementsChange(change: { placement: StudentsPlacement, index: number }): void {
    this.manualPlacementFormComponent.students(change.index).setValue(change.placement.students);
  }

  private isTermRunInfoStepComplete(numberOfRemainingStudents: number): void {
    const isSomeoneSeated = this.termRunInfoFormComponent?.numberOfSeatedStudents > 0;
    const isAllSeated = numberOfRemainingStudents <= 0;
    this.isAllStudentsSeated = isSomeoneSeated && isAllSeated;
  }

  private isAllStudentsOk(students: LoginNamePayload[]): boolean {
    if (students.length <= 0) {
      return false;
    }
    this.isLoginsNotOk = students.some(student => !student.login);
    this.isNameNotOk = students.some(student => !student.nameAndDegrees);

    const res = !this.isLoginsNotOk && !this.isNameNotOk;
    if (!res) {
      this.studentListErrorMessage = 'Seznam studentů není správně načten - některému ze studentů chybí jméno nebo login.\nUjistěte se, ' +
        'že máte zvoleny správná čísla sloupců s daty.';
    }
    return res;
  }

  private checkIfDuplicateExists(studentsList: LoginNamePayload[]): boolean {
    const set = new Set(this.editTermService.existingStudentListValue.map(s => s.login));

    for (const student of studentsList) {
      if (student.login && student.nameAndDegrees) {
        if (set.has(student.login)) {
          this.studentListErrorMessage = 'Student s loginem \'' + student.login + '\' je v seznamu vícekrát.\n' +
            'Odstraňte prosím duplicitní záznamy.';
          this.snackBarService.openDangerSnackBar(this.studentListErrorMessage);
          return true;
        }
        set.add(student.login);
      }
    }
    this.studentListErrorMessage = null;
    return false;
  }

  private showFormErrorMessage(form: FormGroup): void {
    this.loopThroughFormControls(form);
    this.snackBarService.openDangerSnackBar('Formulář obsahuje chyby, opravte je');
  }

  private loopThroughFormControls(form: FormGroup): void {
    Object.keys(form.controls).forEach(field => {
      const control = form.get(field);
      control.markAsTouched({onlySelf: true});
      if (control instanceof FormGroup) {
        this.loopThroughFormControls(control);
      } else if (control instanceof FormArray) {
        for (const formArrayControl of (control as FormArray).controls) {
          this.loopThroughFormControls(formArrayControl as FormGroup);
        }
      }
    });
  }
}
