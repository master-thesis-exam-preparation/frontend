import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HttpClientTestingModule } from '@angular/common/http/testing';
import { SharedModule } from '@shared/shared.module';
import { NewTermComponent } from './new-term.component';
import { RouterTestingModule } from '@angular/router/testing';
import { AppModule } from '../../../../../../app.module';
import { StudentListFormComponent } from './student-list-form/student-list-form.component';
import { TermInfoFormComponent } from './term-info-form/term-info-form.component';
import { TermRunInfoFormComponent } from './term-run-info-form/term-run-info-form.component';
import { ManualPlacementFormComponent } from './manual-placement-form/manual-placement-form.component';
import { DistributionAndInfoFormComponent } from './distribution-and-info-form/distribution-and-info-form.component';

describe('NewTermComponent', () => {
  let component: NewTermComponent;
  let fixture: ComponentFixture<NewTermComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [NewTermComponent, TermInfoFormComponent, StudentListFormComponent, TermRunInfoFormComponent,
        ManualPlacementFormComponent, DistributionAndInfoFormComponent],
      imports: [HttpClientTestingModule, RouterTestingModule, AppModule, SharedModule],
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NewTermComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
