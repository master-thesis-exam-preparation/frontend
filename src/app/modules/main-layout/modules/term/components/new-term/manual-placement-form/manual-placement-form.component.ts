import { Component, Input, OnChanges, OnDestroy, OnInit, SimpleChanges } from '@angular/core';
import { AbstractControl, FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LoginNamePayload } from '../../../models/login-name.payload';
import { FreeRoomResponsePayload } from '../../../../course/models/free-room-response.payload';
import { TermRunPayload } from '../../../models/term-run.payload';
import { NewTermService } from '../../../services/new-term.service';
import { SnackBarService } from '@core/services';
import { pairwise, startWith, take, takeUntil } from 'rxjs/operators';
import { SpacingBetweenStudents } from '../../../spacing-between-students.enum';
import { StudentsPlacement } from '../../../models/students-placement';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { SelectPlaceDialogComponent } from './select-place-dialog/select-place-dialog.component';
import { RoomService } from '../../../../rooms/services/room.service';
import { CellPayload } from '../../../../rooms/models/cell.payload';
import { merge, Subject } from 'rxjs';
import { EditTermService } from '../../../services/edit-term.service';
import { EditTermInfoResponse } from '../../../models/term/edit-term-info-response.payload';
import { TermRunEditResponse } from '../../../models/term-run-edit-response.payload';
import { CellType } from '../../../../rooms/cell-type.enum';
import { SeatingPlanCell } from '../../../models/seating-plan-cell.payload';
import { EditTermRoomPayload } from '../../../models/edit-term-room.payload';

/**
 * Component for displaying the page with a step of a new term form for manual placement of the students on the specific seats.
 */
@Component({
  selector: 'app-manual-placement-form',
  templateUrl: './manual-placement-form.component.html',
  styleUrls: ['./manual-placement-form.component.scss']
})
export class ManualPlacementFormComponent implements OnInit, OnDestroy, OnChanges {

  constructor(private fb: FormBuilder,
              private newTermService: NewTermService,
              private snackBarService: SnackBarService,
              private selectPlaceDialog: MatDialog,
              private roomService: RoomService,
              private editTermService: EditTermService) {
  }

  /**
   * Gets placementFormGroups form array from the form
   */
  get placementFormGroups(): FormArray {
    return this.manualPlacementForm.get('placementFormGroups') as FormArray;
  }

  /**
   * Determines whether form is for creating new manual placements or editing an existing ones.
   */
  @Input() edit = false;

  /**
   * Data about existing term.
   */
  @Input() formFillData: EditTermInfoResponse;

  filteredFormFillData: EditTermInfoResponse;

  manualPlacementForm: FormGroup;

  selectedSpacingBetweenStudents: SpacingBetweenStudents = 0;

  selectedTermRuns: TermRunPayload[] = [];
  selectedRoomsAbbreviations: string[][] = [];

  studentList: LoginNamePayload[] = [];
  filteredStudentsLists: LoginNamePayload[][] = [];

  termRunList: TermRunPayload[] = [];
  roomList: FreeRoomResponsePayload[][] = [[]];
  selectedSeatsList: CellPayload[][] = [[]];
  mandatorySelectedSeats: boolean[] = [];

  placeButtonDisabled: boolean[] = [true];

  private ngUnsubscribe$ = new Subject();

  private static getIndexOfLastRoomPlanWithFreeSeatsAtTheEnd(termRun: TermRunEditResponse, selectedStudents: LoginNamePayload[]): number {
    let resultIndex;
    let totalCapacity = 0;
    for (let i = termRun.rooms.length - 1; i >= 0; i--) {
      const roomPlan = termRun.rooms[i];
      let freeSeatsAtTheEndOfRoom = roomPlan.freeSeatsAtTheEndOfRoom;
      if (!freeSeatsAtTheEndOfRoom) {
        freeSeatsAtTheEndOfRoom = [];
      }
      totalCapacity += freeSeatsAtTheEndOfRoom.length;
      if (freeSeatsAtTheEndOfRoom.length !== roomPlan.capacity) {
        if (freeSeatsAtTheEndOfRoom.length === 0) {
          resultIndex = i + 1;
        } else {
          resultIndex = i;
        }
        break;
      }
    }
    if (resultIndex >= termRun.rooms.length || totalCapacity < selectedStudents.length) {
      return -1;
    }
    return resultIndex;
  }

  private static getRoomSeatsInSnakePattern(room: EditTermRoomPayload): SeatingPlanCell[] {
    const seatsInSnakePattern: SeatingPlanCell[] = [];
    let goRight = true;
    for (const row of room.seatingPlan) {
      let containsSeat = false;
      if (goRight) {
        for (const cell of row) {
          if (cell.cellType === CellType.SEAT) {
            seatsInSnakePattern.push(cell);
            containsSeat = true;
          }
        }
      } else {
        for (let i = row.length - 1; i >= 0; i--) {
          if (row[i].cellType === CellType.SEAT) {
            seatsInSnakePattern.push(row[i]);
            containsSeat = true;
          }
        }
      }
      if (containsSeat) {
        goRight = !goRight;
      }
    }
    seatsInSnakePattern.reverse();
    return seatsInSnakePattern;
  }

  private static getFreeSeatsAtTheEndOfRoom(room: EditTermRoomPayload, seatsInSnakePattern: SeatingPlanCell[]): number[] {
    const freeRoomPlanSeatsInSnakePattern: number[] = [];
    for (let i = 0; i < room.freeSeatIds.length; i++) {
      const freeSeatId = seatsInSnakePattern[i].cellId;
      if (room.freeSeatIds.some(seatId => seatId === freeSeatId)) {
        freeRoomPlanSeatsInSnakePattern.push(freeSeatId);
      } else {
        break;
      }
    }
    freeRoomPlanSeatsInSnakePattern.reverse();
    return freeRoomPlanSeatsInSnakePattern;
  }

  /**
   * Gets values of the students select menu at the specified index in the placementFormGroups form array.
   * @param index  index of the placement form group
   */
  students(index: number): AbstractControl {
    return this.placementFormGroups.controls[index].get('students');
  }

  /**
   * Gets value of the termRun select menu at the specified index in the placementFormGroups form array.
   * @param index  index of the placement form group
   */
  termRun(index: number): AbstractControl {
    return this.placementFormGroups.controls[index].get('termRun');
  }

  /**
   * Gets value of the room select menu at the specified index in the placementFormGroups form array.
   * @param index  index of the placement form group
   */
  room(index: number): AbstractControl {
    return this.placementFormGroups.controls[index].get('room');
  }

  /**
   * Sets validators to the form inputs. Fills edit form with existing data.
   */
  ngOnInit(): void {
    this.getSelectedSpacingBetweenStudents();
    this.createManualPlacementForm();
    if (!this.edit) {
      this.fillStudentList();
      this.fillTermRunList(null);
    }
  }

  /**
   * Unsubscribes from everything before destroying the component.
   */
  ngOnDestroy(): void {
    this.ngUnsubscribe$.next();
    this.ngUnsubscribe$.complete();
  }

  /**
   * Update everything everytime something on input change.
   * @param changes  changes in inputs
   */
  ngOnChanges(changes: SimpleChanges): void {
    if (changes.formFillData?.currentValue) {
      this.filteredFormFillData = this.formFillData;
      this.fillStudentList();
      this.fillTermRunList(null);
    }
  }

  /**
   * Adds new placement group to form array.
   */
  addPlacementFormGroup(): void {
    this.filteredStudentsLists.push(this.studentList);
    this.selectedTermRuns.push();
    this.selectedRoomsAbbreviations.push([]);
    this.placeButtonDisabled.push(true);
    this.selectedSeatsList.push([]);
    const newGroup = this.createPlacementFormGroup([], null, null, true);
    this.placementFormGroups.push(newGroup);
    this.newTermService.addStudentsPlacements(this.findFormGroupIndex(newGroup));
  }

  /**
   * Removes existing placement group from form array.
   * @param index  index of the placement form group
   */
  removePlacementFormGroup(index: number): void {
    this.filteredStudentsLists.splice(index, 1);
    this.selectedTermRuns.splice(index, 1);
    this.selectedRoomsAbbreviations.splice(index, 1);
    this.newTermService.removeStudentsPlacements(index);
    this.placeButtonDisabled.splice(index, 1);
    this.selectedSeatsList.splice(index, 1);
    this.mandatorySelectedSeats.splice(index, 1);
    this.placementFormGroups.removeAt(index);
    if (this.edit) {
      this.removeAllSelectedStudentsFromTermRuns();
    }
  }

  /**
   * Filter students on each key press in filter text input.
   * @param event  key press event
   * @param index  index of the placement form group
   */
  onKeyStudent(event: Event, index: number): void {
    const filterValue = (event.target as HTMLInputElement).value;
    this.filteredStudentsLists[index] = this.searchStudent(filterValue);
  }

  /**
   * Displays dialog pop up window with room plan to select specific seats.
   * @param index  index of the placement form group
   */
  showSelectPlaceDialog(index: number): void {
    let dialog: MatDialogRef<any>;
    if (this.edit) {
      const existingRoom = this.filteredFormFillData?.termRuns
        .find(termRun => termRun.id === this.termRun(index).value.id)
        .rooms.find(room => room.roomId === this.room(index).value.roomId);
      dialog = this.selectPlaceDialog.open(SelectPlaceDialogComponent, {
        maxWidth: '85vw',
        maxHeight: '85vh',
        data: {
          studentsList: this.students(index).value,
          roomPlanCells: existingRoom.seatingPlan,
          selectedSeats: this.selectedSeatsList[index],
          edit: true
        }
      });
      this.getSelectedSeatsFromDialog(dialog, index);
    } else {
      this.roomService.getRoomDetailsPublic$((this.room(index).value as FreeRoomResponsePayload).roomId)
        .subscribe({
          next: roomDetails => {
            dialog = this.selectPlaceDialog.open(SelectPlaceDialogComponent, {
              maxWidth: '85vw',
              maxHeight: '85vh',
              data: {
                studentsList: this.students(index).value,
                roomPlanCells: roomDetails.roomPlans[this.selectedSpacingBetweenStudents].cells,
                selectedSeats: this.selectedSeatsList[index],
                edit: false
              }
            });
            this.getSelectedSeatsFromDialog(dialog, index);
          }
        });
    }
  }

  /**
   * Resets students filter.
   * @param opened  true if select menu is opened
   * @param index  index of the group in form array
   */
  clearFilter(opened: boolean, index: number): void {
    if (!opened) {
      this.filteredStudentsLists[index] = this.studentList;
    }
  }

  private getSelectedSpacingBetweenStudents(): void {
    this.newTermService.getSelectedSpacing$()
      .pipe(takeUntil(this.ngUnsubscribe$))
      .subscribe({
        next: spacing => {
          if (this.manualPlacementForm) { // is initialized?
            for (let i = 0; i < this.placementFormGroups.length; i++) {
              this.selectedSeatsList[i] = [];
              this.updateStudentsPlacementSeats(i, []);
            }
          }
          this.selectedSpacingBetweenStudents = spacing;
        }
      });
  }

  private createManualPlacementForm(): void {
    this.manualPlacementForm = this.fb.group({
      placementFormGroups: this.fb.array([])
    });
  }

  private fillStudentList(): void {
    const obs1 = this.newTermService.getStudentList$();
    const obs2 = this.editTermService.getExistingStudentList$();
    merge(obs1, obs2)
      .pipe(takeUntil(this.ngUnsubscribe$))
      .subscribe({
        next: () => {
          this.studentList = this.newTermService.studentListValue
            .concat(this.editTermService.existingStudentListValue);
          if (this.studentList.every(loginName => loginName.login)) {
            this.studentList.sort((a, b) => a.login.localeCompare(b.login));
            for (let i = 0; i < this.filteredStudentsLists.length; i++) {
              this.filteredStudentsLists[i] = this.studentList;
              this.students(i).setValue(
                this.studentList.filter(s1 => this.students(i).value.some(s2 => JSON.stringify(s1) === JSON.stringify(s2)))
              );
            }
          }
        }
      });
  }

  private fillTermRunList(students: LoginNamePayload[]): void {
    this.newTermService.getTermRuns$()
      .pipe(takeUntil(this.ngUnsubscribe$))
      .subscribe({
        next: termRuns => {
          termRuns = termRuns.sort((a, b) => a.startTime - b.startTime);
          if (termRuns.length > 0) {
            if (this.edit) {
              const existingTermRuns = termRuns.map(termRun => this.filteredFormFillData?.termRuns
                .find(existingTermRun => existingTermRun.id === termRun.id)
              );
              let termRunsWithFreeSpace;
              if (students) {
                termRunsWithFreeSpace = existingTermRuns.filter(termRun => termRun.rooms
                  .some(room => room.freeSeatIds.length >= students.length)
                ).map(termRun => termRun.id);
              } else {
                termRunsWithFreeSpace = existingTermRuns.filter(termRun => termRun.rooms
                  .some(room => room.freeSeatIds.length > 0)
                ).map(termRun => termRun.id);
              }
              this.termRunList = termRuns.filter(termRun => termRunsWithFreeSpace.includes(termRun.id));
            } else {
              this.termRunList = termRuns;
            }
          }
        }
      });
  }

  private createPlacementFormGroup(defaultStudents: LoginNamePayload[], defaultTermRun: TermRunPayload,
                                   defaultRoom: FreeRoomResponsePayload, disabledRoomSelect: boolean): FormGroup {
    const newGroup = this.fb.group({
      students: [defaultStudents, [
        Validators.required
      ]],
      termRun: [defaultTermRun, [
        Validators.required
      ]],
      room: [{value: defaultRoom, disabled: disabledRoomSelect}]
    });
    this.listenForTermRunChanges(newGroup);
    this.listenForRoomChanges(newGroup);
    this.listenForStudentsChanges(newGroup);
    newGroup.controls.students.markAsTouched();   // immediately show 'required' message
    return newGroup;
  }

  private searchStudent(value: string): LoginNamePayload[] {
    const filter = value.trim().toLowerCase();
    return this.studentList.filter(student => student.login.toLowerCase().includes(filter) ||
      student.nameAndDegrees.toLowerCase().includes(filter));
  }

  private listenForTermRunChanges(formGroup: FormGroup): void {
    formGroup.controls.termRun.valueChanges
      .pipe(
        startWith(''),
        pairwise()
      )
      .subscribe({
        next: ([prevTermRun, newTermRun]: [TermRunPayload, TermRunPayload]) => {
          if (newTermRun && prevTermRun !== newTermRun) {
            const index = this.findFormGroupIndex(formGroup);
            const allRoomsCapacity = (this.termRun(index).value.rooms as FreeRoomResponsePayload[])
              .reduce((acc, room) =>
                acc + room.roomPlans[this.selectedSpacingBetweenStudents].capacity, 0);
            if (this.students(index).value.length > allRoomsCapacity) {
              const message = 'Běh nemá dostatečnou kapacitu.\n' +
                'Byl zvolen větší počet studentů, než je kapacita všech místností daného běhu';
              this.termRunError(message, index, prevTermRun);
              return;
            }
            const termRunsFormGroupIds = this.getPreviouslySelectedTermRunsFormGroupIds(newTermRun, index);
            if (termRunsFormGroupIds.length > 0) { // term run is already selected
              const hasAllTermRunsSelectedRoom = this.checkTermRunRoomSelect(termRunsFormGroupIds);
              if (!hasAllTermRunsSelectedRoom) {
                const message = 'Tento běh již byl alespoň jednou vybrán, a to bez místnosti.\n' +
                  'Pokud v tomto běhu chcete studenty usadit do konkrétní místnosti, určete konkrétní místnosti ' +
                  'i u všech stejných dříve vybraných běhů.';
                this.termRunError(message, index, prevTermRun);
                return;
              } else if (termRunsFormGroupIds.length === this.termRun(index).value.rooms.length) {
                const message = 'Tento běh již má vybrané všechny místnosti a není možné vybrat další.';
                this.termRunError(message, index, prevTermRun);
                return;
              } else {
                this.room(index).setValidators(Validators.required);
                this.room(index).markAsTouched();
              }
            } else {
              this.room(index).setValidators(null);
            }

            if (this.edit) {
              const existingTermRun = this.filteredFormFillData?.termRuns.find(termRun => termRun.id === newTermRun.id);
              if (existingTermRun) {
                const students = this.students(index).value;
                if (ManualPlacementFormComponent.getIndexOfLastRoomPlanWithFreeSeatsAtTheEnd(existingTermRun, students) === -1 &&
                  students.length > 0) {
                  if (existingTermRun.rooms.some(room => room.freeSeatIds.length >= this.students(index).value.length)) {
                    this.room(index).setValidators(Validators.required);
                    this.room(index).markAsTouched();
                  } else {
                    const message = 'V jakékoliv místnosti vybraného běhu není dostatečná kapacita pro usazení všech vybraných studentů.';
                    this.termRunError(message, index, prevTermRun);
                    return;
                  }
                }
                this.updateRoomsWithFreeSeatsList(existingTermRun, students, index, newTermRun);
              }
            }

            this.room(index).enable();
            this.room(index).setValue('');
            this.selectedTermRuns[index] = newTermRun;
            if (!this.edit) {
              this.selectedRoomsAbbreviations[index] = newTermRun.rooms.map(room => room.abbreviation);
              this.roomList[index] = newTermRun.rooms;
            }
            this.updateStudentsPlacementTermRun(index, newTermRun);
          }
        }
      });
  }

  private listenForStudentsChanges(formGroup: FormGroup): void {
    formGroup.controls.students.valueChanges
      .pipe(
        startWith([]),
        pairwise()
      )
      .subscribe({
        next: ([prevAllSelected, newAllSelected]: [LoginNamePayload[], LoginNamePayload[]]) => {
          const newSelected = newAllSelected.filter(loginName => !prevAllSelected.includes(loginName));
          if (newAllSelected && prevAllSelected !== newAllSelected) {
            const index = this.findFormGroupIndex(formGroup);

            if (this.edit) {
              this.removeAllSelectedStudentsFromTermRuns();
              this.fillTermRunList(newAllSelected);
              const existingTermRun = this.filteredFormFillData?.termRuns.find(tr => tr.id === this.termRun(index).value?.id);
              this.updateRoomsWithFreeSeatsList(existingTermRun, newAllSelected, index, this.termRun(index).value);
            }

            if (newSelected.length > 0) {
              if (this.room(index).value) {
                const existingRoom = this.filteredFormFillData?.termRuns.find(tr => tr.id === this.termRun(index).value.id)
                  .rooms.find(room => room.roomId === this.room(index).value.roomId);
                if (this.edit && existingRoom) {
                  if (existingRoom.freeSeatIds.length < newAllSelected.length) {
                    const message = 'Byl zvolen větší počet studentů, než je kapacita volných míst v této místnosti.';
                    this.studentsError(message, index, prevAllSelected);
                    return;
                  }
                } else if (this.room(index).value.roomPlans[this.selectedSpacingBetweenStudents].capacity < newAllSelected.length) {
                  const message = 'Byl zvolen větší počet studentů, než je kapacita vybrané místnosti';
                  this.studentsError(message, index, prevAllSelected);
                  return;
                }
              } else if (this.termRun(index).value) {
                const existingTermRun = this.filteredFormFillData?.termRuns.find(tr => tr.id === this.termRun(index).value.id);
                if (this.edit && existingTermRun) {
                  const startIndex = ManualPlacementFormComponent.getIndexOfLastRoomPlanWithFreeSeatsAtTheEnd(existingTermRun,
                    newAllSelected);
                  let freeSeatsCapacity = 0;
                  if (startIndex >= 0) {
                    for (let i = startIndex; i < existingTermRun.rooms.length; i++) {
                      freeSeatsCapacity += existingTermRun.rooms[i].freeSeatsAtTheEndOfRoom.length;
                    }
                  }
                  if (freeSeatsCapacity < newAllSelected.length || startIndex === -1) {
                    const message = 'Byl zvolen větší počet studentů, než je kapacita volných míst na konci tohoto běhu.';
                    this.studentsError(message, index, prevAllSelected);
                    return;
                  }
                } else {
                  const allRoomsCapacity = (this.termRun(index).value.rooms as FreeRoomResponsePayload[])
                    .reduce((acc, room) => acc + room.roomPlans[this.selectedSpacingBetweenStudents].capacity, 0);
                  if (allRoomsCapacity < newAllSelected.length) {
                    const message = 'Byl zvolen větší počet studentů, než je kapacita všech místností daného běhu';
                    this.studentsError(message, index, prevAllSelected);
                    return;
                  }
                }
              }
              for (let i = 0; i < this.placementFormGroups.length; i++) {                 // go through all student selectors
                if (i !== index) {                                                        // skip selector in which was change made
                  for (const student of (this.students(i).value as LoginNamePayload[])) { // go through all selected students
                    if (newSelected[0] === student) {
                      const message = `Student ${student.login} - ${student.nameAndDegrees} je již usazen v jiném běhu`;
                      this.studentsError(message, index, prevAllSelected);
                      break;
                    }
                  }
                }
              }
            }
            if (newAllSelected.length > 0 && this.room(index).value) {
              this.placeButtonDisabled[index] = false;
            }
            this.updateStudentsPlacementStudents(index, newAllSelected);
          }
        }
      });
  }

  private listenForRoomChanges(formGroup: FormGroup): void {
    formGroup.controls.room.valueChanges
      .pipe(
        startWith(''),
        pairwise()
      )
      .subscribe({
        next: ([prevSelectedRoom, newSelectedRoom]: [FreeRoomResponsePayload, FreeRoomResponsePayload]) => {
          if (prevSelectedRoom !== newSelectedRoom) {
            const index = this.findFormGroupIndex(formGroup);
            if (!newSelectedRoom) {
              this.selectedRoomsAbbreviations[index] = this.termRun(index).value.rooms.map(room => room.abbreviation);
              this.updateStudentsPlacementRoom(index, newSelectedRoom);
              return;
            }
            const termRunsFormGroupIds = this.getPreviouslySelectedTermRunsFormGroupIds(this.termRun(index).value, index);
            if (this.isSelectedSameRoom(newSelectedRoom, termRunsFormGroupIds)) {
              const message = 'Tato místnost již byla u tohoto běhu jednou vybrána. Zvolte prosím jinou';
              this.roomError(message, index, prevSelectedRoom);
              return;
            } else if (this.students(index).value.length > newSelectedRoom.roomPlans[this.selectedSpacingBetweenStudents].capacity) {
              const message = 'Do této místnosti se nevejdou všichni vybraní studenti.';
              this.roomError(message, index, prevSelectedRoom);
              return;
            }

            const existingRoom = this.filteredFormFillData?.termRuns
              .find(termRun => termRun.id === this.termRun(index).value.id)
              .rooms.find(room => room.roomId === newSelectedRoom.roomId);
            if (this.edit && existingRoom) {
              this.mandatorySelectedSeats[index] = false;
              const students = this.students(index).value;
              if (existingRoom.freeSeatsAtTheEndOfRoom.length < students.length && students.length > 0) {
                if (existingRoom.freeSeatIds.length >= students.length) {
                  this.mandatorySelectedSeats[index] = true;
                } else {
                  const message = 'V této místnosti není dostatečný počet volných míst pro usazení všech vybraných studentů.';
                  this.roomError(message, index, prevSelectedRoom);
                  return;
                }
              }
            }

            this.selectedRoomsAbbreviations[index] = [newSelectedRoom.abbreviation];
            if (this.students(index).value.length > 0) {
              this.placeButtonDisabled[index] = false;
            }
            this.updateStudentsPlacementRoom(index, newSelectedRoom);
          }
        }
      });
  }

  private findFormGroupIndex(formGroup: FormGroup): number {
    for (let i = 0; i < this.placementFormGroups.length; i++) {
      if ((this.placementFormGroups.at(i) as FormGroup) === formGroup) {
        return i;
      }
    }
    return this.placementFormGroups.length - 1;
  }

  private getPreviouslySelectedTermRunsFormGroupIds(newTermRun: TermRunPayload, currentId: number): number[] {
    const resultArray: number[] = [];
    for (let i = 0; i < this.selectedTermRuns.length; i++) {
      if (i === currentId) {
        continue;
      }
      if (this.selectedTermRuns[i] === newTermRun) {
        resultArray.push(i);
      }
    }
    return resultArray;
  }

  private checkTermRunRoomSelect(termRunsFormGroupIds: number[]): boolean {
    return termRunsFormGroupIds.every(index => this.room(index).value);
  }

  private isSelectedSameRoom(selectedRoom: FreeRoomResponsePayload, termRunsFormGroupIds: number[]): boolean {
    return termRunsFormGroupIds.some(id => this.room(id).value === selectedRoom);
  }

  private studentsError(message: string, index: number, resetValue: LoginNamePayload[]): void {
    this.snackBarService.openDangerSnackBar(message);
    this.newTermService.clearStudentsPlacementStudents(index);
    this.students(index).setValue(resetValue);
  }

  private roomError(message: string, index: number, resetValue: FreeRoomResponsePayload): void {
    this.snackBarService.openDangerSnackBar(message);
    this.newTermService.clearStudentsPlacementRoom(index);
    this.room(index).setValue(resetValue);
  }

  private termRunError(message: string, index: number, resetValue: TermRunPayload): void {
    this.snackBarService.openDangerSnackBar(message);
    this.newTermService.clearStudentsPlacementTermRun(index);
    this.termRun(index).setValue(resetValue);
  }

  private updateStudentsPlacement(index: number, students: LoginNamePayload[], termRun: TermRunPayload,
                                  room: FreeRoomResponsePayload, seats: CellPayload[]): void {
    const studentsPlacement = new StudentsPlacement(students, termRun, room, seats);
    this.newTermService.updateStudentsPlacements(studentsPlacement, index);
  }

  private updateStudentsPlacementStudents(index: number, students: LoginNamePayload[]): void {
    const termRun = this.termRun(index).value;
    const room = this.room(index).value;
    const seats = this.selectedSeatsList[index];
    this.updateStudentsPlacement(index, students, termRun, room, seats);
  }

  private updateStudentsPlacementRoom(index: number, room: FreeRoomResponsePayload): void {
    const termRun = this.termRun(index).value;
    const students = this.students(index).value;
    const seats = this.selectedSeatsList[index];
    this.updateStudentsPlacement(index, students, termRun, room, seats);
  }

  private updateStudentsPlacementTermRun(index: number, termRun: TermRunPayload): void {
    const students = this.students(index).value;
    const room = this.room(index).value;
    const seats = this.selectedSeatsList[index];
    this.updateStudentsPlacement(index, students, termRun, room, seats);
  }

  updateStudentsPlacementSeats(index: number, seats: CellPayload[]): void {
    const students = this.students(index).value;
    const termRun = this.termRun(index).value;
    const room = this.room(index).value;
    this.updateStudentsPlacement(index, students, termRun, room, seats);
  }

  private removeAllSelectedStudentsFromTermRuns(): void {
    this.filteredFormFillData = JSON.parse(JSON.stringify(this.formFillData));
    for (const termRun of this.filteredFormFillData.termRuns) {
      for (const room of termRun.rooms) {
        for (const row of room.seatingPlan) {
          for (const cell of row) {
            for (let i = 0; i < this.placementFormGroups.length; i++) {
              this.removeSelectedStudentsFromSeats(cell, this.students(i).value, room);
            }
          }
        }
        const seatsInSnakePattern = ManualPlacementFormComponent.getRoomSeatsInSnakePattern(room);
        room.freeSeatsAtTheEndOfRoom = ManualPlacementFormComponent.getFreeSeatsAtTheEndOfRoom(room, seatsInSnakePattern);
      }
    }
  }

  private removeSelectedStudentsFromSeats(cell: SeatingPlanCell, newAllSelected, room: EditTermRoomPayload): void {
    if (cell.cellType === CellType.SEAT && cell.student &&
      newAllSelected.some(loginName => loginName.login === cell.student.login)) {
      cell.student = null;
      cell.seatNumber = null;
      room.freeSeatIds.push(cell.cellId);
    }
  }

  private getSelectedSeatsFromDialog(dialog: MatDialogRef<any>, index: number): void {
    dialog.afterClosed()
      .pipe(take(1))
      .subscribe({
        next: (selectedSeats: CellPayload[]) => {
          if (selectedSeats) {
            this.selectedSeatsList[index] = selectedSeats;
            this.updateStudentsPlacementSeats(index, selectedSeats);
          }
        }
      });
  }

  private updateRoomsWithFreeSeatsList(existingTermRun: TermRunEditResponse, students, index: number, newTermRun): void {
    if (existingTermRun) {
      const roomsWithEnoughSpace = existingTermRun.rooms
        .filter(room => room.freeSeatIds.length >= students.length && students.length > 0)
        .map(room => room.roomId);
      this.roomList[index] = newTermRun.rooms.filter(room => roomsWithEnoughSpace.includes(room.roomId));
      this.selectedRoomsAbbreviations[index] = this.roomList[index].map(room => room.abbreviation);
    }
  }
}
