import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectPlaceDialogComponent } from './select-place-dialog.component';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { SharedModule } from '@shared/shared.module';

describe('SelectPlaceDialogComponent', () => {
  let component: SelectPlaceDialogComponent;
  let fixture: ComponentFixture<SelectPlaceDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [SelectPlaceDialogComponent],
      imports: [SharedModule],
      providers: [
        {
          provide: MAT_DIALOG_DATA,
          useValue: {
            studentsList: [],
            roomPlanCells: [[]],
            selectedSeats: [],
            edit: false
          }
        },
        {
          provide: MatDialogRef,
          useValue: []
        }
      ]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectPlaceDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
