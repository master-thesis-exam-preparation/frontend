import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StudentListTableComponent } from './student-list-table.component';
import { MatDialogModule } from '@angular/material/dialog';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { MatSnackBarModule } from '@angular/material/snack-bar';

describe('StudentListTableComponent', () => {
  let component: StudentListTableComponent;
  let fixture: ComponentFixture<StudentListTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [StudentListTableComponent],
      imports: [HttpClientTestingModule, MatDialogModule, MatSnackBarModule]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StudentListTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
