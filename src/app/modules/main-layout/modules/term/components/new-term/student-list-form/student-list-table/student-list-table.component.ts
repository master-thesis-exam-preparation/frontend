import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges, ViewChild } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { LoginNamePayload } from '../../../../models/login-name.payload';
import { DialogService } from '@core/services/dialog.service';
import { ConfirmDialogOptionsData } from '@shared/modules/dialog/confirm-dialog/confirm-dialog.component';
import { SnackBarService } from '@core/services';
import { EditTermService } from '../../../../services/edit-term.service';
import { NewTermService } from '../../../../services/new-term.service';
import { StudentsPlacement } from '../../../../models/students-placement';

/**
 * Component for displaying the table of students from uploaded file.
 */
@Component({
  selector: 'app-student-list-table',
  templateUrl: './student-list-table.component.html',
  styleUrls: ['./student-list-table.component.scss']
})
export class StudentListTableComponent implements OnChanges {

  /**
   * List of students to display.
   */
  @Input() tableDataSource: LoginNamePayload[] = [];

  /**
   * Message to display when no students are in the table.
   */
  @Input() noDataMessage: string;

  /**
   * Determines whether column with actions should be displayed.
   */
  @Input() showAction = false;

  /**
   * Outputs an event every time some changes occurs in students table.
   */
  @Output() studentPlacementsChange = new EventEmitter<{ placement: StudentsPlacement, index: number }>();

  /**
   * Sets MatSort to work even in ngIf
   * @param sort  MatSort to set
   */
  @ViewChild(MatSort, {static: false}) set sort(sort: MatSort) {
    this.studentsDataSource.sort = sort;
  }

  /**
   * Sets MatPaginator to work even in ngIf
   * @param paginator  MatPaginator to set
   */
  @ViewChild(MatPaginator, {static: false}) set paginator(paginator: MatPaginator) {
    this.studentsDataSource.paginator = paginator;
  }

  headerColumns: string[];
  studentsDataSource: MatTableDataSource<LoginNamePayload> = new MatTableDataSource<LoginNamePayload>();

  constructor(private dialogService: DialogService,
              private editTermService: EditTermService,
              private snackBarService: SnackBarService,
              private newTermService: NewTermService) {
  }

  /**
   * Update student table everytime input list of students change.
   * @param changes  changes in inputs
   */
  ngOnChanges(changes: SimpleChanges): void {
    this.studentsDataSource.data = this.tableDataSource;
    if (this.showAction) {
      this.headerColumns = ['login', 'nameAndDegrees', 'action'];
    } else {
      this.headerColumns = ['login', 'nameAndDegrees'];
    }
  }

  /**
   * Takes filter value from input and apply it to list of rooms.
   * @param event  filter key press event
   */
  applyFilter(event: Event): void {
    const filterValue = (event.target as HTMLInputElement).value;
    this.studentsDataSource.filter = filterValue.trim().toLowerCase();

    if (this.studentsDataSource.paginator) {
      this.studentsDataSource.paginator.firstPage();
    }
  }

  /**
   * Shows confirmation dialog before removing the student.
   * @param studentData  information about the student that should be removed
   */
  showConfirmDialog(studentData: LoginNamePayload): void {
    const options: ConfirmDialogOptionsData = {
      title: 'Odebrat studenta?',
      message: `Opravdu chcete odebrat studenta "${studentData.login} - ${studentData.nameAndDegrees} z termínu zkoušky"?`,
      cancelText: 'ZRUŠIT',
      cancelColor: 'primary',
      confirmText: 'ODSTRANIT',
      confirmColor: 'warn'
    };
    this.dialogService.openConfirmDialog$(options)
      .subscribe(confirmed => {
        if (confirmed) {
          this.removeStudent(studentData.login);
          this.editTermService.setExistingStudentList(this.studentsDataSource.data);
        }
      });
  }

  private removeStudent(login: string): void {
    this.newTermService.studentPlacementsValue
      .forEach((placement, index) => {
        const placementStudents = placement.students;
        if (placementStudents.some(student => student.login === login)) {
          placement.students = placementStudents.filter(student => student.login !== login);
          this.studentPlacementsChange.emit({placement, index});
        }
      });

    this.studentsDataSource.data = this.studentsDataSource.data.filter(student => student.login !== login);
    this.snackBarService.openSuccessSnackBar('Student odebrán');
  }
}
