import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DistributionAndInfoFormComponent } from './distribution-and-info-form.component';
import { FormBuilder } from '@angular/forms';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { AppModule } from '../../../../../../../app.module';
import { SharedModule } from '@shared/shared.module';

describe('DistributionAndInfoFormComponent', () => {
  let component: DistributionAndInfoFormComponent;
  let fixture: ComponentFixture<DistributionAndInfoFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [DistributionAndInfoFormComponent],
      imports: [HttpClientTestingModule, RouterTestingModule, SharedModule, AppModule],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            snapshot: {
              paramMap: {
                get(): number {
                  return 1;
                }
              }
            }
          }
        }
      ]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DistributionAndInfoFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
