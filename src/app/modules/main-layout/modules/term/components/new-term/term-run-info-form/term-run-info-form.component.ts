import { ChangeDetectorRef, Component, Input, OnChanges, OnDestroy, OnInit, SimpleChanges } from '@angular/core';
import { AbstractControl, FormArray, FormBuilder, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { SnackBarService } from '@core/services';
import { SpacingBetweenStudents } from '../../../spacing-between-students.enum';
import { RoomService } from '../../../../rooms/services/room.service';
import { debounceTime, pairwise, startWith, take, takeUntil } from 'rxjs/operators';
import { merge, Observable, Subject, Subscriber } from 'rxjs';
import { NewTermService } from '../../../services/new-term.service';
import { FreeRoomResponsePayload } from '../../../../course/models/free-room-response.payload';
import { FreeRoomRequestPayload } from '../../../../course/models/free-room-request.payload';
import { TermRunPayload } from '../../../models/term-run.payload';
import { AssignmentService } from '../../../../assignment/services/assignment.service';
import { BasicAssignmentInfo } from '../../../../assignment/models/basic-assignment-info';
import { EditTermInfoResponse } from '../../../models/term/edit-term-info-response.payload';
import { ActivatedRoute } from '@angular/router';
import { ConfirmDialogOptionsData } from '@shared/modules/dialog/confirm-dialog/confirm-dialog.component';
import { DialogService } from '@core/services/dialog.service';
import { EditTermService } from '../../../services/edit-term.service';
import { TermRunEditResponse } from '../../../models/term-run-edit-response.payload';

/**
 * Component for displaying the page with a step of a new term form for creating term runs of the new term.
 */
@Component({
  selector: 'app-term-run-info-form',
  templateUrl: './term-run-info-form.component.html',
  styleUrls: ['./term-run-info-form.component.scss']
})
export class TermRunInfoFormComponent implements OnInit, OnDestroy, OnChanges {

  constructor(private newTermService: NewTermService,
              private fb: FormBuilder,
              private changeDetector: ChangeDetectorRef,
              private roomService: RoomService,
              private assignmentService: AssignmentService,
              private snackBarService: SnackBarService,
              private route: ActivatedRoute,
              private dialogService: DialogService,
              private editTermService: EditTermService) {
    this.spacingKeys = Object.keys(SpacingBetweenStudents);
  }

  /**
   * Gets value of spacing select menu in the form.
   */
  get spacing(): AbstractControl {
    return this.termRunInfoForm.get('spacing');
  }

  /**
   * Gets termRunFormGroups form array from the form.
   */
  get termRunFormGroups(): FormArray {
    return this.termRunInfoForm.get('termRunFormGroups') as FormArray;
  }

  /**
   * Determines whether form is for creating new term run or editing an existing one.
   */
  @Input() edit = false;

  /**
   * Data about existing term.
   */
  @Input() formFillData: EditTermInfoResponse;

  spacingKeys: string[];
  selectedSpacingBetweenStudents = 0;

  numberOfStudentsInList: number;

  termRunInfoForm: FormGroup;
  isLoading: boolean;
  isRefreshButtonEnabled: boolean[] = [false];

  freeRoomLists: FreeRoomResponsePayload[][] = [[]];
  filteredFreeRoomLists: FreeRoomResponsePayload[][] = [[]];
  selectedFreeRoomLists: FreeRoomResponsePayload[][] = [[]];

  availableAssignments: BasicAssignmentInfo[] = [];
  assignmentsLists: BasicAssignmentInfo[][] = [];
  filteredAssignmentsLists: BasicAssignmentInfo[][] = [];
  selectedAssignmentsLists: BasicAssignmentInfo[][] = [];

  numberOfStudentsInRuns: number[] = [0];

  numberOfSeatedStudents: number;
  numberOfRemainingStudents: number;

  private termRunIdCounter = 0;
  private termRunGroupIdCounter = 0;
  private initialized = false;
  private editDefaultTermRunsInitialized = false;

  private termRunIds: number[] = [];
  private termIdsIndexCounter = 0;

  private ngUnsubscribe$ = new Subject();

  private static isTermRunsSubsequent(t1: TermRunEditResponse, t2: TermRunEditResponse): boolean {
    if (t1 === undefined) {
      return false;
    }
    const isSameAssignments = JSON.stringify(t2.assignments) === JSON.stringify(t1.assignments);
    const isSameRooms = JSON.stringify(t2.rooms) === JSON.stringify(t1.rooms);
    const isSameTermRunLength = (t2.endTime - t2.startTime) === (t1.endTime - t1.startTime);
    const subsequent = t1.endTime === t2.startTime;
    return isSameAssignments && isSameRooms && isSameTermRunLength && subsequent;
  }

  /**
   * Gets value of the startDateTime input in the termRunFormGroups form array.
   * @param index  index of the group
   */
  startDateTime(index: number): AbstractControl {
    return this.termRunFormGroups.controls[index].get('startDateTime');
  }

  /**
   * Gets value of the termRunLength input in the termRunFormGroups form array.
   * @param index  index of the group
   */
  termRunLength(index: number): AbstractControl {
    return this.termRunFormGroups.controls[index].get('termRunLength');
  }

  /**
   * Gets value of the termRunGroupId in the termRunFormGroups form array.
   * @param index  index of the group
   */
  termRunGroupId(index: number): AbstractControl {
    return this.termRunFormGroups.controls[index].get('termRunGroupId');
  }

  /**
   * Gets value of the numberOfSubsequentRuns input in the termRunFormGroups form array.
   * @param index  index of the group
   */
  numberOfSubsequentRuns(index: number): AbstractControl {
    return this.termRunFormGroups.controls[index].get('numberOfSubsequentRuns');
  }

  /**
   * Gets values of the selectedRooms select menu in the termRunFormGroups form array.
   * @param index  index of the group
   */
  selectedRooms(index: number): AbstractControl {
    return this.termRunFormGroups.controls[index].get('selectedRooms');
  }

  /**
   * Gets values of the selectedAssignments select menu in the termRunFormGroups form array.
   * @param index  index of the group
   */
  selectedAssignments(index: number): AbstractControl {
    return this.termRunFormGroups.controls[index].get('selectedAssignments');
  }

  /**
   * Sets validators to the form inputs. Fills edit form with existing data.
   */
  ngOnInit(): void {
    const now = new Date();
    now.setHours(now.getHours() + 1);
    let fbArray = [];
    if (!this.edit) {
      this.assignmentService.getAllAssignmentsInfo$()
        .subscribe({
          next: assignments => {
            this.availableAssignments = assignments;
            this.filteredAssignmentsLists.push(assignments);
            this.assignmentsLists.push(assignments);
            this.selectedAssignments(0).setValue([]);
            this.initialized = true;
          }
        });
      const newGroup = this.createTermRunForm(now, null);
      this.initFormGroupListeners(newGroup);
      fbArray = [newGroup];
    }
    this.termRunInfoForm = this.fb.group({
      spacing: [this.spacingKeys[0], [
        Validators.required
      ]],
      termRunFormGroups: this.fb.array(fbArray)
    });

    this.spacing.valueChanges
      .subscribe({
        next: spacing => {
          if (+spacing === this.selectedSpacingBetweenStudents) {
            return;
          }
          const placementsWithSelectedSeats = this.newTermService.studentPlacementsValue.filter(sp => sp.seats?.length > 0);
          if (placementsWithSelectedSeats.length > 0) {
            const options: ConfirmDialogOptionsData = {
              title: 'Změnit počet volných míst mezi studenty?',
              message: `Máte vytvořena manuální usazení na konkrétní místa. ` +
                `Změnou dojde k odebrání těchto míst a bude zapotřebí místa znovu vybrat. ` +
                `Opravdu chcete změnit počet volných míst mezi studenty? Toto rozhodnutí nelze vzít zpět.`,
              cancelText: 'ZRUŠIT',
              cancelColor: 'primary',
              confirmText: 'ZMĚNIT',
              confirmColor: 'warn'
            };
            this.dialogService.openConfirmDialog$(options)
              .subscribe(confirmed => {
                if (confirmed) {
                  this.newTermService.setSelectedSpacing(spacing);
                  this.selectedSpacingBetweenStudents = +spacing;
                  this.updateStudentNumbersAtAllTermRuns();
                } else {
                  this.spacing.setValue(this.selectedSpacingBetweenStudents.toString());
                }
              });
          } else {
            this.newTermService.setSelectedSpacing(spacing);
            this.selectedSpacingBetweenStudents = +spacing;
            this.updateStudentNumbersAtAllTermRuns();
          }
        }
      });

    this.newTermService.getStudentList$()
      .pipe(takeUntil(this.ngUnsubscribe$))
      .subscribe({
        next: studentList => {
          this.numberOfStudentsInList = studentList.length;
          this.setNumberOfRemainingStudents();
        }
      });

    this.newTermService.getNumberOfRemainingStudents$()
      .pipe(takeUntil(this.ngUnsubscribe$))
      .subscribe({
        next: numberOfRemainingStudents => {
          this.numberOfRemainingStudents = numberOfRemainingStudents;
        }
      });
  }

  /**
   * Unsubscribes from everything before destroying the component.
   */
  ngOnDestroy(): void {
    this.ngUnsubscribe$.next();
    this.ngUnsubscribe$.complete();
  }

  /**
   * Update everything everytime something on input change.
   * @param changes  changes in inputs
   */
  ngOnChanges(changes: SimpleChanges): void {
    if (changes.formFillData?.currentValue) {
      if (!this.initialized) {
        this.assignmentService.getAllAssignmentsInfo$()
          .subscribe({
            next: assignments => {
              this.availableAssignments = assignments;
              this.addExistingTermRunFormGroups(this.formFillData.termRuns);
              this.initialized = true;
            }
          });
      }
      this.spacing.setValue(SpacingBetweenStudents[this.formFillData.spacingBetweenStudents].toString());
    }
  }

  /**
   * Adds new term run group to form array.
   */
  addTermRunFormGroup(): void {
    this.numberOfStudentsInRuns.push(0);
    this.freeRoomLists.push([]);
    this.filteredFreeRoomLists.push([]);
    this.selectedFreeRoomLists.push([]);

    this.assignmentsLists.push(this.availableAssignments);
    this.filteredAssignmentsLists.push(this.availableAssignments);
    this.selectedAssignmentsLists.push([]);

    const previousFormIndex = this.termRunFormGroups.length - 1;
    const length = this.termRunLength(previousFormIndex).value;
    const numberOfRuns = this.numberOfSubsequentRuns(previousFormIndex).value;
    const oldStartTime = this.startDateTime(previousFormIndex);
    let newDate = null;
    if (oldStartTime.valid) {
      newDate = new Date(oldStartTime.value);
      newDate.setMinutes(newDate.getMinutes() + length * numberOfRuns);
    }

    const newGroup = this.createTermRunForm(newDate, length);
    this.termRunFormGroups.push(newGroup);
    this.initFormGroupListeners(newGroup);
    this.isRefreshButtonEnabled.push(newGroup.valid);
  }

  /**
   * Add existing term run group to form array
   * @param termRuns  existing term runs
   */
  addExistingTermRunFormGroups(termRuns: TermRunEditResponse[]): void {
    let lastNewGroupIndex: number = null;
    for (let i = 0; i < termRuns.length; i++) {
      const isSubsequent = TermRunInfoFormComponent.isTermRunsSubsequent(termRuns[i - 1], termRuns[i]);
      if (!isSubsequent) {
        if (i !== 0) {
          this.initFormGroupListeners(this.termRunFormGroups.at(lastNewGroupIndex) as FormGroup);
        }
        this.numberOfStudentsInRuns.push(0);
        this.freeRoomLists.push([]);
        this.filteredFreeRoomLists.push([]);
        this.selectedFreeRoomLists.push([]);

        this.assignmentsLists.push(this.availableAssignments);
        this.filteredAssignmentsLists.push(this.availableAssignments);
        this.selectedAssignmentsLists.push([]);

        const newGroup = this.createTermRunForm(new Date(termRuns[i].startTime),
          Math.floor((termRuns[i].endTime - termRuns[i].startTime) / 60000));

        const selectedAssignments = this.availableAssignments.filter(assignment =>
          termRuns[i].assignments.includes(assignment.assignmentId)
        );
        newGroup.get('selectedAssignments').setValue(selectedAssignments);

        this.termRunFormGroups.push(newGroup);

        this.isRefreshButtonEnabled.push(newGroup.valid);
        const newGroupIndex = this.findFormGroupIndex(newGroup);
        this.findFreeRooms(newGroupIndex, termRuns[i]);
        lastNewGroupIndex = newGroupIndex;
      } else {
        const value = this.numberOfSubsequentRuns(lastNewGroupIndex).value;
        this.numberOfSubsequentRuns(lastNewGroupIndex).setValue(value + 1);
      }
      this.termRunIds[i] = termRuns[i].id;
      this.termRunIdCounter = termRuns[i].id + 1;
    }
    this.initFormGroupListeners(this.termRunFormGroups.at(lastNewGroupIndex) as FormGroup);
  }

  /**
   * Removes existing term run group from form array.
   * @param index  index of the term run form group
   */
  removeTermRunFormGroup(index: number): void {
    this.newTermService.getStudentsPlacements$()
      .pipe(take(1))
      .subscribe({
        next: studentPlacements => {
          const termUsed = studentPlacements.some(placement => placement.termRun.termRunGroupId === this.termRunGroupId(index).value);
          if (termUsed) {
            this.snackBarService.openDangerSnackBar('Tuto skupinu běhů nelze smazat, jelikož v některém z běhu této skupiny je ' +
              'vytvořeno manuální usazení. Pokud chcete skupinu odstranit, odstraňte i odpovídající manuální usazení');
          } else {
            this.termRunFormGroups.removeAt(index);
            this.numberOfStudentsInRuns.splice(index, 1);
            this.freeRoomLists.splice(index, 1);
            this.filteredFreeRoomLists.splice(index, 1);
            this.selectedFreeRoomLists.splice(index, 1);
            this.assignmentsLists.splice(index, 1);
            this.filteredAssignmentsLists.splice(index, 1);
            this.selectedAssignmentsLists.splice(index, 1);
            this.isRefreshButtonEnabled.splice(index, 1);
            this.newTermService.removeTermRuns(index);
            this.setNumberOfRemainingStudents();
          }
        }
      });
  }

  /**
   * Finds free rooms in given time window.
   * @param index  index of the group
   * @param termRun  information about new term run
   */
  findFreeRooms(index: number, termRun: TermRunEditResponse): void {
    if ((this.startDateTime(index).valid && this.termRunLength(index).valid && this.numberOfSubsequentRuns(index).valid) || !!termRun) {
      this.isLoading = true;
      let termId = null;
      if (this.edit) {
        termId = Number(this.route.snapshot.paramMap.get('id'));
      }
      const freeRoomRequest: FreeRoomRequestPayload = {
        numberOfTermRuns: this.numberOfSubsequentRuns(index).value,
        startTime: new Date(this.startDateTime(index).value).getTime(),
        termRunLength: this.termRunLength(index).value,
        termId
      };
      this.roomService.getFreeRoomsInfo$(freeRoomRequest)
        .subscribe({
          next: rooms => {
            this.setRoomPlansToSelectInput(rooms, index);
            this.isLoading = false;
            this.isRefreshButtonEnabled[index] = false;
            if (this.edit) {
              const selectedRooms = this.freeRoomLists[index]
                .filter(room => termRun.rooms.some(editRoom => editRoom.roomId === room.roomId));
              this.selectedRooms(index).setValue(selectedRooms);
            }
          }
        });
    } else {
      if (termRun && this.startDateTime(index).hasError('invalidDate')) {
        this.snackBarService.openInfoSnackBar('Některý z běhů tohoto termínu zkoušky již započal. Aby bylo možné změny uložit, je nutné ' +
          'upravit jeho čas zahájení.');
      } else {
        this.snackBarService.openDangerSnackBar('Nelze načíst volné místnosti. ' +
          'Zkontrolujte, zda jsou správně vyplněné údaje ve formuláři a opakujte pokus.');
      }
    }
  }

  /**
   * Filter free rooms on each key press in filter text input.
   * @param event  key press event
   * @param index  index of the group
   */
  onKey(event: Event, index: number): void {
    const filterValue = (event.target as HTMLInputElement).value;
    this.filteredFreeRoomLists[index] = this.search(filterValue, index);
  }

  /**
   * Filter assignments on each key press in filter text input.
   * @param event  key press event
   * @param index  index of the group
   */
  onKeyAssignment(event: Event, index: number): void {
    const filterValue = (event.target as HTMLInputElement).value;
    this.filteredAssignmentsLists[index] = this.searchAssignment(filterValue, index);
  }

  /**
   * Compares two room based on theirs IDs
   * @param o1  room 1
   * @param o2  room 2
   */
  compareFunctionRooms(o1: FreeRoomResponsePayload, o2: FreeRoomResponsePayload): boolean {
    if (o1 && o2) {
      return (o1.roomId === o2.roomId);
    }
    return false;
  }

  /**
   * Compares two assignments based on theirs IDs and names
   * @param o1  assignment 1
   * @param o2  assignment 2
   */
  compareFunctionAssignments(o1: BasicAssignmentInfo, o2: BasicAssignmentInfo): boolean {
    return (o1.assignmentId === o2.assignmentId && o1.name === o2.name);
  }

  /**
   * Resets rooms filter.
   * @param opened  true if select menu is opened
   * @param index  index of the group in form array
   */
  clearFilterRooms(opened: boolean, index: number): void {
    if (!opened) {
      this.filteredFreeRoomLists[index] = this.freeRoomLists[index];
    }
  }

  /**
   * Resets assignments filter.
   * @param opened  true if select menu is opened
   * @param index  index of the group in form array
   */
  clearFilterAssignments(opened: boolean, index: number): void {
    if (!opened) {
      this.filteredAssignmentsLists[index] = this.assignmentsLists[index];
    }
  }

  private createTermRunForm(defaultDate: Date, defaultLength: number): FormGroup {
    let parsedDate = null;
    if (defaultDate) {
      parsedDate = new Date(defaultDate.getTime() - (defaultDate.getTimezoneOffset() * 60000))
        .toISOString()
        .slice(0, 16);
    }
    return this.fb.group({
      startDateTime: [parsedDate, [
        Validators.required,
        Validators.maxLength(255),
        this.timeValidator({invalidDate: true})
      ]],
      termRunLength: [defaultLength, [
        Validators.required,
        Validators.min(1),
        Validators.max(65535)
      ]],
      numberOfSubsequentRuns: [1, [
        Validators.required,
        Validators.min(1),
        Validators.max(100)
      ]],
      selectedRooms: [{value: [] as FreeRoomResponsePayload[], disabled: true}, [
        Validators.required
      ]],
      selectedAssignments: [[], []],
      termRunGroupId: [this.termRunGroupIdCounter++, []]
    });
  }

  private timeValidator(error: ValidationErrors): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } => {
      if (!control.value) {
        // if control is empty return no error
        return null;
      }
      // test the value of the control
      const pickedTime = new Date(control.value);
      const currentTime = new Date();
      const valid = pickedTime >= currentTime;

      // if true, return no error (no error), else return error passed in the second parameter
      return valid ? null : error;
    };
  }

  private initFormGroupListeners(formGroup: FormGroup): void {
    this.initListenForFormExceptRoomsChanges(formGroup);
    this.initListenForRoomsChanges(formGroup);
    this.initListenForAssignmentChanges(formGroup);
  }

  private setRoomPlansToSelectInput(rooms: FreeRoomResponsePayload[], index: number): void {
    this.freeRoomLists[index] = rooms;
    this.filteredFreeRoomLists[index] = rooms;
    this.selectedFreeRoomLists[index] = [];

    this.numberOfStudentsInRuns[index] = 0;
    if (rooms.length > 0) {
      this.selectedRooms(index).enable();
    } else {
      this.snackBarService.openInfoSnackBar('Nebyly nalezeny žádné volné místnosti');
      this.selectedRooms(index).disable();
    }
  }

  private search(value: string, index: number): FreeRoomResponsePayload[] {
    const filter = value.trim().toLowerCase();
    return this.freeRoomLists[index].filter(roomPlan => roomPlan.name.toLowerCase().startsWith(filter) ||
      roomPlan.abbreviation.toLowerCase().startsWith(filter));
  }

  private searchAssignment(value: string, index: number): BasicAssignmentInfo[] {
    const filter = value.trim().toLowerCase();
    return this.assignmentsLists[index].filter(assignment => assignment.name.toLowerCase().startsWith(filter));
  }

  private setNumberOfRemainingStudents(): void {
    this.numberOfSeatedStudents = this.numberOfStudentsInRuns.reduce((acc, num) => acc + num, 0);
    let numberOfExistingStudents = this.editTermService.existingStudentListValue?.length;
    if (!numberOfExistingStudents) {
      numberOfExistingStudents = 0;
    }
    this.newTermService.setNumberOfRemainingStudents(
      (this.numberOfStudentsInList + numberOfExistingStudents) - this.numberOfSeatedStudents
    );
  }

  private updateStudentNumbers(formGroup: FormGroup, index: number): void {
    const selectedRoomPlans = formGroup.controls.selectedRooms?.value;
    if (!!selectedRoomPlans) {
      this.numberOfStudentsInRuns[index] =
        (selectedRoomPlans as FreeRoomResponsePayload[]).reduce((acc, room) =>
          acc + (room.roomPlans[this.selectedSpacingBetweenStudents].capacity * formGroup.controls.numberOfSubsequentRuns.value), 0
        );
    } else {
      this.numberOfStudentsInRuns[index] = 0;
    }
    this.setNumberOfRemainingStudents();
  }

  private initListenForFormExceptRoomsChanges(formGroup: FormGroup): void {
    const o1 = formGroup.controls.startDateTime.valueChanges;
    const o2 = formGroup.controls.termRunLength.valueChanges;
    const o3 = formGroup.controls.numberOfSubsequentRuns.valueChanges;
    merge(o1, o2, o3)
      .pipe(
        debounceTime(250)
      )
      .subscribe({
        next: () => {
          const index = this.findFormGroupIndex(formGroup);
          formGroup.controls.selectedRooms.setValue(undefined); // possible feature: emitEvent: false
          this.freeRoomLists[index] = [];
          this.filteredFreeRoomLists[index] = [];
          this.selectedFreeRoomLists[index] = [];
          this.isRefreshButtonEnabled[index] = true;
        }
      });
  }

  private initListenForRoomsChanges(formGroup: FormGroup): void {
    formGroup.controls.selectedRooms.valueChanges
      .pipe(
        startWith(''),
        pairwise(),
        debounceTime(250)
      )
      .subscribe({
        next: ([oldSelectedRooms, newSelectedRooms]: [FreeRoomResponsePayload[], FreeRoomResponsePayload[]]) => {
          const index = this.findFormGroupIndex(formGroup);
          const v1 = formGroup.controls.startDateTime.valid;
          const v2 = formGroup.controls.termRunLength.valid;
          const v3 = formGroup.controls.numberOfSubsequentRuns.valid;
          if (!!formGroup.controls.selectedRooms.value && (v1 && v2 && v3) && formGroup.controls.selectedRooms.value.length > 0) {
            this.createTermRuns(index)
              .pipe(take(1))
              .subscribe({
                next: (result: boolean) => {
                  if (!result) {
                    this.selectedRooms(index).setValue(oldSelectedRooms);   // possible feature: emitEvent: false
                    this.selectedFreeRoomLists[index] = oldSelectedRooms;
                  }
                }
              });
          }
          this.updateStudentNumbers(formGroup, index);
        }
      });
  }

  private initListenForAssignmentChanges(formGroup: FormGroup): void {
    formGroup.controls.selectedAssignments.valueChanges
      .subscribe({
        next: assignments => {
          const index = this.findFormGroupIndex(formGroup);
          const v1 = formGroup.controls.startDateTime.valid;
          const v2 = formGroup.controls.termRunLength.valid;
          const v3 = formGroup.controls.numberOfSubsequentRuns.valid;
          if (!!formGroup.controls.selectedRooms.value && (v1 && v2 && v3) && formGroup.controls.selectedRooms.value.length > 0) {
            this.createTermRuns(index)
              .pipe(take(1))
              .subscribe();
          }
        }
      });
  }

  private updateStudentNumbersAtAllTermRuns(): void {
    for (let index = 0; index < this.termRunFormGroups.length; index++) {
      this.updateStudentNumbers(this.termRunFormGroups.at(index) as FormGroup, index);
    }
  }

  private isThereAnyCollisions$(newTermRuns: TermRunPayload[], termGroupId: number): Observable<boolean> {
    return new Observable((observer: Subscriber<boolean>): void => {
      this.newTermService.getTermRuns$()
        .pipe(take(1))
        .subscribe({
          next: otherTermRuns => {
            const newTermRunsStartTime = newTermRuns[0].startTime;
            const newTermRunsEndTime = newTermRuns[newTermRuns.length - 1].endTime;
            const newTermRunsRoomIds = newTermRuns[0].rooms.map(room => room.roomId);
            otherTermRuns = otherTermRuns.filter(tr => tr.termRunGroupId !== termGroupId);
            for (const tr of otherTermRuns) {
              const trRoomIds = tr.rooms.map(room => room.roomId);
              const sameRoom = trRoomIds.some(trRoomId => newTermRunsRoomIds.includes(trRoomId));
              if (sameRoom) {
                const startTimeOverlap = (tr.startTime > newTermRunsStartTime && tr.startTime < newTermRunsEndTime);
                const endTimeOverlap = (tr.endTime > newTermRunsStartTime && tr.endTime < newTermRunsEndTime);
                const insideOverlap = (tr.startTime < newTermRunsStartTime && tr.endTime > newTermRunsEndTime);
                const sameOverlap = (tr.startTime === newTermRunsStartTime && tr.endTime === newTermRunsEndTime);
                if (startTimeOverlap || endTimeOverlap || insideOverlap || sameOverlap) {
                  observer.next(true);
                  observer.complete();
                  break;
                }
              }
            }
            observer.next(false);
            observer.complete();
          }
        });
    });
  }

  private createTermRuns(index: number): Observable<boolean> {
    return new Observable((observer: Subscriber<boolean>): void => {
      const length = this.termRunLength(index).value;
      const termRunGroupId = this.termRunGroupId(index).value;
      const termRuns: TermRunPayload[] = [];
      let startTime = new Date(this.startDateTime(index).value);

      for (let termRunCounter = 0; termRunCounter < this.numberOfSubsequentRuns(index).value; termRunCounter++) {
        const endTime = new Date(startTime);
        endTime.setMinutes(startTime.getMinutes() + length);
        let id;
        if (this.edit && !this.editDefaultTermRunsInitialized) {
          id = this.termRunIds[this.termIdsIndexCounter++];
        } else {
          id = this.newTermService.getFormGroupTermRunsValue(termRunGroupId)?.[termRunCounter]?.id;
          if (id == null) {
            id = this.termRunIdCounter++;
          }
        }
        termRuns.push({
          termRunGroupId,
          id,
          startTime: startTime.getTime(),
          endTime: endTime.getTime(),
          rooms: this.selectedRooms(index).value,
          assignments: this.selectedAssignments(index).value
        });
        startTime = endTime;
      }

      this.isThereAnyCollisions$(termRuns, termRunGroupId)
        .subscribe({
          next: isCollision => {
            if (!isCollision) {
              this.newTermService.updateTermRuns(termRuns, index);
              if (this.edit && index === this.termRunFormGroups.length - 1) {
                this.editDefaultTermRunsInitialized = true;
              }
              observer.next(true);
              observer.complete();
            } else {
              this.snackBarService.openDangerSnackBar('Kolize s jinými vámi vytvořenými běhy.\n' +
                'Zkontrolujte, zda se běhy nekonají ve stejný čas ve stejné místnosti');
              observer.next(false);
              observer.complete();
            }
          }
        });
    });
  }

  private findFormGroupIndex(formGroup: FormGroup): number {
    for (let i = 0; i < this.termRunFormGroups.length; i++) {
      if ((this.termRunFormGroups.at(i) as FormGroup) === formGroup) {
        return i;
      }
    }
    return this.termRunFormGroups.length - 1;
  }
}
