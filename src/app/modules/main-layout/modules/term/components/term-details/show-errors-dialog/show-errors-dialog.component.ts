import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { GeneratingFail } from '../../../models/generating-fail.payload';
import { TermService } from '../../../services/term.service';

/**
 * Component for displaying the popup window with information about errors during assignment generation.
 */
@Component({
  selector: 'app-show-errors-dialog',
  templateUrl: './show-errors-dialog.component.html',
  styleUrls: ['./show-errors-dialog.component.scss']
})
export class ShowErrorsDialogComponent {

  selectedFileContent: string[] = [null];
  displayLogFileContent: boolean[] = [false];

  constructor(@Inject(MAT_DIALOG_DATA) public fails: GeneratingFail[],
              private termService: TermService) {
  }

  /**
   * Displays content of log file with error.
   * @param testScheduleId  unique ID of the test schedule for which generating failed.
   * @param index  index in the list of errors
   */
  showLogFileContent(testScheduleId: number, index: number): void {
    this.termService.getLogFileContent$(testScheduleId)
      .subscribe({
        next: logFileContent => {
          this.selectedFileContent[index] = logFileContent.fileContent;
          this.displayLogFileContent[index] = true;
        }
      });
  }

  /**
   * Hides displayed error log file content.
   * @param index  index in the list of errors
   */
  hideSelectedFileContent(index: number): void {
    this.displayLogFileContent[index] = false;
  }
}
