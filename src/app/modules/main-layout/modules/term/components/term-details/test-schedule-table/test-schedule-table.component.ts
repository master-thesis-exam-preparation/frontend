import { Component, Input, OnChanges, SimpleChanges, ViewChild } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { TestSchedule } from '../../../models/test-schedule.payload';


/**
 * Component for displaying the table with test schedules of specific room plan in specific term run of specific term.
 */
@Component({
  selector: 'app-test-schedule-table',
  templateUrl: './test-schedule-table.component.html',
  styleUrls: ['./test-schedule-table.component.scss']
})
export class TestScheduleTableComponent implements OnChanges {

  /**
   * Sets MatSort to work even in ngIf
   * @param sort  MatSort to set
   */
  @ViewChild(MatSort, {static: false}) set sort(sort: MatSort) {
    this.schedulesDataSource.sort = sort;
  }

  /**
   * Sets MatPaginator to work even in ngIf
   * @param paginator  MatPaginator to set
   */
  @ViewChild(MatPaginator, {static: false}) set paginator(paginator: MatPaginator) {
    this.schedulesDataSource.paginator = paginator;
  }

  /**
   * Id of the term
   */
  @Input() termId: number;

  /**
   * List of test shedules to display
   */
  @Input() testSchedules: TestSchedule[];

  /**
   * Determines whether generate assignment buttons in the table should be disabled
   */
  @Input() disableGenerateButtons = false;

  headerColumns: string[];
  schedulesDataSource: MatTableDataSource<TestSchedule> = new MatTableDataSource<TestSchedule>();
  showAction = false;

  constructor() {
  }

  /**
   * Update test schedules in table everytime something on input change.
   * @param changes  changes in inputs
   */
  ngOnChanges(changes: SimpleChanges): void {
    this.schedulesDataSource.data = this.testSchedules;
    this.showAction = this.isThereAnyAssignment();
    if (this.showAction) {
      this.headerColumns = ['seatNumber', 'login', 'nameAndDegrees', 'orderNumber', 'assignment', 'action'];
    } else {
      this.headerColumns = ['seatNumber', 'login', 'nameAndDegrees', 'orderNumber', 'assignment'];
    }
  }

  private isThereAnyAssignment(): boolean {
    return this.testSchedules.some(testSchedule => testSchedule.assignment);
  }
}
