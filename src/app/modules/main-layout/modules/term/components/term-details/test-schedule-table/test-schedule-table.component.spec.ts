import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TestScheduleTableComponent } from './test-schedule-table.component';

describe('TestScheduleTableComponent', () => {
  let component: TestScheduleTableComponent;
  let fixture: ComponentFixture<TestScheduleTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [TestScheduleTableComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TestScheduleTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
