import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { combineLatest, Observable, Subject, throwError } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService, SnackBarService } from '@core/services';
import { TermService } from '../../services/term.service';
import { catchError, takeUntil } from 'rxjs/operators';
import { TermDetailsResponsePayload } from '../../models/term-details-response.payload';
import { LoggedUserInfo } from '@core/logged-user-info';
import { ConfirmDialogOptionsData } from '@shared/modules/dialog/confirm-dialog/confirm-dialog.component';
import { DialogService } from '@core/services/dialog.service';
import { TestSchedule } from '../../models/test-schedule.payload';
import { SeatingPlanPayload } from '../../models/seating-plan.payload';
import { TermRunDetailsPayload } from '../../models/term-run-details.payload';
import { MatDialog } from '@angular/material/dialog';
import { ShowErrorsDialogComponent } from './show-errors-dialog/show-errors-dialog.component';
import { saveAs } from 'file-saver';

/**
 * Component for displaying the page with details of specific term.
 */
@Component({
  selector: 'app-term-details',
  templateUrl: './term-details.component.html',
  styleUrls: ['./term-details.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class TermDetailsComponent implements OnInit, OnDestroy {

  termDetails: TermDetailsResponsePayload;
  loggedUserEmail: string;
  isUserAdmin = false;
  isUserEmployee = false;
  isUserStudent = false;
  isUserLoggedIn = false;
  termId: number;
  generatingInProgress = false;
  isLoading = true;

  private poolingInterval;
  private ngUnsubscribe$ = new Subject();


  constructor(private termService: TermService,
              private route: ActivatedRoute,
              private router: Router,
              private authService: AuthService,
              private dialogService: DialogService,
              private snackBarService: SnackBarService,
              private showErrorsDialog: MatDialog) {
  }

  /**
   * Gets information about logged-in user and gets term id from router URL to sends request to get term details.
   */
  ngOnInit(): void {
    this.getUserInfo();

    this.termId = Number(this.route.snapshot.paramMap.get('id'));
    this.getTermDetails();
  }

  /**
   * Unsubscribes from everything and stop timer for the new token refresh before destroying the component.
   */
  ngOnDestroy(): void {
    this.stopPooling();
    this.ngUnsubscribe$.next();
    this.ngUnsubscribe$.complete();
  }

  /**
   * Gets index of the term run which is marked as highlighted.
   * @param termDetails  information about term
   */
  getHighlightIndex(termDetails: TermDetailsResponsePayload): number {
    return termDetails.termRuns.findIndex(tr => tr.highlight);
  }

  /**
   * Shows confirmation dialog before deleting the term.
   * @param termDetails  information about the term that should be removed
   */
  showConfirmDialog(termDetails: TermDetailsResponsePayload): void {
    const options: ConfirmDialogOptionsData = {
      title: 'Odstranit termín zkoušky?',
      message: `Opravdu chcete odstranit termín zkoušky "${termDetails.courseAbbreviation} - ${termDetails.termName} (${termDetails.academicYear})"? Toto rozhodnutí nelze vzít zpět.`,
      cancelText: 'ZRUŠIT',
      cancelColor: 'primary',
      confirmText: 'ODSTRANIT',
      confirmColor: 'warn'
    };
    this.dialogService.openConfirmDialog$(options)
      .subscribe(confirmed => {
        if (confirmed) {
          this.deleteTerm();
        }
      });
  }

  /**
   * Determines whether all test schedules have assigned assignments.
   * @param testSchedules  list of test schedules to check
   */
  doesAllTestSchedulesHaveAssignments(testSchedules: TestSchedule[]): boolean {
    if (!!testSchedules) {
      return testSchedules.every(testSchedule => !!testSchedule.assignment);
    } else {
      return false;
    }
  }

  /**
   * Determines whether all test schedules in all terms term runs have assigned assignment.
   * @param seatingPlans  list of seating plan whose test schedules should be checked
   */
  doesAllTermRunTestSchedulesHaveAssignments(seatingPlans: SeatingPlanPayload[]): boolean {
    return seatingPlans.every(seatingPlan =>
      this.doesAllTestSchedulesHaveAssignments(seatingPlan.testSchedules)
    );
  }

  /**
   * Determines whether all test schedules has generated assignments.
   * @param testSchedules  list of test schedules to check
   */
  areAllTestScheduleAssignmentsGenerated(testSchedules: TestSchedule[]): boolean {
    if (!!testSchedules) {
      return testSchedules.every(testSchedule => testSchedule.generated);
    } else {
      return false;
    }
  }

  /**
   * Determines whether all test schedules in specific term run has generated assignments.
   * @param seatingPlans  list of term run seating plans
   */
  areAllTermRunTestScheduleAssignmentsGenerated(seatingPlans: SeatingPlanPayload[]): boolean {
    return seatingPlans.every(seatingPlan =>
      this.areAllTestScheduleAssignmentsGenerated(seatingPlan.testSchedules)
    );
  }

  /**
   * Determines whether all test schedules in all terms term runs has generated assignments.
   * @param termRuns  list of term runs
   */
  areAllTermTermRunTestScheduleAssignmentsGenerated(termRuns: TermRunDetailsPayload[]): boolean {
    return termRuns.every(termRun =>
      this.areAllTermRunTestScheduleAssignmentsGenerated(termRun.seatingPlans)
    );
  }

  /**
   * Starts pooling when generating is started.
   */
  onGeneratingStarted(): void {
    this.startPooling();
    this.getTermDetails();
  }

  /**
   * Stops pooling when generating is canceled.
   */
  onGeneratingCanceled(): void {
    this.stopPooling();
  }

  /**
   * Links old and new rooms by their abbreviation.
   * @param index  index in list of seating plans
   * @param seatingPlan  seating plan at current index
   */
  trackByRoomAbbreviation = (index: number, seatingPlan: SeatingPlanPayload) => seatingPlan.roomAbbreviation;

  /**
   * Links old and new term runs by their abbreviation.
   * @param index  index in list of term runs
   * @param termRun  term run at current index
   */
  trackByTermRunId = (index: number, termRun: TermRunDetailsPayload) => termRun.id;

  /**
   * Displays dialog popup window with error message when generating fails.
   */
  showGeneratingFails(): void {
    this.showErrorsDialog.open(ShowErrorsDialogComponent, {
      maxWidth: '85vw',
      data: this.termDetails.generatingFails
    });
  }

  /**
   * Downloads CSV file with test schedules info
   * @param termRunId  ID of the term run whose data is to be downloaded
   * @param roomAbbreviation  room abbreviation whose data is to be downloaded
   */
  downloadStudentsCsv(termRunId: number, roomAbbreviation: string): void {
    this.termService.downloadStudentsCsv$(this.termId, termRunId, roomAbbreviation)
      .subscribe({
        next: response => {
          saveAs(response[0], response[1]);
        }
      });
  }

  private deleteTerm(): void {
    this.termService.deleteTerm$(this.termId)
      .subscribe({
        next: message => {
          this.snackBarService.openSuccessSnackBar(message);
          this.router.navigate(['/terms']);
        }
      });
  }

  private getUserInfo(): void {
    combineLatest([
      this.authService.isUserEmployee$(),
      this.authService.isUserAdmin$(),
      this.authService.isUserLoggedIn$(),
      this.authService.loggedUserInfo$,

    ])
      .pipe(takeUntil(this.ngUnsubscribe$))
      .subscribe({
        next: res => {
          this.isUserEmployee = res[0];
          this.isUserAdmin = res[1];
          this.isUserLoggedIn = res[2];
          if (res[3]) {
            this.loggedUserEmail = (res[3] as LoggedUserInfo).email;
          } else {
            this.loggedUserEmail = null;
          }
          this.isUserStudent = res[2] && !(res[0] || res[1]);
        },
        error: err => console.log(err)
      });
  }

  private getTermDetails(): void {
    if (!this.generatingInProgress) {
      this.isLoading = true;
    }
    let obs: Observable<TermDetailsResponsePayload>;
    if (this.loggedUserEmail) {
      obs = this.termService.getTermDetailsInternal$(this.termId);
    } else {
      obs = this.termService.getTermDetailsPublic$(this.termId);
    }
    obs.pipe(
      catchError(err => {
        this.router.navigate(['/terms']);
        return throwError(err);
      }),
      takeUntil(this.ngUnsubscribe$)
    ).subscribe({
      next: termDetails => {
        this.termDetails = termDetails;
        if (!termDetails.remainingGenerating && this.generatingInProgress) {
          this.stopPooling();
          if (!termDetails.generatingFails) {
            this.snackBarService.openSuccessSnackBar('Generování dokončeno');
          }
          if (termDetails.generatingFails) {
            const options: ConfirmDialogOptionsData = {
              title: 'Při generování zadání nastala chyba',
              message: `Chyby můžete zobrazit kliknutím na tlačítko ZOBRAZIT CHYBY.`,
              confirmText: 'OK',
              confirmColor: 'accent'
            };
            this.dialogService.openConfirmDialog$(options);
          }
        } else if (termDetails.remainingGenerating && !this.generatingInProgress) {
          this.startPooling();
        }
        if (termDetails.generatingFails) {
          this.snackBarService.openDangerSnackBar('Při generování zadání nastala chyba');
        }
        this.isLoading = false;
      }
    });
  }

  private startPooling(): void {
    this.generatingInProgress = true;
    this.poolingInterval = setInterval(() => this.getTermDetails(), 3000);
  }

  private stopPooling(): void {
    clearInterval(this.poolingInterval);
    this.generatingInProgress = false;
  }
}
