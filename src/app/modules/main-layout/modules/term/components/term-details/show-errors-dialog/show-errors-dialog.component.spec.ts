import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowErrorsDialogComponent } from './show-errors-dialog.component';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('ShowErrorsDialogComponent', () => {
  let component: ShowErrorsDialogComponent;
  let fixture: ComponentFixture<ShowErrorsDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ShowErrorsDialogComponent],
      imports: [HttpClientTestingModule],
      providers: [
        {
          provide: MAT_DIALOG_DATA,
          useValue: []
        }
      ]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowErrorsDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
