import { Component, OnDestroy, OnInit } from '@angular/core';
import { DialogService } from '@core/services/dialog.service';
import { AuthService, SnackBarService } from '@core/services';
import { Router } from '@angular/router';
import { combineLatest, Observable, Subject } from 'rxjs';
import { map, takeUntil } from 'rxjs/operators';
import { ConfirmDialogOptionsData } from '@shared/modules/dialog/confirm-dialog/confirm-dialog.component';
import { AllTermsInfoResponsePayload } from '../../models/all-terms-info-response.payload';
import { TermService } from '../../services/term.service';

/**
 * Component for displaying the page with list of all terms.
 */
@Component({
  selector: 'app-term-list',
  templateUrl: './term-list.component.html',
  styleUrls: ['./term-list.component.scss']
})
export class TermListComponent implements OnInit, OnDestroy {

  isUserAdmin = false;
  isUserEmployee = false;
  isUserLoggedIn = false;

  isLoading = true;

  terms: AllTermsInfoResponsePayload[];
  filteredTerms: AllTermsInfoResponsePayload[] = [];

  private ngUnsubscribe$ = new Subject();

  constructor(private dialogService: DialogService,
              private snackBarService: SnackBarService,
              private router: Router,
              private authService: AuthService,
              private termService: TermService) {
  }

  /**
   * Determines whether current user is logged-in and based on user role shows corresponding data on component init.
   */
  ngOnInit(): void {
    this.getUserRoles();

    let obs: Observable<AllTermsInfoResponsePayload[]>;
    if (this.isUserLoggedIn) {
      obs = this.termService.getAllTermsInfoInternal$();
    } else {
      obs = this.termService.getAllTermsInfoPublic$();
    }
    obs.subscribe({
      next: terms => {
        this.terms = terms;
        this.filteredTerms = terms;
        this.isLoading = false;
      },
      error: () => {
        this.router.navigate(['/terms']);
      }
    });
  }

  /**
   * Unsubscribes from everything before destroying the component.
   */
  ngOnDestroy(): void {
    this.ngUnsubscribe$.next();
    this.ngUnsubscribe$.complete();
  }

  /**
   * Filter terms on each key press in filter text input.
   * @param event  key press event
   */
  onKey(event: Event): void {
    const filterValue = (event.target as HTMLInputElement).value;
    this.filteredTerms = this.searchTerms(filterValue);
  }

  /**
   * Shows confirmation dialog before deleting the term.
   * @param term  information about term that should be deleted
   */
  showConfirmDialog(term: AllTermsInfoResponsePayload): void {
    const options: ConfirmDialogOptionsData = {
      title: 'Odstranit termín zkoušky?',
      message: `Opravdu chcete odstranit termín zkoušky ${term.courseAbbreviation} - ${term.termName} (${term.academicYear})? Toto rozhodnutí nelze vzít zpět.`,
      cancelText: 'ZRUŠIT',
      cancelColor: 'primary',
      confirmText: 'ODSTRANIT',
      confirmColor: 'warn'
    };
    this.dialogService.openConfirmDialog$(options)
      .subscribe(confirmed => {
        if (confirmed) {
          this.deleteTerm(term.termId);
        }
      });
  }

  /**
   * Determines whether currently logged user is manager of term.
   * @param managersEmails  list of term managers
   */
  isUserTermManager(managersEmails: string[]): Observable<boolean> {
    return this.authService.loggedUserInfo$
      .pipe(
        map(userInfo => managersEmails.includes(userInfo?.email))
      );
  }

  private searchTerms(value: string): AllTermsInfoResponsePayload[] {
    const filter = value.trim().toLowerCase();
    return this.terms.filter(term => term.courseAbbreviation.toLowerCase().includes(filter) ||
      term.termName.toLowerCase().includes(filter) || term.academicYear.toString().includes(filter));
  }

  private getUserRoles(): void {
    combineLatest([
      this.authService.isUserEmployee$(),
      this.authService.isUserAdmin$(),
      this.authService.isUserLoggedIn$()
    ])
      .pipe(takeUntil(this.ngUnsubscribe$))
      .subscribe({
        next: res => {
          this.isUserEmployee = res[0];
          this.isUserAdmin = res[1];
          this.isUserLoggedIn = res[2];
        },
        error: err => console.log(err)
      });
  }

  private deleteTerm(termId: number): void {
    this.termService.deleteTerm$(termId)
      .subscribe(message => {
        this.terms = this.terms.filter(term => term.termId !== termId);
        this.filteredTerms = this.filteredTerms.filter(term => term.termId !== termId);
        this.snackBarService.openSuccessSnackBar(message);
      });
  }
}
