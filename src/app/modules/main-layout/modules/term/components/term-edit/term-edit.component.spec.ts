import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TermEditComponent } from './term-edit.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRoute } from '@angular/router';
import { SharedModule } from '@shared/shared.module';
import { NewTermComponent } from '../new-term/new-term.component';
import { RouterTestingModule } from '@angular/router/testing';
import { AppModule } from '../../../../../../app.module';
import { TermInfoFormComponent } from '../new-term/term-info-form/term-info-form.component';
import { StudentListFormComponent } from '../new-term/student-list-form/student-list-form.component';
import { TermRunInfoFormComponent } from '../new-term/term-run-info-form/term-run-info-form.component';
import { ManualPlacementFormComponent } from '../new-term/manual-placement-form/manual-placement-form.component';
import { DistributionAndInfoFormComponent } from '../new-term/distribution-and-info-form/distribution-and-info-form.component';

describe('TermEditComponent', () => {
  let component: TermEditComponent;
  let fixture: ComponentFixture<TermEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [TermEditComponent, NewTermComponent, TermInfoFormComponent, StudentListFormComponent, TermRunInfoFormComponent,
        ManualPlacementFormComponent, DistributionAndInfoFormComponent],
      imports: [HttpClientTestingModule, RouterTestingModule, AppModule, SharedModule],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            snapshot: {
              paramMap: {
                get(): number {
                  return 1;
                }
              }
            }
          }
        }
      ]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TermEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
