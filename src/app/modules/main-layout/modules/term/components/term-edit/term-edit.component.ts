import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { EditTermInfoResponse } from '../../models/term/edit-term-info-response.payload';
import { EditTermService } from '../../services/edit-term.service';

/**
 * Component for displaying the page with a form for editing existing term.
 */
@Component({
  selector: 'app-term-edit',
  templateUrl: './term-edit.component.html',
  styleUrls: ['./term-edit.component.scss']
})
export class TermEditComponent implements OnInit {

  termData: EditTermInfoResponse;

  constructor(private termService: EditTermService,
              private route: ActivatedRoute) {
  }

  /**
   * Gets term id from router URL and sends request to get term edit data.
   */
  ngOnInit(): void {
    const termId = Number(this.route.snapshot.paramMap.get('id'));
    this.termService.getEditTermDetails$(termId)
      .subscribe({
        next: termData => {
          this.termData = termData;
          this.termService.setExistingStudentList(termData.studentsList);
        }
      });
  }

}
