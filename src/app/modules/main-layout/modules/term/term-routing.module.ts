import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '@core/services/guards/auth.guard';
import { Role } from '@core/role.enum';
import { UnsavedChangesGuard } from '@core/services/guards/unsaved-changes.guard';
import { NewTermComponent } from './components/new-term/new-term.component';
import { TermListComponent } from './components/term-list/term-list.component';
import { TermDetailsComponent } from './components/term-details/term-details.component';
import { TermEditComponent } from './components/term-edit/term-edit.component';
import { TermDataGuard } from '@core/services/guards/term-data.guard';


const termRoutes: Routes = [
  {
    path: 'new',
    component: NewTermComponent,
    canActivate: [AuthGuard, TermDataGuard],
    canDeactivate: [UnsavedChangesGuard],
    data: {
      roles: [Role.ADMIN, Role.EMPLOYEE]
    }
  },
  {
    path: ':id/edit',
    component: TermEditComponent,
    canActivate: [AuthGuard],
    canDeactivate: [UnsavedChangesGuard],
    data: {
      roles: [Role.ADMIN, Role.EMPLOYEE]
    }
  },
  {
    path: ':id',
    component: TermDetailsComponent
  },
  {
    path: '',
    component: TermListComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(termRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class TermRoutingModule {
}
