import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { LoginNamePayload } from '../models/login-name.payload';
import { SpacingBetweenStudents } from '../spacing-between-students.enum';
import { TermRun, TermRunPayload } from '../models/term-run.payload';
import { map } from 'rxjs/operators';
import { StudentsPlacement } from '../models/students-placement';
import { Room } from '../../course/models/free-room-response.payload';
import { HttpClient } from '@angular/common/http';
import { environment } from '@env';
import { MessagePayload } from '@core/services/models/message.payload';
import { NewTermRequestPayload } from '../models/term/new-term-request.payload';
import { EditTermRequestPayload } from '../models/term/edit-term-request.payload';

/**
 * Service for storing data necessary to create new term.
 */
@Injectable({
  providedIn: 'root'
})
export class NewTermService {

  private studentList$: BehaviorSubject<LoginNamePayload[]>;
  private numberOfRemainingStudents$: BehaviorSubject<number>;
  private selectedSpacing$: BehaviorSubject<SpacingBetweenStudents>;
  private termRuns$: BehaviorSubject<TermRunPayload[][]>;
  private studentsPlacements$: BehaviorSubject<StudentsPlacement[]>;

  constructor(private http: HttpClient) {
    this.studentList$ = new BehaviorSubject<LoginNamePayload[]>([]);
    this.numberOfRemainingStudents$ = new BehaviorSubject<number>(0);
    this.selectedSpacing$ = new BehaviorSubject<SpacingBetweenStudents>(SpacingBetweenStudents.ZERO.valueOf());
    this.termRuns$ = new BehaviorSubject<TermRunPayload[][]>([[]]);
    this.studentsPlacements$ = new BehaviorSubject<StudentsPlacement[]>([]);
  }

  /**
   * Gets list of stored students as observable.
   */
  getStudentList$(): Observable<LoginNamePayload[]> {
    return this.studentList$.asObservable();
  }

  /**
   * Sets new students list to subject.
   * @param studentList  students list to be stored.
   */
  setStudentList(studentList: LoginNamePayload[]): void {
    this.studentList$.next(studentList);
  }

  /**
   * Gets current students list value from subject.
   */
  get studentListValue(): LoginNamePayload[] {
    return this.studentList$.getValue();
  }

  /**
   * Gets number of remaining students as observable.
   */
  getNumberOfRemainingStudents$(): Observable<number> {
    return this.numberOfRemainingStudents$.asObservable();
  }

  /**
   * Sets new number of remaining students to subject.
   * @param numberOfRemainingStudents  number of remaining students to be stored
   */
  setNumberOfRemainingStudents(numberOfRemainingStudents: number): void {
    this.numberOfRemainingStudents$.next(numberOfRemainingStudents);
  }

  /**
   * Gets selected spacing as observable.
   */
  getSelectedSpacing$(): Observable<SpacingBetweenStudents> {
    return this.selectedSpacing$.asObservable();
  }

  /**
   * Sets new spacing to subject.
   * @param spacing  spacing to be stored
   */
  setSelectedSpacing(spacing: SpacingBetweenStudents): void {
    this.selectedSpacing$.next(spacing.valueOf());
  }

  /**
   * Gets current spacing value from the subject.
   */
  get selectedSpacingValue(): SpacingBetweenStudents {
    return this.selectedSpacing$.getValue();
  }

  /**
   * Gets list of created term runs as observable.
   */
  getTermRuns$(): Observable<TermRunPayload[]> {
    return this.termRuns$.asObservable()
      .pipe(
        map(
          allTermRuns =>
            allTermRuns.reduce((acc, termRunArray) => acc.concat(termRunArray), [])
        )
      );
  }

  /**
   * Updates list of created term runs in subject.
   * @param termRuns  updated list of term runs
   * @param index  index of the group
   */
  updateTermRuns(termRuns: TermRunPayload[], index: number): void {
    const newArray = this.termRuns$.getValue();
    newArray.splice(index, 1, termRuns);
    this.termRuns$.next(newArray);
  }

  /**
   * Clears term runs from the subject at given index (stays empty).
   * @param index  index of the group
   */
  clearTermRuns(index: number): void {
    const newArray = this.termRuns$.getValue();
    newArray[index] = [];
    this.termRuns$.next(newArray);
  }

  /**
   * Removes term runs from the subject at given index.
   * @param index  index of the group
   */
  removeTermRuns(index: number): void {
    const newArray = this.termRuns$.getValue();
    newArray.splice(index, 1);
    this.termRuns$.next(newArray);
  }

  /**
   * Gets current term runs list value from the subject.
   */
  get termRunsValue(): TermRunPayload[] {
    return this.termRuns$.getValue()
      .reduce((acc, termRunArray) => acc.concat(termRunArray), []);
  }

  /**
   * Gets term runs of given form group from subject.
   * @param formGroupId  ID of the form group
   */
  getFormGroupTermRunsValue(formGroupId: number): TermRunPayload[] {
    return this.termRuns$.getValue()[formGroupId];
  }

  /**
   * Gets list of students placements as observable.
   */
  getStudentsPlacements$(): Observable<StudentsPlacement[]> {
    return this.studentsPlacements$.asObservable()
      .pipe(
        map(
          allStudentsPlacements =>
            allStudentsPlacements.reduce((acc, studentsArray) => acc.concat(studentsArray), [])
        )
      );
  }

  /**
   * Updates list of created student placements in subject.
   * @param studentPlacement  updated list of student placements
   * @param index  index of the group
   */
  updateStudentsPlacements(studentPlacement: StudentsPlacement, index: number): void {
    const newArray = this.studentsPlacements$.getValue();
    newArray.splice(index, 1, studentPlacement);
    this.studentsPlacements$.next(newArray);
  }

  /**
   * Clears student placements students list from the subject at given index (stays empty).
   * @param index  index of the group
   */
  clearStudentsPlacementStudents(index: number): void {
    const studentsPlacement = this.studentsPlacements$.getValue()[index];
    studentsPlacement.students = [];
    this.updateStudentsPlacements(studentsPlacement, index);
  }

  /**
   * Clears student placements room from the subject at given index (stays empty).
   * @param index  index of the group
   */
  clearStudentsPlacementRoom(index: number): void {
    const studentsPlacement = this.studentsPlacements$.getValue()[index];
    studentsPlacement.room = undefined;
    this.updateStudentsPlacements(studentsPlacement, index);
  }

  /**
   * Clears student placements term run from the subject at given index (stays empty).
   * @param index  index of the group
   */
  clearStudentsPlacementTermRun(index: number): void {
    const studentsPlacement = this.studentsPlacements$.getValue()[index];
    studentsPlacement.termRun = null;
    this.updateStudentsPlacements(studentsPlacement, index);
  }

  /**
   * Adds new students placement at the given index in the subject.
   * @param index  index of the group
   */
  addStudentsPlacements(index: number): void {
    const newArray = this.studentsPlacements$.getValue();
    newArray.splice(index, 1, new StudentsPlacement([], new TermRun(), new Room(), []));
    this.studentsPlacements$.next(newArray);
  }

  /**
   * Removes student placement at the given index in the subject.
   * @param index  index of the group
   */
  removeStudentsPlacements(index: number): void {
    const newArray = this.studentsPlacements$.getValue();
    newArray.splice(index, 1);
    this.studentsPlacements$.next(newArray);
  }

  /**
   * Gets list of students placements from subject
   */
  get studentPlacementsValue(): StudentsPlacement[] {
    return this.studentsPlacements$.getValue();
  }


  /**
   * Sends request to create new term.
   * @param termModel   information about term and other data necessary to create new term
   */
  createNewTerm$(termModel: NewTermRequestPayload): Observable<string> {
    return this.http.post<MessagePayload>(`${environment.backendUrl}/internal/terms`, termModel)
      .pipe(
        map(message => message.message)
      );
  }

  /**
   * Sends request to edit existing assignment.
   * @param termRunId  unique ID of the edit which should be edited
   * @param termModel  information about term and other data necessary to edit existing term
   */
  editTerm$(termRunId: number, termModel: EditTermRequestPayload): Observable<string> {
    return this.http.put<MessagePayload>(`${environment.backendUrl}/internal/terms/${termRunId}`, termModel)
      .pipe(
        map(message => message.message)
      );
  }

  /**
   * Clears all data from subjects
   */
  flushBehaviorSubjects(): void {
    this.setNumberOfRemainingStudents(0);
    this.setSelectedSpacing(SpacingBetweenStudents.ZERO.valueOf());
    this.setStudentList([]);
    this.flushStudentPlacements();
    this.flushTermRuns();
  }

  private flushStudentPlacements(): void {
    this.studentsPlacements$.next([]);
  }

  private flushTermRuns(): void {
    this.termRuns$.next([[]]);
  }
}
