import { TestBed } from '@angular/core/testing';

import { NewTermService } from './new-term.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { SharedModule } from '@shared/shared.module';

describe('NewTermService', () => {
  let service: NewTermService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, SharedModule]
    });
    service = TestBed.inject(NewTermService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
