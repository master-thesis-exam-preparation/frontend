import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { EditTermInfoResponse } from '../models/term/edit-term-info-response.payload';
import { environment } from '@env';
import { HttpClient } from '@angular/common/http';
import { LoginNamePayload } from '../models/login-name.payload';

/**
 * Service for storing data necessary to edit existing term.
 */
@Injectable({
  providedIn: 'root'
})
export class EditTermService {

  private existingStudentList$: BehaviorSubject<LoginNamePayload[]>;

  constructor(private http: HttpClient) {
    this.existingStudentList$ = new BehaviorSubject<LoginNamePayload[]>([]);
  }

  /**
   * Gets list of stored students as observable.
   */
  getExistingStudentList$(): Observable<LoginNamePayload[]> {
    return this.existingStudentList$.asObservable();
  }

  /**
   * Sets new student list to subject.
   * @param studentList  students list to be stored
   */
  setExistingStudentList(studentList: LoginNamePayload[]): void {
    this.existingStudentList$.next(studentList);
  }

  /**
   * Gets current value of stored student list.
   */
  get existingStudentListValue(): LoginNamePayload[] {
    return this.existingStudentList$.getValue();
  }

  /**
   * Sends request to information about term for edit term form.
   * @param termId  unique ID of the term
   */
  getEditTermDetails$(termId: number): Observable<EditTermInfoResponse> {
    return this.http.get<EditTermInfoResponse>(`${environment.backendUrl}/internal/terms/${termId}/edit`);
  }

  /**
   * Clears all data from subjects
   */
  flushBehaviorSubjects(): void {
    this.setExistingStudentList([]);
  }
}
