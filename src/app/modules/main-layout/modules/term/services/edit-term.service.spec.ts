import { TestBed } from '@angular/core/testing';

import { EditTermService } from './edit-term.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { SharedModule } from '@shared/shared.module';

describe('EditTermService', () => {
  let service: EditTermService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, SharedModule]
    });
    service = TestBed.inject(EditTermService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
