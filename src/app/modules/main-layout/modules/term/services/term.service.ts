import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { MessagePayload } from '@core/services/models/message.payload';
import { environment } from '@env';
import { map } from 'rxjs/operators';
import { HttpClient, HttpParams } from '@angular/common/http';
import { AllTermsInfoResponsePayload } from '../models/all-terms-info-response.payload';
import { TermDetailsResponsePayload } from '../models/term-details-response.payload';
import { FileContent } from '../../assignment/models/file-content.payload';

/**
 * Service for sending request related to terms.
 */
@Injectable({
  providedIn: 'root'
})
export class TermService {

  constructor(private http: HttpClient) {
  }

  /**
   * Sends request to get public information about all terms.
   */
  getAllTermsInfoPublic$(): Observable<AllTermsInfoResponsePayload[]> {
    return this.http.get<AllTermsInfoResponsePayload[]>(`${environment.backendUrl}/public/terms`);
  }

  /**
   * Sends request to get internal information about all terms.
   */
  getAllTermsInfoInternal$(): Observable<AllTermsInfoResponsePayload[]> {
    return this.http.get<AllTermsInfoResponsePayload[]>(`${environment.backendUrl}/internal/terms`);
  }

  /**
   * Sends request to get detailed public information about specific term.
   * @param termId  unique ID of the term
   */
  getTermDetailsPublic$(termId: number): Observable<TermDetailsResponsePayload> {
    return this.http.get<TermDetailsResponsePayload>(`${environment.backendUrl}/public/terms/${termId}`);
  }

  /**
   * Sends request to get detailed internal information about specific term.
   * @param termId  unique ID of the term
   */
  getTermDetailsInternal$(termId: number): Observable<TermDetailsResponsePayload> {
    return this.http.get<TermDetailsResponsePayload>(`${environment.backendUrl}/internal/terms/${termId}`);
  }

  /**
   * Sends request to delete a term.
   * @param termId  unique ID of term which should be deleted
   */
  deleteTerm$(termId: number): Observable<string> {
    return this.http.delete<MessagePayload>(`${environment.backendUrl}/internal/terms/${termId}`)
      .pipe(
        map(message => message.message)
      );
  }

  /**
   * Sends request to download generated PDF files.
   * @param termId  unique ID of term whose assignments should be downloaded
   * @param termRunId  unique ID of term run whose assignments should be downloaded
   * @param roomAbbreviation  abbreviation of the room whose assignments should be downloaded
   * @param testScheduleId  unique ID of the test schedule whose assignment should be downloaded
   * @param pack  determines whether should be PDF files merged into one file
   * @param doubleSided  determines whether empty page should be added at the end of file with odd number of pages
   */
  downloadPdfFiles$(termId: number, termRunId: number, roomAbbreviation: string, testScheduleId: number,
                    pack: boolean, doubleSided: boolean): Observable<(string | Blob)[]> {
    let params = new HttpParams();
    if (termId) {
      params = params.append('termId', String(termId));
    }
    if (termRunId) {
      params = params.append('termRunId', String(termRunId));
    }
    if (roomAbbreviation) {
      params = params.append('roomAbbreviation', roomAbbreviation);
    }
    if (testScheduleId) {
      params = params.append('testScheduleId', String(testScheduleId));
    }
    params = params.append('pack', String(pack));
    params = params.append('doubleSided', String(doubleSided));
    return this.http.get(`${environment.backendUrl}/internal/test-schedules/download`,
      {responseType: 'blob', params, observe: 'response'})
      .pipe(
        map(response => {
            const fileName = response.headers
              .get('content-disposition')
              .match(/"([^"]+)"/)[1]
              .replace(/^.*[\\\/]/, '');
            return [response.body, fileName];
          }
        ));
  }

  /**
   * Sends request to get content of log file with error.
   * @param testScheduleId  unique ID of test schedule whose assignment generating failed
   */
  getLogFileContent$(testScheduleId: number): Observable<FileContent> {
    return this.http.get<FileContent>(`${environment.backendUrl}/internal/test-schedules/${testScheduleId}/log-file-content`);
  }

  /**
   * Sends request to download students list as CSV file
   * @param termId  unique IF of the term
   * @param termRunId  unique ID of the term run
   * @param roomAbbreviation  room abbreviation of the room whose students list should be downloaded
   */
  downloadStudentsCsv$(termId: number, termRunId: number, roomAbbreviation: string): Observable<(string | Blob)[]> {
    let params = new HttpParams();
    if (termId) {
      params = params.append('termId', String(termId));
    }
    if (termRunId) {
      params = params.append('termRunId', String(termRunId));
    }
    if (roomAbbreviation) {
      params = params.append('roomAbbreviation', roomAbbreviation);
    }
    return this.http.get(`${environment.backendUrl}/internal/test-schedules/download-students-csv`,
      {responseType: 'blob', params, observe: 'response'})
      .pipe(
        map(response => {
            const fileName = response.headers
              .get('content-disposition')
              .match(/"([^"]+)"/)[1]
              .replace(/^.*[\\\/]/, '');
            return [response.body, fileName];
          }
        ));
  }
}
