import { NgModule } from '@angular/core';
import { SharedModule } from '@shared/shared.module';
import { TermRoutingModule } from './term-routing.module';
import { NewTermComponent } from './components/new-term/new-term.component';
import { TermListComponent } from './components/term-list/term-list.component';
import { TermDetailsComponent } from './components/term-details/term-details.component';
import { TermEditComponent } from './components/term-edit/term-edit.component';
import { TermInfoFormComponent } from './components/new-term/term-info-form/term-info-form.component';
import { StudentListFormComponent } from './components/new-term/student-list-form/student-list-form.component';
import { TermRunInfoFormComponent } from './components/new-term/term-run-info-form/term-run-info-form.component';
import { ManualPlacementFormComponent } from './components/new-term/manual-placement-form/manual-placement-form.component';
import { SelectPlaceDialogComponent } from './components/new-term/manual-placement-form/select-place-dialog/select-place-dialog.component';
import { DistributionAndInfoFormComponent } from './components/new-term/distribution-and-info-form/distribution-and-info-form.component';
import { TestScheduleTableComponent } from './components/term-details/test-schedule-table/test-schedule-table.component';
import { ShowErrorsDialogComponent } from './components/term-details/show-errors-dialog/show-errors-dialog.component';
import { StudentListTableComponent } from './components/new-term/student-list-form/student-list-table/student-list-table.component';


@NgModule({
  declarations: [
    NewTermComponent,
    TermListComponent,
    TermDetailsComponent,
    TermEditComponent,
    TermInfoFormComponent,
    StudentListFormComponent,
    TermRunInfoFormComponent,
    ManualPlacementFormComponent,
    SelectPlaceDialogComponent,
    DistributionAndInfoFormComponent,
    TestScheduleTableComponent,
    ShowErrorsDialogComponent,
    StudentListTableComponent
  ],
  imports: [
    SharedModule,
    TermRoutingModule
  ]
})
export class TermModule {
}
