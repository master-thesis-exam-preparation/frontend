import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ChangePasswordPayload } from '../models/change-password.payload';
import { MessagePayload } from '@core/services/models/message.payload';
import { environment } from '@env';
import { map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { ChangeUserDetailsPayload } from '../models/change-user-details.payload';
import { LoggedUserInfo } from '@core/logged-user-info';
import { ChangeRolePayload } from '../models/change-role.payload';

/**
 * Service for sending request related to user accounts.
 */
@Injectable({
  providedIn: 'root'
})
export class AccountService {

  constructor(private http: HttpClient) {
  }

  /**
   * Sends change user password request.
   * @param changePasswordPayload  information about users old and new password
   */
  changePassword$(changePasswordPayload: ChangePasswordPayload): Observable<string> {
    return this.http.post<MessagePayload>(`${environment.backendUrl}/internal/accounts/change-password`, changePasswordPayload)
      .pipe(map(value => {
          return value.message;
        })
      );
  }

  /**
   * Sends change user details request.
   * @param changeUserDetailsPayload  information about name and degrees of the user.
   */
  changeUserDetails$(changeUserDetailsPayload: ChangeUserDetailsPayload): Observable<string> {
    return this.http.post<MessagePayload>(`${environment.backendUrl}/internal/accounts/change-user-details`, changeUserDetailsPayload)
      .pipe(map(value => {
          return value.message;
        })
      );
  }

  /**
   * Sends request to get details about currently logged-in user.
   */
  getUserData$(): Observable<ChangeUserDetailsPayload> {
    return this.http.get<ChangeUserDetailsPayload>(`${environment.backendUrl}/internal/accounts/change-user-details`);
  }

  /**
   * Sends request to get details about all users accounts.
   */
  getAllUsersData$(): Observable<LoggedUserInfo[]> {
    return this.http.get<LoggedUserInfo[]>(`${environment.backendUrl}/internal/accounts`);
  }

  /**
   * Sends request to change role of specific user.
   * @param changeRolePayload  information necessary to set a new role to user
   * @param userId  unique ID of the user whose role should be changed
   */
  changeAccountRole$(changeRolePayload: ChangeRolePayload, userId: number): Observable<string> {
    return this.http.post<MessagePayload>(`${environment.backendUrl}/internal/accounts/change-role/${userId}`, changeRolePayload)
      .pipe(
        map(message => message.message)
      );
  }

  /**
   * Sends request to delete user account.
   * @param userId  unique ID of the user whose account should be deleted
   */
  deleteAccount$(userId: number): Observable<string> {
    return this.http.delete<MessagePayload>(`${environment.backendUrl}/internal/accounts/delete/${userId}`)
      .pipe(
        map(message => message.message)
      );
  }

  /**
   * Sends request to lock user account.
   * @param userId  unique ID of the user whose account should be locked
   */
  lockAccount$(userId: number): Observable<string> {
    return this.http.get<MessagePayload>(`${environment.backendUrl}/internal/accounts/lock/${userId}`)
      .pipe(
        map(message => message.message)
      );
  }

  /**
   * Sends request to unlock user account.
   * @param userId  unique ID of the user whose account should be unlocked
   */
  unlockAccount$(userId: number): Observable<string> {
    return this.http.get<MessagePayload>(`${environment.backendUrl}/internal/accounts/unlock/${userId}`)
      .pipe(
        map(message => message.message)
      );
  }
}
