import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '@core/services/guards/auth.guard';
import { Role } from '@core/role.enum';
import { ChangePasswordComponent } from './components/change-password/change-password.component';
import { ChangeUserDetailsComponent } from './components/change-user-details/change-user-details.component';
import { ManageAccountsComponent } from './components/manage-accounts/manage-accounts.component';


const accountRoutes: Routes = [
  {
    path: 'change-password',
    component: ChangePasswordComponent,
    canActivate: [AuthGuard],
    data: {
      roles: [Role.STUDENT, Role.EMPLOYEE, Role.ADMIN]
    }
  },
  {
    path: 'change-user-details',
    component: ChangeUserDetailsComponent,
    canActivate: [AuthGuard],
    data: {
      roles: [Role.STUDENT, Role.EMPLOYEE, Role.ADMIN]
    }
  },
  {
    path: 'manage-accounts',
    component: ManageAccountsComponent,
    canActivate: [AuthGuard],
    data: {
      roles: [Role.ADMIN]
    }
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(accountRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class AccountRoutingModule {
}
