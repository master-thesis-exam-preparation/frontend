/**
 * Used to store information necessary to set a new role to user.
 */
export interface ChangeRolePayload {
  role: string;
}

export class ChangeRolePayload {
  role: string;

  constructor(role: string) {
    this.role = role;
  }
}
