/**
 * Used to store information about name and degrees of the user.
 */
export interface ChangeUserDetailsPayload {
  degreesBeforeName: string;
  degreesBehindName: string;
  firstName: string;
  lastName: string;
}

export class ChangeUserDetailsPayload {
  degreesBeforeName: string;
  degreesBehindName: string;
  firstName: string;
  lastName: string;

  constructor(degreesBeforeName: string, degreesBehindName: string, firstName: string, lastName: string) {
    this.degreesBeforeName = degreesBeforeName;
    this.degreesBehindName = degreesBehindName;
    this.firstName = firstName;
    this.lastName = lastName;
  }
}
