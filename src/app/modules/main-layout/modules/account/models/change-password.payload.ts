/**
 * Used to store information about users old and new password.
 */
export interface ChangePasswordPayload {
  currentPassword: string;
  newPassword: string;
}

export class ChangePasswordPayload {
  currentPassword: string;
  newPassword: string;

  constructor(currentPassword: string, newPassword: string) {
    this.currentPassword = currentPassword;
    this.newPassword = newPassword;
  }
}
