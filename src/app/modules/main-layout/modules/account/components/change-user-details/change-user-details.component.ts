import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AccountService } from '../../services/account.service';
import { Router } from '@angular/router';
import { AuthService, SnackBarService } from '@core/services';
import { ChangeUserDetailsPayload } from '../../models/change-user-details.payload';

/**
 * Component for displaying the page with a form for user details change.
 */
@Component({
  selector: 'app-change-user-details',
  templateUrl: './change-user-details.component.html',
  styleUrls: ['./change-user-details.component.scss']
})
export class ChangeUserDetailsComponent implements OnInit {

  constructor(private fb: FormBuilder,
              private accountService: AccountService,
              private authService: AuthService,
              private router: Router,
              private snackBarService: SnackBarService) {
  }

  /**
   * Gets value of the degreesBeforeName input in the form.
   */
  get degreesBeforeName(): AbstractControl {
    return this.changeUserDetailsForm.get('degreesBeforeName');
  }

  /**
   * Gets value of the firstName input in the form.
   */
  get firstName(): AbstractControl {
    return this.changeUserDetailsForm.get('firstName');
  }

  /**
   * Gets value of the lastName input in the form.
   */
  get lastName(): AbstractControl {
    return this.changeUserDetailsForm.get('lastName');
  }

  /**
   * Gets value of the degreesBehindName input in the form.
   */
  get degreesBehindName(): AbstractControl {
    return this.changeUserDetailsForm.get('degreesBehindName');
  }

  changeUserDetailsForm: FormGroup;
  isWaiting = false;

  /**
   * Sets validators to the form inputs and fills form with logged-in user data.
   */
  ngOnInit(): void {
    this.changeUserDetailsForm = this.fb.group(
      {
        degreesBeforeName: ['', [
          Validators.maxLength(255)
        ]],
        firstName: ['', [
          Validators.required,
          Validators.maxLength(255)
        ]],
        lastName: ['', [
          Validators.required,
          Validators.maxLength(255)
        ]],
        degreesBehindName: ['', [
          Validators.maxLength(255)
        ]],
      }
    );
    this.accountService.getUserData$()
      .subscribe({
        next: userData => {
          this.changeUserDetailsForm.patchValue({
            degreesBeforeName: userData.degreesBeforeName,
            firstName: userData.firstName,
            lastName: userData.lastName,
            degreesBehindName: userData.degreesBehindName
          });
        }
      });
  }

  /**
   * Submit the form and send the change user details request.
   */
  onSubmit(): void {
    if (this.changeUserDetailsForm.valid) {
      const changeUserDetailsPayload = new ChangeUserDetailsPayload(
        this.degreesBeforeName.value,
        this.degreesBehindName.value,
        this.firstName.value,
        this.lastName.value
      );

      this.isWaiting = true;
      this.accountService.changeUserDetails$(changeUserDetailsPayload)
        .subscribe({
          next: response => {
            this.authService.logMeIn$().subscribe();
            this.isWaiting = false;
            this.router.navigate(['']);
            this.snackBarService.openSuccessSnackBar(response);
          },
          error: () => {
            this.isWaiting = false;
          }
        });
    }
  }
}
