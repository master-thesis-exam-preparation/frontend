import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChangeUserDetailsComponent } from './change-user-details.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { FormBuilder } from '@angular/forms';
import { AppModule } from '../../../../../../app.module';
import { SharedModule } from '@shared/shared.module';

describe('ChangeUserDetailsComponent', () => {
  let component: ChangeUserDetailsComponent;
  let fixture: ComponentFixture<ChangeUserDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ChangeUserDetailsComponent],
      imports: [HttpClientTestingModule, RouterTestingModule, SharedModule, AppModule],
      providers: [FormBuilder]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChangeUserDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
