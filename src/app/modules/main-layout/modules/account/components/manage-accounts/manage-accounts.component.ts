import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { Role } from '@core/role.enum';
import { DialogService } from '@core/services/dialog.service';
import { SnackBarService } from '@core/services';
import { Observable } from 'rxjs';
import { ConfirmDialogOptionsData } from '@shared/modules/dialog/confirm-dialog/confirm-dialog.component';
import { AccountService } from '../../services/account.service';
import { LoggedUserInfo } from '@core/logged-user-info';
import { MatSelectChange } from '@angular/material/select';
import { ChangeRolePayload } from '../../models/change-role.payload';

/**
 * Component for displaying the page with form for managing existing accounts.
 */
@Component({
  selector: 'app-manage-accounts',
  templateUrl: './manage-accounts.component.html',
  styleUrls: ['./manage-accounts.component.scss']
})
export class ManageAccountsComponent implements OnInit, AfterViewInit {

  headerColumns: string[] = ['email', 'nameAndDegrees', 'role', 'actions'];
  accountsDataSource: MatTableDataSource<LoggedUserInfo> = new MatTableDataSource<LoggedUserInfo>();

  isLoading = true;

  roleKeys: string[];

  /**
   * Sets MatSort to work even in ngIf
   * @param sort  MatSort to set
   */
  @ViewChild(MatSort, {static: false}) set sort(sort: MatSort) {
    this.accountsDataSource.sort = sort;
  }

  /**
   * Sets MatPaginator to work even in ngIf
   * @param paginator  MatPaginator to set
   */
  @ViewChild(MatPaginator, {static: false}) set paginator(paginator: MatPaginator) {
    this.accountsDataSource.paginator = paginator;
  }

  constructor(private dialogService: DialogService,
              private snackBarService: SnackBarService,
              private accountService: AccountService) {
    this.roleKeys = Object.keys(Role);
  }

  /**
   * Sends request to get information about all account on component init.
   */
  ngOnInit(): void {
    this.setAccountsDataSource();
  }

  /**
   * Sets paginator and sort functionalities in table after component view init is done.
   */
  ngAfterViewInit(): void {
    this.accountsDataSource.paginator = this.paginator;
    this.accountsDataSource.sort = this.sort;
  }

  /**
   * Takes filter value from input and apply it to list of rooms.
   * @param event  filter key press event
   */
  applyFilter(event: Event): void {
    const filterValue = (event.target as HTMLInputElement).value;
    this.accountsDataSource.filter = filterValue.trim().toLowerCase();

    if (this.accountsDataSource.paginator) {
      this.accountsDataSource.paginator.firstPage();
    }
  }

  /**
   * Sends request to delete selected account.
   * @param email  e-mail of the user whose account should be deleted
   * @param userId  unique ID of the user whose account should be deleted
   */
  deleteAccount(email: string, userId: number): void {
    this.showConfirmDeleteDialog(email)
      .subscribe({
        next: confirmed => {
          if (confirmed) {
            this.accountService.deleteAccount$(userId)
              .subscribe(message => {
                this.removeAccountFromTable(email);
                this.snackBarService.openSuccessSnackBar(message);
              });
          }
        }
      });
  }

  /**
   * Sends request to lock selected account.
   * @param email  e-mail of the user whose account should be locked
   * @param userId  unique ID of the user whose account should be locked
   * @param locked  true if account should be unlocked, false if account should be unlocked
   */
  lockAccount(email: string, userId: number, locked: boolean): void {
    this.showConfirmLockDialog(email, locked)
      .subscribe({
        next: confirmed => {
          if (confirmed) {
            if (locked) {
              this.accountService.unlockAccount$(userId)
                .subscribe(message => {
                  this.updateAccountTable(userId, !locked);
                  this.snackBarService.openSuccessSnackBar(message);
                });
            } else {
              this.accountService.lockAccount$(userId)
                .subscribe(message => {
                  this.updateAccountTable(userId, !locked);
                  this.snackBarService.openSuccessSnackBar(message);
                });
            }
          }
        }
      });
  }

  /**
   * Sends request to change role of specific user.
   * @param event  MatSelect value change event
   * @param userId  unique ID of the user whose role should be changed
   */
  changeAccountRole(event: MatSelectChange, userId: number): void {
    const changeRolePayload = new ChangeRolePayload(event.value);
    this.accountService.changeAccountRole$(changeRolePayload, userId)
      .subscribe(message => {
        this.snackBarService.openSuccessSnackBar(message);
      });
  }

  private showConfirmDeleteDialog(email: string): Observable<boolean> {
    const options: ConfirmDialogOptionsData = {
      title: 'Odstranit uživatelský účet?',
      message: `Opravdu chcete odstranit účet uživatele s e-mailem "${email}"? Toto rozhodnutí nelze vzít zpět.`,
      cancelText: 'ZRUŠIT',
      cancelColor: 'primary',
      confirmText: 'ODSTRANIT',
      confirmColor: 'warn'
    };
    return this.dialogService.openConfirmDialog$(options);
  }

  private showConfirmLockDialog(email: string, locked: boolean): Observable<boolean> {
    const options: ConfirmDialogOptionsData = {
      title: `${locked ? 'Odblokovat' : 'Zablokovat'} uživatelský účet?`,
      message: `Opravdu chcete ${locked ? 'odblokovat' : 'zablokovat'} účet uživatele s e-mailem "${email}"?`,
      cancelText: 'ZRUŠIT',
      cancelColor: 'primary',
      confirmText: `${locked ? 'ODBLOKOVAT' : 'ZABLOKOVAT'}`,
      confirmColor: 'warn'
    };
    return this.dialogService.openConfirmDialog$(options);
  }

  private removeAccountFromTable(email: string): void {
    this.accountsDataSource.data = this.accountsDataSource.data
      .filter(tableAccount => email !== tableAccount.email);
  }

  private updateAccountTable(userId: number, locked: boolean): void {
    this.accountsDataSource.data = this.accountsDataSource.data
      .map(tableAccount => {
        if (tableAccount.userId === userId) {
          tableAccount.locked = locked;
        }
        return tableAccount;
      });
  }

  private setAccountsDataSource(): void {
    this.isLoading = true;
    this.accountService
      .getAllUsersData$()
      .subscribe(allAccountsInfo => {
          this.accountsDataSource.data = allAccountsInfo;
          this.isLoading = false;
        }
      );
  }
}
