import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SnackBarService } from '@core/services';
import { Router } from '@angular/router';
import { PasswordValidationService } from '@core/services/password-validation.service';
import { AccountService } from '../../services/account.service';
import { ChangePasswordPayload } from '../../models/change-password.payload';

/**
 * Component for displaying the page with a form for current password change.
 */
@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit {

  constructor(private fb: FormBuilder,
              private accountService: AccountService,
              private router: Router,
              private snackBarService: SnackBarService,
              private passwordValidationService: PasswordValidationService) {
  }

  /**
   * Gets value of the currentPassword input in the form.
   */
  get currentPassword(): AbstractControl {
    return this.changePasswordForm.get('currentPassword');
  }

  /**
   * Gets value of the password input in the form.
   */
  get password(): AbstractControl {
    return this.changePasswordForm.get('password');
  }

  /**
   * Gets value of the confirmPassword input in the form.
   */
  get confirmPassword(): AbstractControl {
    return this.changePasswordForm.get('confirmPassword');
  }

  changePasswordForm: FormGroup;
  hideCurrentPassword = true;
  hidePassword = true;
  hideConfirmPassword = true;

  isWaiting = false;

  /**
   * Sets validators to the form inputs.
   */
  ngOnInit(): void {
    this.changePasswordForm = this.fb.group(
      {
        currentPassword: ['', [
          Validators.required
        ]],
        password: ['', [
          // 1. Password Field is Required
          Validators.required,
          // 2. check whether the entered password has a number
          this.passwordValidationService.regexValidator(/\d/, {hasNumber: true}),
          // 3. check whether the entered password has upper case letter
          this.passwordValidationService.regexValidator(/[A-Z]/, {hasCapitalCase: true}),
          // 4. check whether the entered password has a lower-case letter
          this.passwordValidationService.regexValidator(/[a-z]/, {hasSmallCase: true}),
          // 5. check whether the entered password has a special character
          this.passwordValidationService.regexValidator(/\W|_/, {hasSpecialCharacters: true}),
          // 6. Has a minimum length of 8 characters
          Validators.minLength(8)
        ]],
        confirmPassword: ['', [
          Validators.required
        ]],
      },
      {
        validators: [
          this.passwordValidationService.passwordMatchValidator
        ]
      }
    );
  }

  /**
   * Submit the form and send the change password request.
   */
  onSubmit(): void {
    if (this.changePasswordForm.valid) {
      if (this.currentPassword.value === this.password.value) {
        this.snackBarService.openDangerSnackBar('Nové a současné heslo je stejné');
        return;
      }
      const changePasswordPayload = new ChangePasswordPayload(this.currentPassword.value, this.password.value);

      this.isWaiting = true;
      this.accountService.changePassword$(changePasswordPayload)
        .subscribe({
          next: response => {
            this.snackBarService.openSuccessSnackBar(response);
            this.isWaiting = false;
            this.router.navigate(['']);
          },
          error: () => {
            this.isWaiting = false;
          }
        });
    }
  }
}
