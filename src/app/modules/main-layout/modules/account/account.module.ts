import { NgModule } from '@angular/core';
import { ChangePasswordComponent } from './components/change-password/change-password.component';
import { ChangeUserDetailsComponent } from './components/change-user-details/change-user-details.component';
import { ManageAccountsComponent } from './components/manage-accounts/manage-accounts.component';
import { SharedModule } from '@shared/shared.module';
import { AccountRoutingModule } from './account-routing.module';
import { MatPasswordStrengthModule } from '@angular-material-extensions/password-strength';


@NgModule({
  declarations: [
    ChangePasswordComponent,
    ChangeUserDetailsComponent,
    ManageAccountsComponent
  ],
  imports: [
    SharedModule,
    MatPasswordStrengthModule,
    AccountRoutingModule
  ]
})
export class AccountModule {
}
