import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '@env';
import { Injectable } from '@angular/core';
import { MessagePayload } from '@core/services/models/message.payload';
import { map } from 'rxjs/operators';
import { AllCoursesInfoResponsePayload } from '../models/all-courses-info-response.payload';
import { BasicCoursePayload } from '../models/basic-course.payload';

/**
 * Service for sending request related to courses.
 */
@Injectable({
  providedIn: 'root'
})
export class CourseService {

  constructor(private http: HttpClient) {
  }

  /**
   * Sends request to get information about all existing courses.
   */
  getAllCoursesInfo$(): Observable<AllCoursesInfoResponsePayload[]> {
    return this.http.get<AllCoursesInfoResponsePayload[]>(`${environment.backendUrl}/internal/courses`);
  }

  /**
   * Sends request to create new course.
   * @param course  basic information about course
   */
  createNewCourse$(course: BasicCoursePayload): Observable<AllCoursesInfoResponsePayload> {
    return this.http.post<AllCoursesInfoResponsePayload>(`${environment.backendUrl}/internal/courses`, course);
  }

  /**
   * Sends request to edit existing course.
   * @param courseId  unique ID of the course that should be edited
   * @param courseModel  basic information about course
   */
  editCourse$(courseId: number, courseModel: BasicCoursePayload): Observable<string> {
    return this.http.put<MessagePayload>(`${environment.backendUrl}/internal/courses/${courseId}`, courseModel)
      .pipe(
        map(message => message.message)
      );
  }

  /**
   * Sends request to delete a course.
   * @param courseId  unique ID of the course that should be deleted
   */
  deleteCourse$(courseId: number): Observable<string> {
    return this.http.delete<MessagePayload>(`${environment.backendUrl}/internal/courses/${courseId}`)
      .pipe(
        map(message => message.message)
      );
  }

  /**
   * Sends request to create multiple new courses at once.
   * @param courses  list of courses to be created
   */
  importCourses$(courses: BasicCoursePayload[]): Observable<string> {
    return this.http.post<MessagePayload>(`${environment.backendUrl}/internal/courses/import`, courses)
      .pipe(
        map(message => message.message)
      );
  }

  /**
   * Send request to get courses for current academic year.
   */
  getCurrentAcademicYearCoursesInfo$(): Observable<AllCoursesInfoResponsePayload[]> {
    return this.http.get<AllCoursesInfoResponsePayload[]>(`${environment.backendUrl}/internal/current-courses`);
  }

}
