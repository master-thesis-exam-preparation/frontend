import { NgModule } from '@angular/core';
import { CourseListComponent } from './components/course-list/course-list.component';
import { SharedModule } from '@shared/shared.module';
import { CourseRoutingModule } from './course-routing.module';
import { EditCourseDialogComponent } from './components/course-list/edit-course-dialog/edit-course-dialog.component';
import { NewCourseComponent } from './components/new-course/new-course.component';


@NgModule({
  declarations: [
    CourseListComponent,
    EditCourseDialogComponent,
    NewCourseComponent
  ],
  imports: [
    SharedModule,
    CourseRoutingModule
  ]
})
export class CourseModule {
}
