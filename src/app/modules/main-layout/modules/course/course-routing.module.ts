import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CourseListComponent } from './components/course-list/course-list.component';
import { AuthGuard } from '@core/services/guards/auth.guard';
import { Role } from '@core/role.enum';
import { UnsavedChangesGuard } from '@core/services/guards/unsaved-changes.guard';
import { NewCourseComponent } from './components/new-course/new-course.component';


const courseRoutes: Routes = [
  {
    path: 'new',
    component: NewCourseComponent,
    canActivate: [AuthGuard],
    canDeactivate: [UnsavedChangesGuard],
    data: {
      roles: [Role.ADMIN]
    }
  },
  {
    path: '',
    canActivate: [AuthGuard],
    data: {
      roles: [Role.ADMIN]
    },
    component: CourseListComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(courseRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class CourseRoutingModule {
}
