import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { CsvParseService } from '@core/services/csv-parse.service';
import { debounceTime, takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { BasicCoursePayload } from '../../models/basic-course.payload';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { UnsavedChangesGuard } from '@core/services/guards/unsaved-changes.guard';
import { CourseService } from '../../services/course.service';
import { Router } from '@angular/router';
import { SnackBarService } from '@core/services';

/**
 * Component for displaying the page with a form for importing courses in bulk.
 */
@Component({
  selector: 'app-new-course',
  templateUrl: './new-course.component.html',
  styleUrls: ['./new-course.component.scss']
})
export class NewCourseComponent implements OnInit, OnDestroy {
  // TODO possible refactor (similar component StudentListForm)

  /**
   * Sets MatSort to work even in ngIf
   * @param sort  MatSort to set
   */
  @ViewChild(MatSort, {static: false}) set sort(sort: MatSort) {
    this.coursesDataSource.sort = sort;
  }

  /**
   * Sets MatPaginator to work even in ngIf
   * @param paginator  MatPaginator to set
   */
  @ViewChild(MatPaginator, {static: false}) set paginator(paginator: MatPaginator) {
    this.coursesDataSource.paginator = paginator;
  }

  courseListForm: FormGroup;

  csvFile: File;
  acceptedFileTypes = ['text/csv', 'application/csv', 'text/comma-separated-values', '.csv'];
  encodingList = ['Windows-1250', 'ISO-8859-2', 'UTF-8'];

  showPreview = false;
  csvLines: string[] = [];
  headerColumns: string[] = ['abbreviation', 'name', 'academicYear'];
  coursesDataSource: MatTableDataSource<BasicCoursePayload> = new MatTableDataSource<BasicCoursePayload>();

  parsedCourses: BasicCoursePayload[];

  private ngUnsubscribe$ = new Subject();
  private wasReset = false;


  constructor(private fb: FormBuilder,
              private csvParseService: CsvParseService,
              private courseService: CourseService,
              private router: Router,
              private snackBarService: SnackBarService,
              private deactivateGuard: UnsavedChangesGuard) {
  }

  /**
   * Gets value of the coursesCSV text area in the form.
   */
  get coursesCsv(): AbstractControl {
    return this.courseListForm.get('coursesCsv');
  }

  /**
   * Gets value of the encoding select menu in the form.
   */
  get encoding(): AbstractControl {
    return this.courseListForm.get('encoding');
  }

  /**
   * Gets value of the separator input in the form.
   */
  get separator(): AbstractControl {
    return this.courseListForm.get('separator');
  }

  /**
   * Gets value of the abbreviationPosition input in the form.
   */
  get abbreviationPosition(): AbstractControl {
    return this.courseListForm.get('abbreviationPosition');
  }

  /**
   * Gets value of the namePosition input in the form.
   */
  get namePosition(): AbstractControl {
    return this.courseListForm.get('namePosition');
  }

  /**
   * Gets value of the academicYearPosition input in the form.
   */
  get academicYearPosition(): AbstractControl {
    return this.courseListForm.get('academicYearPosition');
  }

  private static sameNumberValidator(formGroup: AbstractControl): ValidationErrors | null {
    const abbreviationPosition: AbstractControl = formGroup.get('abbreviationPosition');
    const namePosition: AbstractControl = formGroup.get('namePosition');
    const academicYearPosition: AbstractControl = formGroup.get('academicYearPosition');

    return CsvParseService.sameNumberValidatorThree(abbreviationPosition, namePosition, academicYearPosition);
  }

  /**
   * Sets validators to the form inputs.
   */
  ngOnInit(): void {
    this.courseListForm = this.fb.group({
      coursesCsv: ['', [
        Validators.required
      ]],
      encoding: [this.encodingList[0], [
        Validators.required
      ]],
      separator: ['', [
        // this.allowWhitespaceValidator,
        Validators.required,
        Validators.maxLength(20)
      ]],
      abbreviationPosition: ['', [
        Validators.required,
        Validators.min(1),
        Validators.max(1000)
      ]],
      namePosition: ['', [
        Validators.required,
        Validators.min(1),
        Validators.max(1000)
      ]],
      academicYearPosition: ['', [
        Validators.required,
        Validators.min(1),
        Validators.max(1000)
      ]]
    }, {
      validators: [
        NewCourseComponent.sameNumberValidator
      ]
    });

    this.initFormValueChanges();
    this.initEncodingValueChanges();
    this.initCoursesCsvValueChanges();
  }

  /**
   * Unsubscribes from everything before destroying the component.
   */
  ngOnDestroy(): void {
    this.ngUnsubscribe$.next();
    this.ngUnsubscribe$.complete();
  }

  /**
   * Catches new file selected event. If this file is text file, reads in and sets its content to text area in form.
   * @param event  file selected event
   */
  onFileSelected(event: Event): void {
    const file: File = (event.target as HTMLInputElement).files[0];
    if (file) {
      this.csvFile = file;
      this.readFile();
    }
    (event.target as HTMLInputElement).value = null;
  }

  /**
   * Catches TAB key press in separator input.
   * @param event  TAB key press event
   */
  onTabSeparator(event: any): void {
    this.csvParseService.onTab(event, this.separator);
  }

  /**
   * Catches TAB key press in coursesCsv text area.
   * @param event  TAB key press event
   */
  onTabTextarea(event: any): void {
    this.csvParseService.onTab(event, this.coursesCsv);
  }

  /**
   * Clears all form input values.
   */
  clearForm(): void {
    this.csvFile = null;
    this.courseListForm.reset({
      encoding: this.encodingList[0]
    });
    this.deactivateGuard.canBeDeactivated = true;
    this.wasReset = true;
  }

  /**
   * Submit the form and send the import courses request.
   */
  onSubmit(): void {
    if (this.courseListForm.valid && this.isAllCoursesOk()) {
      this.courseService.importCourses$(this.parsedCourses)
        .subscribe({
          next: successMessage => {
            this.deactivateGuard.canBeDeactivated = true;
            this.snackBarService.openSuccessSnackBar(successMessage);
            this.router.navigate(['/courses']);
          }
        });
    }
  }

  private readFile(): void {
    this.csvParseService.readFile(this.csvFile, this.encoding.value)
      .subscribe({
        next: fileText => {
          this.courseListForm.patchValue({
            coursesCsv: fileText
          });
        }
      });
  }

  private initFormValueChanges(): void {
    this.courseListForm.valueChanges
      .pipe(
        debounceTime(250),
        takeUntil(this.ngUnsubscribe$)
      )
      .subscribe({
        next: formValues => {
          if (this.wasReset) {
            this.wasReset = false;
          } else {
            this.deactivateGuard.canBeDeactivated = false;
          }
          if (CsvParseService.isAllAttributesSet(formValues)) {
            this.parsedCourses = this.csvLines
              .map(line => {
                const parts = line.split(formValues.separator)
                  .map(part => part.trim());
                return {
                  abbreviation: parts[formValues.abbreviationPosition - 1],
                  name: parts[formValues.namePosition - 1],
                  academicYear: +parts[formValues.academicYearPosition - 1]
                };
              });
          } else {
            this.parsedCourses = [];
          }
          this.coursesDataSource.data = this.parsedCourses;
        }
      });
  }

  private initEncodingValueChanges(): void {
    this.encoding.valueChanges
      .pipe(takeUntil(this.ngUnsubscribe$))
      .subscribe({
        next: () => {
          if (this.csvFile) {
            this.readFile();
          }
        }
      });
  }

  private initCoursesCsvValueChanges(): void {
    this.coursesCsv.valueChanges
      .pipe(takeUntil(this.ngUnsubscribe$))
      .subscribe({
        next: text => {
          if (text) {
            this.csvLines = CsvParseService.splitTextToLines(text);
          }
        }
      });
  }

  private isAllCoursesOk(): boolean {
    const isAbbreviationNotOk = this.parsedCourses.some(course => !course.abbreviation);
    if (isAbbreviationNotOk) {
      this.snackBarService.openDangerSnackBar('Některý z předmětů nemá načtenou zkratku.\n' +
        'V náhledu zkontrolujte, zda všechny předměty mají správně nastavenou zkratku.');
      return false;
    }

    const isNameNotOk = this.parsedCourses.some(course => !course.name);
    if (isNameNotOk) {
      this.snackBarService.openDangerSnackBar('Některý z předmětů nemá načtený název.\n' +
        'V náhledu zkontrolujte, zda všechny předměty mají správně nastavený název.');
      return false;
    }

    const isAcademicYearNotOk = this.parsedCourses.some(course => !course.academicYear);
    if (isAcademicYearNotOk) {
      this.snackBarService.openDangerSnackBar('Některý z předmětů nemá správně načtený akademický rok.\n' +
        'V náhledu zkontrolujte, zda všechny předměty obsahují správně nastavený akademický rok.');
      return false;
    }

    return true;
  }
}
