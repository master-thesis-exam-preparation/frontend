import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditCourseDialogComponent } from './edit-course-dialog.component';
import { SharedModule } from '@shared/shared.module';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { AppModule } from '../../../../../../../app.module';

describe('EditCourseDialogComponent', () => {
  let component: EditCourseDialogComponent;
  let fixture: ComponentFixture<EditCourseDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [EditCourseDialogComponent],
      imports: [SharedModule, AppModule],
      providers: [
        {
          provide: MAT_DIALOG_DATA,
          useValue: {
            courseId: 1,
            abbreviation: 'PIS',
            name: 'Pokročilé informační systémy',
            academicYear: 2020
          }
        },
        {
          provide: MatDialogRef,
          useValue: []
        }
      ]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditCourseDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
