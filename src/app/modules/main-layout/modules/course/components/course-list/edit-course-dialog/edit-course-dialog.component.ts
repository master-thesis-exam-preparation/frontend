import { Component, Inject, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BasicCoursePayload } from '../../../models/basic-course.payload';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { AllCoursesInfoResponsePayload } from '../../../models/all-courses-info-response.payload';
import { CourseService } from '../../../services/course.service';
import { SnackBarService } from '@core/services';

/**
 * Component for displaying the popup window with form to edit existing course.
 */
@Component({
  selector: 'app-edit-course-dialog',
  templateUrl: './edit-course-dialog.component.html',
  styleUrls: ['./edit-course-dialog.component.scss']
})
export class EditCourseDialogComponent implements OnInit {

  editCourseForm: FormGroup;

  constructor(@Inject(MAT_DIALOG_DATA) public course: AllCoursesInfoResponsePayload,
              private dialogRef: MatDialogRef<EditCourseDialogComponent>,
              private fb: FormBuilder,
              private courseService: CourseService,
              private snackBarService: SnackBarService) {
  }

  /**
   * Gets value of the abbreviation input in the form.
   */
  get abbreviation(): AbstractControl {
    return this.editCourseForm.get('abbreviation');
  }

  /**
   * Gets value of the name input in the form.
   */
  get name(): AbstractControl {
    return this.editCourseForm.get('name');
  }

  /**
   * Gets value of the academicYear input in the form.
   */
  get academicYear(): AbstractControl {
    return this.editCourseForm.get('academicYear');
  }

  /**
   * Sets validators to the form inputs.
   */
  ngOnInit(): void {
    this.editCourseForm = this.fb.group({
      abbreviation: [this.course.abbreviation, [
        Validators.required,
        Validators.maxLength(20)
      ]],
      name: [this.course.name, [
        Validators.required,
        Validators.maxLength(255)
      ]],
      academicYear: [this.course.academicYear, [
        Validators.required,
        Validators.min(2000),
        Validators.max(2050)
      ]]
    });
  }

  /**
   * Submit the form and send the edit course request.
   */
  onSubmit(): void {
    if (this.editCourseForm.valid) {
      const editedCourse: BasicCoursePayload = {
        abbreviation: this.editCourseForm.value.abbreviation,
        name: this.editCourseForm.value.name,
        academicYear: this.editCourseForm.value.academicYear
      };

      this.courseService.editCourse$(this.course.courseId, editedCourse)
        .subscribe({
          next: response => {
            this.snackBarService.openSuccessSnackBar(response);
            this.dialogRef.close(editedCourse);
          }
        });
    }
  }
}
