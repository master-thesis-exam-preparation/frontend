import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { CourseService } from '../../services/course.service';
import { DialogService } from '@core/services/dialog.service';
import { SnackBarService } from '@core/services';
import { AllCoursesInfoResponsePayload } from '../../models/all-courses-info-response.payload';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BasicCoursePayload } from '../../models/basic-course.payload';
import { Observable } from 'rxjs';
import { ConfirmDialogOptionsData } from '@shared/modules/dialog/confirm-dialog/confirm-dialog.component';
import { MatDialog } from '@angular/material/dialog';
import { EditCourseDialogComponent } from './edit-course-dialog/edit-course-dialog.component';
import { take } from 'rxjs/operators';

/**
 * Component for displaying the page with list of all courses and form to create new one.
 */
@Component({
  selector: 'app-course-list',
  templateUrl: './course-list.component.html',
  styleUrls: ['./course-list.component.scss']
})
export class CourseListComponent implements OnInit, AfterViewInit {

  newCourseForm: FormGroup;

  headerColumns: string[] = ['abbreviation', 'name', 'academicYear', 'actions'];
  coursesDataSource: MatTableDataSource<AllCoursesInfoResponsePayload> = new MatTableDataSource<AllCoursesInfoResponsePayload>();

  isLoading = true;

  /**
   * Sets MatSort to work even in ngIf
   * @param sort  MatSort to set
   */
  @ViewChild(MatSort, {static: false}) set sort(sort: MatSort) {
    this.coursesDataSource.sort = sort;
  }

  /**
   * Sets MatPaginator to work even in ngIf
   * @param paginator  MatPaginator to set
   */
  @ViewChild(MatPaginator, {static: false}) set paginator(paginator: MatPaginator) {
    this.coursesDataSource.paginator = paginator;
  }

  constructor(private courseService: CourseService,
              private dialogService: DialogService,
              private snackBarService: SnackBarService,
              private fb: FormBuilder,
              private editCourseDialog: MatDialog) {
  }

  /**
   * Gets value of the abbreviation input in the form.
   */
  get abbreviation(): AbstractControl {
    return this.newCourseForm.get('abbreviation');
  }

  /**
   * Gets value of the name input in the form.
   */
  get name(): AbstractControl {
    return this.newCourseForm.get('name');
  }

  /**
   * Gets value of the academic year input in the form.
   */
  get academicYear(): AbstractControl {
    return this.newCourseForm.get('academicYear');
  }

  /**
   * Sets validators to the form inputs.
   */
  ngOnInit(): void {
    this.setCourseDataSource();

    this.newCourseForm = this.fb.group({
      abbreviation: ['', [
        Validators.required,
        Validators.maxLength(50)
      ]],
      name: ['', [
        Validators.required,
        Validators.maxLength(255)
      ]],
      academicYear: [(new Date()).getFullYear(), [
        Validators.required,
        Validators.min(2000),
        Validators.max(2050)
      ]]
    });
  }

  /**
   * Sets paginator and sort functionalities in table after component view init is done.
   */
  ngAfterViewInit(): void {
    this.coursesDataSource.paginator = this.paginator;
    this.coursesDataSource.sort = this.sort;
  }

  /**
   * Submit the form and send the create new course request.
   */
  onSubmit(): void {
    if (this.newCourseForm.valid) {
      const newCourse: BasicCoursePayload = {
        abbreviation: this.abbreviation.value,
        name: this.name.value,
        academicYear: this.academicYear.value
      };

      this.courseService.createNewCourse$(newCourse)
        .subscribe(
          createdCourse => {
            this.snackBarService.openSuccessSnackBar(
              `Předmět "${createdCourse.abbreviation} - ${createdCourse.name} (${createdCourse.academicYear})" byl vytvořen`
            );
            this.addCourseToTable(createdCourse);
            this.clearForm();
          });
    }
  }

  /**
   * Takes filter value from input and apply it to list of rooms.
   * @param event  filter key press event
   */
  applyFilter(event: Event): void {
    const filterValue = (event.target as HTMLInputElement).value;
    this.coursesDataSource.filter = filterValue.trim().toLowerCase();

    if (this.coursesDataSource.paginator) {
      this.coursesDataSource.paginator.firstPage();
    }
  }

  /**
   * Sends request to delete selected course.
   * @param course  course that should be deleted
   */
  deleteCourse(course: AllCoursesInfoResponsePayload): void {
    this.showConfirmDialog(course)
      .subscribe({
        next: confirmed => {
          if (confirmed) {
            this.courseService.deleteCourse$(course.courseId)
              .subscribe(message => {
                this.removeCourseFromTable(course.courseId);
                this.snackBarService.openSuccessSnackBar(message);
              });
          }
        }
      });
  }

  /**
   * Sends request to edit selected course.
   * @param course  course that should be edited
   */
  editCourse(course: AllCoursesInfoResponsePayload): void {
    // open dialog with edit form
    this.editCourseDialog.open(EditCourseDialogComponent, {
      maxWidth: '600px',
      data: course
    }).afterClosed()
      .pipe(take(1))
      .subscribe({
        next: editedCourse => {
          if (!!editedCourse) { // not empty or undefined
            course.name = editedCourse.name;
            course.abbreviation = editedCourse.abbreviation;
            course.academicYear = editedCourse.academicYear;
          }
        }
      });
  }

  private showConfirmDialog(course: AllCoursesInfoResponsePayload): Observable<boolean> {
    const options: ConfirmDialogOptionsData = {
      title: 'Odstranit předmět?',
      message: `Opravdu chcete odstranit předmět "${course.abbreviation} - ${course.name} (${course.academicYear})"? Toto rozhodnutí nelze vzít zpět.`,
      cancelText: 'ZRUŠIT',
      cancelColor: 'primary',
      confirmText: 'ODSTRANIT',
      confirmColor: 'warn'
    };
    return this.dialogService.openConfirmDialog$(options);
  }

  private setCourseDataSource(): void {
    this.isLoading = true;
    this.courseService
      .getAllCoursesInfo$()
      .subscribe(allCoursesInfo => {
          this.coursesDataSource.data = allCoursesInfo;
          this.isLoading = false;
        }
      );
  }

  private addCourseToTable(course: AllCoursesInfoResponsePayload): void {
    const data = this.coursesDataSource.data;
    data.push(course);
    this.coursesDataSource.data = data;
  }

  private removeCourseFromTable(courseId: number): void {
    this.coursesDataSource.data = this.coursesDataSource.data
      .filter(tableCourse => courseId !== tableCourse.courseId);
  }

  private clearForm(): void {
    this.newCourseForm.reset();

    Object.keys(this.newCourseForm.controls).forEach(key => {
      this.newCourseForm.get(key).setErrors(null);
    });
  }
}
