import { RoomPlanInfoPayload } from '../../rooms/models/room-plan-info.payload';
import { BasicRoomPayload } from '../../rooms/models/basic-room.payload';

/**
 * Used to store information about free rooms.
 */
export interface FreeRoomResponsePayload extends BasicRoomPayload {
  roomId: number;
  roomPlans: RoomPlanInfoPayload;
}

/**
 * Used to store information about free rooms.
 */
export class Room implements FreeRoomResponsePayload {
  roomId: number;
  abbreviation: string;
  name: string;
  roomPlans: RoomPlanInfoPayload;

  constructor() {
  }
}
