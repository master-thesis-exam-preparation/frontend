/**
 * Used to store data to find free rooms at given time.
 */
export interface FreeRoomRequestPayload {
  startTime: number;
  termRunLength: number;
  numberOfTermRuns: number;
  termId: number;
}
