/**
 * Used to store basic information about course.
 */
export interface BasicCoursePayload {
  abbreviation: string;
  name: string;
  academicYear: number;
}
