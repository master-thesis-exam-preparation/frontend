import { BasicCoursePayload } from './basic-course.payload';

/**
 * Used to store basic information about course and ID of the course.
 */
export interface AllCoursesInfoResponsePayload extends BasicCoursePayload {
  courseId: number;
}
