import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '@env';
import { NewRoomRequestPayload } from '../models/new-room-request.payload';
import { Injectable } from '@angular/core';
import { MessagePayload } from '@core/services/models/message.payload';
import { map } from 'rxjs/operators';
import { AllRoomsInfoResponsePayload } from '../models/all-rooms-info-response.payload';
import { RoomDetailsResponsePayload } from '../models/room-details-response.payload';
import { EditRoomResponsePayload } from '../models/edit-room-response.payload';
import { FreeRoomRequestPayload } from '../../course/models/free-room-request.payload';
import { FreeRoomResponsePayload } from '../../course/models/free-room-response.payload';

/**
 * Service for sending request related to rooms.
 */
@Injectable({
  providedIn: 'root'
})
export class RoomService {

  constructor(private http: HttpClient) {
  }

  /**
   * Sends request to get information about all existing rooms.
   */
  getAllRoomsInfo$(): Observable<AllRoomsInfoResponsePayload[]> {
    return this.http.get<AllRoomsInfoResponsePayload[]>(`${environment.backendUrl}/public/rooms`);
  }

  /**
   * Sends request to create new room.
   * @param roomModel  basic information about room
   */
  createNewRoom$(roomModel: NewRoomRequestPayload): Observable<AllRoomsInfoResponsePayload> {
    return this.http.post<AllRoomsInfoResponsePayload>(`${environment.backendUrl}/internal/rooms`, roomModel);
  }

  /**
   * Sends request to get detailed public information about specific room.
   * @param roomId  unique ID of the room
   */
  getRoomDetailsPublic$(roomId: number): Observable<RoomDetailsResponsePayload> {
    return this.http.get<RoomDetailsResponsePayload>(`${environment.backendUrl}/public/rooms/${roomId}`);
  }

  /**
   * Sends request to get detailed internal information about specific room.
   * @param roomId  unique ID of the room
   */
  getRoomDetailsInternal$(roomId: number): Observable<RoomDetailsResponsePayload> {
    return this.http.get<RoomDetailsResponsePayload>(`${environment.backendUrl}/internal/rooms/${roomId}`);
  }

  /**
   * Sends request to get information about specific room for edit room form.
   * @param roomId  unique ID of the room which should be edited
   */
  getEditRoomDetails$(roomId: number): Observable<EditRoomResponsePayload> {
    return this.http.get<EditRoomResponsePayload>(`${environment.backendUrl}/internal/rooms/${roomId}/edit`);
  }

  /**
   * Sends request to get information about room that will be used to create new room.
   * @param roomId  unique ID of the room
   */
  getEditRoomDetailsForCreation$(roomId: number): Observable<EditRoomResponsePayload> {
    return this.http.get<EditRoomResponsePayload>(`${environment.backendUrl}/internal/rooms/${roomId}/form-info`);
  }

  /**
   * Sends request to edit existing room
   * @param roomId  unique ID of the room which should be edited
   * @param roomModel  information necessary to edit existing room (room size, cell types etc.)
   */
  editRoom$(roomId: number, roomModel: NewRoomRequestPayload): Observable<string> {
    return this.http.put<MessagePayload>(`${environment.backendUrl}/internal/rooms/${roomId}`, roomModel)
      .pipe(
        map(message => message.message)
      );
  }

  /**
   * Sends request to delete a room.
   * @param roomId  unique ID of room which should be deleted
   */
  deleteRoom$(roomId: number): Observable<string> {
    return this.http.delete<MessagePayload>(`${environment.backendUrl}/internal/rooms/${roomId}`)
      .pipe(
        map(message => message.message)
      );
  }

  /**
   * Sends request to get all free room at given time window.
   * @param freeRoomSearchParams  data to find free rooms at given time
   */
  getFreeRoomsInfo$(freeRoomSearchParams: FreeRoomRequestPayload): Observable<FreeRoomResponsePayload[]> {
    return this.http.post<FreeRoomResponsePayload[]>(`${environment.backendUrl}/internal/rooms/free`, freeRoomSearchParams);
  }
}
