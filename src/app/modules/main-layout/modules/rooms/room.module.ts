import { NgModule } from '@angular/core';
import { NewRoomComponent } from './components/new-room/new-room.component';
import { SharedModule } from '@shared/shared.module';
import { RoomRoutingModule } from './room-routing.module';
import { RoomListComponent } from './components/room-list/room-list.component';
import { RoomDetailsComponent } from './components/room-details/room-details.component';
import { RoomEditComponent } from './components/room-edit/room-edit.component';


@NgModule({
  declarations: [
    NewRoomComponent,
    RoomListComponent,
    RoomDetailsComponent,
    RoomEditComponent
  ],
  imports: [
    SharedModule,
    RoomRoutingModule
  ]
})
export class RoomModule {
}
