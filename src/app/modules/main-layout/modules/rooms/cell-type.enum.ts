export enum CellType {
  AISLE = 'AISLE',
  SEAT = 'SEAT',
  DESK = 'DESK',
  BLOCKED = 'BLOCKED',
  SELECTED = 'SELECTED'
}
