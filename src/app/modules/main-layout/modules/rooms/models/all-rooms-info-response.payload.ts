import { BasicRoomPayload } from './basic-room.payload';

/**
 * Used to store more detailed information about room, but not too specific.
 */
export interface AllRoomsInfoResponsePayload extends BasicRoomPayload {
  roomId: number;
  capacities: number[];
  creatorNameWithDegrees: string;
  creatorEmail: string;
}
