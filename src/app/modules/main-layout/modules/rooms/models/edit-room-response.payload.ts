import { BasicRoomPayload } from './basic-room.payload';
import { CellType } from '../cell-type.enum';

/**
 * Used to store information necessary for room edit.
 */
export interface EditRoomResponsePayload extends BasicRoomPayload {
  description: string;
  numberOfRows: number;
  numberOfColumns: number;
  cells: CellType[][];
}
