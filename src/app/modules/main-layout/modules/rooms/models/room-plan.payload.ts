import { SpacingBetweenStudents } from '../../term/spacing-between-students.enum';
import { CellPayload } from './cell.payload';

/**
 * Used to store information about room plan (includes 2D list of cells of room plan).
 */
export interface RoomPlanPayload {
  capacity: number;
  spacing: SpacingBetweenStudents;
  cells: CellPayload[][];
}
