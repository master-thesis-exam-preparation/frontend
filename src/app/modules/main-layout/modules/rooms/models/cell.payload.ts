import { CellType } from '../cell-type.enum';

/**
 * Used to store information about cell ID and cell type.
 */
export interface CellPayload {
  cellId: number;
  cellType: CellType;
}
