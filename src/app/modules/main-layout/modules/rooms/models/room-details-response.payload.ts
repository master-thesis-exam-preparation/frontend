import { BasicRoomPayload } from './basic-room.payload';
import { RoomPlanPayload } from './room-plan.payload';
import { UserInfoPayload } from '@core/services/models/userInfo.payload';

/**
 * Used to store detailed information about room (basic info + room creator, room description etc.).
 */
export interface RoomDetailsResponsePayload extends BasicRoomPayload {
  creator?: UserInfoPayload;
  description: string;
  roomPlans: RoomPlanPayload[];
}
