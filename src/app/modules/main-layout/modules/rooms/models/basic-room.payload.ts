/**
 * Used to store basic information about room (room name and abbreviation).
 */
export interface BasicRoomPayload {
  name: string;
  abbreviation: string;
}
