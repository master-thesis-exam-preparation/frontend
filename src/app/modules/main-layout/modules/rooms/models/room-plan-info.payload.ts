import { SpacingBetweenStudents } from '../../term/spacing-between-students.enum';

/**
 * Used to store information about room plan (unique ID, capacity and spacing between students).
 */
export interface RoomPlanInfoPayload {
  roomPlanId: number;
  capacity: number;
  spacing: SpacingBetweenStudents;
}
