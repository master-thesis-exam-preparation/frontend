import { BasicRoomPayload } from './basic-room.payload';
import { CellType } from '../cell-type.enum';

/**
 * Used to store information necessary to create new room (room size, cell types etc.).
 */
export interface NewRoomRequestPayload extends BasicRoomPayload {
  description: string;
  cells: CellType[][];
}
