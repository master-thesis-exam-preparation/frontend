import { Component, OnDestroy, OnInit } from '@angular/core';
import { RoomService } from '../../services/room.service';
import { RoomDetailsResponsePayload } from '../../models/room-details-response.payload';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, Subject, throwError } from 'rxjs';
import { catchError, takeUntil } from 'rxjs/operators';
import { AuthService, SnackBarService } from '@core/services';
import { Role } from '@core/role.enum';
import { ConfirmDialogOptionsData } from '@shared/modules/dialog/confirm-dialog/confirm-dialog.component';
import { DialogService } from '@core/services/dialog.service';

/**
 * Component for displaying the page with details of specific room.
 */
@Component({
  selector: 'app-room-details',
  templateUrl: './room-details.component.html',
  styleUrls: ['./room-details.component.scss']
})
export class RoomDetailsComponent implements OnInit, OnDestroy {

  roomDetails$: Observable<RoomDetailsResponsePayload>;
  loggedUserEmail: string;
  isAdmin = false;

  private roomId: number;
  private ngUnsubscribe$ = new Subject();


  constructor(private roomService: RoomService,
              private route: ActivatedRoute,
              private router: Router,
              private authService: AuthService,
              private snackBarService: SnackBarService,
              private dialogService: DialogService) {
  }

  /**
   * Subscribes to subject to get logged user role.
   */
  ngOnInit(): void {
    this.authService.loggedUserInfo$
      .pipe(
        takeUntil(this.ngUnsubscribe$)
      )
      .subscribe({
        next: userInfo => {
          if (userInfo) {
            this.loggedUserEmail = userInfo.email;
            this.isAdmin = userInfo.roles.includes(Role.ADMIN);
          } else {
            this.loggedUserEmail = null;
            this.isAdmin = false;
          }
        }
      });

    this.roomId = Number(this.route.snapshot.paramMap.get('id'));
    this.getRoomDetails();
  }

  /**
   * Unsubscribes from everything before destroying the component.
   */
  ngOnDestroy(): void {
    this.ngUnsubscribe$.next();
    this.ngUnsubscribe$.complete();
  }

  /**
   * Shows confirmation dialog before deleting the room.
   * @param roomDetails  information about room that should be deleted
   */
  showConfirmDialog(roomDetails: RoomDetailsResponsePayload): void {
    const options: ConfirmDialogOptionsData = {
      title: 'Odstranit místnost?',
      message: `Opravdu chcete odstranit místnost "${roomDetails.abbreviation} - ${roomDetails.name}"? Toto rozhodnutí nelze vzít zpět.`,
      cancelText: 'ZRUŠIT',
      cancelColor: 'primary',
      confirmText: 'ODSTRANIT',
      confirmColor: 'warn'
    };
    this.dialogService.openConfirmDialog$(options)
      .subscribe(confirmed => {
        if (confirmed) {
          this.deleteAssignment();
        }
      });
  }

  private deleteAssignment(): void {
    this.roomService.deleteRoom$(this.roomId)
      .subscribe(message => {
        this.snackBarService.openSuccessSnackBar(message);
        this.router.navigate(['/rooms']);
      });
  }

  private getRoomDetails(): void {
    let obs: Observable<RoomDetailsResponsePayload>;
    if (this.loggedUserEmail) {
      obs = this.roomService.getRoomDetailsInternal$(this.roomId);
    } else {
      obs = this.roomService.getRoomDetailsPublic$(this.roomId);
    }
    this.roomDetails$ = obs.pipe(
      catchError(err => {
        this.router.navigate(['/rooms']);
        return throwError(err);
      })
    );
  }
}
