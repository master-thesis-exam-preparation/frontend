import { Component, HostListener, Input, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CellType } from '../../cell-type.enum';
import { NewRoomRequestPayload } from '../../models/new-room-request.payload';
import { RoomService } from '../../services/room.service';
import { SnackBarService } from '@core/services';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { take } from 'rxjs/operators';
import { AllRoomsInfoResponsePayload } from '../../models/all-rooms-info-response.payload';
import { EditRoomResponsePayload } from '../../models/edit-room-response.payload';
import { UnsavedChangesGuard } from '@core/services/guards/unsaved-changes.guard';

/**
 * Component for displaying the page with a form for creating new room and its room plans.
 */
@Component({
  selector: 'app-new-room',
  templateUrl: './new-room.component.html',
  styleUrls: ['./new-room.component.scss']
})
export class NewRoomComponent implements OnInit {

  /**
   * Determines whether form is for creating new room or editing an existing one.
   */
  @Input() edit = false;

  /**
   * Information about existing room.
   */
  @Input() formFillData$: Observable<EditRoomResponsePayload>;

  newRoomForm: FormGroup;
  newRoom: NewRoomRequestPayload;

  legendValue: CellType = CellType.AISLE;

  numberOfRows = 1;
  numberOfColumns = 1;
  numberOfSeats = 1;
  helperNumberOfSeats = 0;
  columnNumbers: number[];
  rowNumbers: number[];

  mouseDown = false;
  startCoordinates: [number, number];
  cellTypeMatrix: CellType[][];

  selectListRooms$: Observable<AllRoomsInfoResponsePayload[]>;

  private fstChange = true;

  minSize = 1;
  maxSize = 150;

  constructor(private fb: FormBuilder,
              private router: Router,
              private route: ActivatedRoute,
              private roomService: RoomService,
              private snackBarService: SnackBarService,
              private deactivateGuard: UnsavedChangesGuard) {
  }

  /**
   * Gets value of the rows input in the form.
   */
  get rows(): AbstractControl {
    return this.newRoomForm.get('rows');
  }

  /**
   * Gets value of the columns input in the form.
   */
  get columns(): AbstractControl {
    return this.newRoomForm.get('columns');
  }

  /**
   * Gets value of the abbreviation input in the form.
   */
  get abbreviation(): AbstractControl {
    return this.newRoomForm.get('abbreviation');
  }

  /**
   * Gets value of the name input in the form.
   */
  get name(): AbstractControl {
    return this.newRoomForm.get('name');
  }

  /**
   * Gets value of the description input in the form.
   */
  get description(): AbstractControl {
    return this.newRoomForm.get('description');
  }

  /**
   * Sets validators to the form inputs. Gets list of existing rooms.
   */
  ngOnInit(): void {
    if (this.edit && !this.formFillData$) {
      throw new TypeError('The input \'formFillData$\' is required');
    }

    this.initNewRoom();
    this.newRoomForm = this.fb.group({
      rows: ['', [
        Validators.required,
        Validators.min(this.minSize),
        Validators.max(this.maxSize)
      ]],
      columns: ['', [
        Validators.required,
        Validators.min(this.minSize),
        Validators.max(this.maxSize)
      ]],
      abbreviation: ['', [
        Validators.required,
        Validators.maxLength(20)
      ]],
      name: ['', [
        Validators.required,
        Validators.maxLength(255)
      ]],
      description: ['', [
        Validators.maxLength(65535)
      ]]
    });

    this.selectListRooms$ = this.roomService.getAllRoomsInfo$();

    if (this.edit) {
      this.getAndSetFormData();
    }

    this.onFormChanges();
  }

  /**
   * Submit the form and send the create a new room request.
   */
  onSubmit(): void {
    if (this.newRoomForm.valid) {
      const formValues = this.newRoomForm.value;
      this.newRoom.abbreviation = formValues.abbreviation;
      this.newRoom.name = formValues.name;
      this.newRoom.description = formValues.description;
      if (!this.edit) {
        this.roomService.createNewRoom$(this.newRoom)
          .subscribe(
            createdRoom => {
              this.snackBarService.openSuccessSnackBar(
                `Místnost "${createdRoom.abbreviation}" s názvem " ${createdRoom.name}" byla vytvořena`
              );
              this.deactivateGuard.canBeDeactivated = true;
              this.router.navigate(['/rooms']);
            });
      } else {
        const roomId = Number(this.route.snapshot.paramMap.get('id'));
        this.roomService.editRoom$(roomId, this.newRoom)
          .subscribe(
            response => {
              this.snackBarService.openSuccessSnackBar(response);
              this.deactivateGuard.canBeDeactivated = true;
              this.router.navigate(['/rooms']);
            });
      }
    }
  }

  /**
   * Clears all form input values.
   */
  clearForm(): void {
    this.newRoomForm.reset();
    this.newRoom.cells = [];
    this.deactivateGuard.canBeDeactivated = true;
  }

  /**
   * Sets new legend value after selected cell type in legend was changed.
   * @param value  new selected cell type
   */
  onLegendValChange(value: CellType): void {
    this.legendValue = value;
  }

  /**
   * Set start coordinates on multi-cell selection start
   * @param x  column number
   * @param y  row number
   */
  onSelectionStart(x: number, y: number): void {
    if (window.getSelection) {
      window.getSelection().removeAllRanges();
    }
    this.mouseDown = true;
    this.helperNumberOfSeats = this.numberOfSeats;
    this.startCoordinates = [x, y];
    this.setEndCoordinates(x, y);
  }

  /**
   * Sets end coordinates on the cell where mouse button was released.
   * @param x  column number
   * @param y  row number
   */
  onSelection(x: number, y: number): void {
    if (!this.mouseDown) {
      return;
    }
    this.setEndCoordinates(x, y);
  }

  /**
   * Updates room plan cells after mouse button was released.
   */
  @HostListener('mouseup', ['$event'])
  onSelectionEnd(): void {
    if (this.mouseDown) {
      this.mouseDown = false;
      this.cellTypeMatrix.map((row, i) =>
        row.map((cell, j) =>
          this.newRoom.cells[i][j] = cell
        )
      );
    }
  }

  /**
   * Fills form with data of room selected in select menu.
   * @param roomInfo  information about selected room
   */
  fillFormWithSelectedRoomData(roomInfo: AllRoomsInfoResponsePayload): void {
    this.formFillData$ = this.roomService.getEditRoomDetailsForCreation$(roomInfo.roomId);
    this.deactivateGuard.canBeDeactivated = false;
    this.getAndSetFormData();
  }

  private initNewRoom(): void {
    this.newRoom = {
      abbreviation: '',
      name: '',
      description: '',
      cells: []
    };
  }

  private onFormChanges(): void {
    this.newRoomForm.valueChanges
      .subscribe(val => {
        if (this.fstChange && this.edit) {
          this.fstChange = false;
        } else {
          this.deactivateGuard.canBeDeactivated = false;
        }

        if (val?.rows <= 150 && val?.columns <= 150 && val?.rows > 0 && val?.columns > 0) {
          const cells: CellType[][] = [];
          for (let i = 0; i < val.rows; i++) {
            cells[i] = [];
            for (let j = 0; j < val.columns; j++) {
              if (this.newRoom.cells.length === 0 || i >= this.numberOfRows || j >= this.numberOfColumns) {
                // room was just created or new rows/columns was added
                cells[i][j] = CellType.SEAT;
              } else {
                // rows/columns was removed, do not change anything
                cells[i][j] = this.newRoom.cells[i][j];
              }
            }
          }
          this.newRoom.cells = cells;
          this.numberOfRows = val.rows;
          this.numberOfColumns = val.columns;

          this.fillCellTypeMatrix();

          this.updateNumberOfSeats();
          this.updateRowAndColumnNumbers();
        }
      });
  }

  private updateNumberOfSeats(): void {
    this.numberOfSeats = this.cellTypeMatrix.reduce(
      (accumulator, value) => accumulator.concat(value), []
    ).filter(
      cell => cell === CellType.SEAT
    ).length;
  }

  private updateRowAndColumnNumbers(): void {
    // row numbers
    this.rowNumbers = this.createNumbering(
      this.cellTypeMatrix.map(row =>
        row.some(cell =>
          cell === CellType.SEAT || cell === CellType.BLOCKED
        )
      )
    ).reverse(); // for easier rendering

    // column numbers
    this.columnNumbers = this.createNumbering(
      this.cellTypeMatrix[0].map((_, cellIndex) =>
        this.cellTypeMatrix.map(row =>
          row[cellIndex]  // transpose the matrix
        )
      ).map(row =>
        row.some(cell =>
          cell === CellType.SEAT || cell === CellType.BLOCKED
        )
      )
    );
  }

  private getAndSetFormData(): void {
    this.formFillData$
      .pipe(take(1))
      .subscribe({
        next: roomData => {
          this.newRoom.cells = roomData.cells;
          this.numberOfRows = roomData.numberOfRows;
          this.numberOfColumns = roomData.numberOfColumns;
          this.newRoomForm.patchValue({
            rows: roomData.numberOfRows,
            columns: roomData.numberOfColumns,
            abbreviation: roomData.abbreviation,
            name: roomData.name,
            description: roomData.description
          });
        },
        error: () => this.router.navigate(['/rooms'])
      });
  }

  private createNumbering(isSeatInArray: boolean[]): any[] {
    let currentNumber = 0;
    return isSeatInArray.map(bool => {
      if (bool) {
        currentNumber++;
        return currentNumber;
      } else {
        return '';
      }
    });
  }

  private fillCellTypeMatrix(): void {
    this.cellTypeMatrix = this.newRoom.cells.map(row =>
      row.map(cell => cell)
    );
  }

  private setEndCoordinates(endX: number, endY: number): void {
    this.fillCellTypeMatrix();
    const topLeft: [number, number] = [Math.min(this.startCoordinates[1], endY), Math.min(this.startCoordinates[0], endX)];
    const bottomRight: [number, number] = [Math.max(this.startCoordinates[1], endY), Math.max(this.startCoordinates[0], endX)];
    this.numberOfSeats = this.helperNumberOfSeats;
    let numberOfSeatDifference = 0;
    for (let row = topLeft[1]; row <= bottomRight[1]; row++) {
      for (let column = topLeft[0]; column <= bottomRight[0]; column++) {
        if (this.cellTypeMatrix[row - 1][column - 1] !== this.legendValue) {
          this.deactivateGuard.canBeDeactivated = false;
          if (this.cellTypeMatrix[row - 1][column - 1] === CellType.SEAT) {
            numberOfSeatDifference--;
          } else if (this.legendValue === CellType.SEAT) {
            numberOfSeatDifference++;
          }
          this.cellTypeMatrix[row - 1][column - 1] = this.legendValue;
          this.updateRowAndColumnNumbers();
        }
      }
    }
    this.numberOfSeats += numberOfSeatDifference;
  }
}
