import { Component, OnInit } from '@angular/core';
import { RoomService } from '../../services/room.service';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { EditRoomResponsePayload } from '../../models/edit-room-response.payload';

/**
 * Component for displaying the page with a form for editing existing room.
 */
@Component({
  selector: 'app-room-edit',
  templateUrl: './room-edit.component.html',
  styleUrls: ['./room-edit.component.scss']
})
export class RoomEditComponent implements OnInit {

  roomData$: Observable<EditRoomResponsePayload>;

  constructor(private roomService: RoomService,
              private route: ActivatedRoute) {
  }

  /**
   * Gets room id from router URL and sends request to get room edit data.
   */
  ngOnInit(): void {
    const roomId = Number(this.route.snapshot.paramMap.get('id'));
    this.roomData$ = this.roomService.getEditRoomDetails$(roomId);
  }
}
