import { AfterViewInit, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { AllRoomsInfoResponsePayload } from '../../models/all-rooms-info-response.payload';
import { RoomService } from '../../services/room.service';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { Router } from '@angular/router';
import { DialogService } from '@core/services/dialog.service';
import { AuthService, SnackBarService } from '@core/services';
import { combineLatest, Observable, Subject } from 'rxjs';
import { map, takeUntil } from 'rxjs/operators';
import { ConfirmDialogOptionsData } from '@shared/modules/dialog/confirm-dialog/confirm-dialog.component';

/**
 * Component for displaying the page with list of all rooms.
 */
@Component({
  selector: 'app-room-list',
  templateUrl: './room-list.component.html',
  styleUrls: ['./room-list.component.scss']
})
export class RoomListComponent implements OnInit, AfterViewInit, OnDestroy {

  fstRowHeaderColumns: string[] = ['room_h', 'capacity_h'];
  sndRowHeaderColumns: string[] = ['abbreviation', 'name', 'cap_0', 'cap_1', 'cap_2'];
  roomsDataSource: MatTableDataSource<AllRoomsInfoResponsePayload> = new MatTableDataSource<AllRoomsInfoResponsePayload>();

  isUserAdminOrEmployee = false;
  isUserAdmin = false;
  isUserEmployee = false;

  isLoading = true;

  /**
   * Sets MatSort to work even in ngIf
   * @param sort  MatSort to set
   */
  @ViewChild(MatSort, {static: false}) set sort(sort: MatSort) {
    this.roomsDataSource.sort = sort;
  }

  /**
   * Sets MatPaginator to work even in ngIf
   * @param paginator  MatPaginator to set
   */
  @ViewChild(MatPaginator, {static: false}) set paginator(paginator: MatPaginator) {
    this.roomsDataSource.paginator = paginator;
  }

  private ngUnsubscribe$ = new Subject();

  constructor(private roomService: RoomService,
              private dialogService: DialogService,
              private snackBarService: SnackBarService,
              private router: Router,
              private authService: AuthService) {
  }

  /**
   * Sends request to get information about all created room on component init.
   */
  ngOnInit(): void {
    this.setRoomDataSource();
    // because sort header ID does not match data properties
    this.setSortingDataAccessor();
    this.getUserRoles();
  }

  /**
   * Sets paginator and sort functionalities in table after component view init is done.
   */
  ngAfterViewInit(): void {
    this.roomsDataSource.paginator = this.paginator;
    this.roomsDataSource.sort = this.sort;
  }

  /**
   * Unsubscribes from everything before destroying the component.
   */
  ngOnDestroy(): void {
    this.ngUnsubscribe$.next();
    this.ngUnsubscribe$.complete();
  }

  /**
   * Takes filter value from input and apply it to list of rooms.
   * @param event  filter key press event
   */
  applyFilter(event: Event): void {
    const filterValue = (event.target as HTMLInputElement).value;
    this.roomsDataSource.filter = filterValue.trim().toLowerCase();

    if (this.roomsDataSource.paginator) {
      this.roomsDataSource.paginator.firstPage();
    }
  }

  /**
   * Navigates user to the page with more details about room.
   * @param row  information about the room for which more details are to be displayed
   */
  showRoomDetails(row: AllRoomsInfoResponsePayload): void {
    this.router.navigate(['rooms', row.roomId]);
  }

  /**
   * Shows confirmation dialog before deleting the room.
   * @param room  information about room that should be deleted
   */
  showConfirmDialog(room: AllRoomsInfoResponsePayload): void {
    const options: ConfirmDialogOptionsData = {
      title: 'Odstranit místnost?',
      message: `Opravdu chcete odstranit místnost "${room.abbreviation} - ${room.name}"? Toto rozhodnutí nelze vzít zpět.`,
      cancelText: 'ZRUŠIT',
      cancelColor: 'primary',
      confirmText: 'ODSTRANIT',
      confirmColor: 'warn'
    };
    this.dialogService.openConfirmDialog$(options)
      .subscribe(confirmed => {
        if (confirmed) {
          this.deleteRoom(room.roomId);
        }
      });
  }

  /**
   * Determines whether currently logged user is creator of room.
   * @param creatorEmail  e-mail of the room creator
   */
  isUserRoomCreator(creatorEmail: string): Observable<boolean> {
    return this.authService.loggedUserInfo$
      .pipe(
        map(userInfo => userInfo?.email === creatorEmail)
      );
  }

  private deleteRoom(roomId: number): void {
    this.roomService.deleteRoom$(roomId)
      .subscribe(message => {
        this.roomsDataSource.data = this.roomsDataSource.data.filter(room => room.roomId !== roomId);
        this.snackBarService.openSuccessSnackBar(message);
      });
  }

  private setRoomDataSource(): void {
    this.isLoading = true;
    this.roomService
      .getAllRoomsInfo$()
      .subscribe(allRoomsInfo => {
          this.roomsDataSource.data = allRoomsInfo;
          this.isLoading = false;
        }
      );
  }

  private setSortingDataAccessor(): void {
    this.roomsDataSource.sortingDataAccessor = (item, property) => {
      switch (property) {
        case 'creator_h':
          return item.creatorNameWithDegrees;
        case 'cap_0':
          return item.capacities[0];
        case 'cap_1':
          return item.capacities[1];
        case 'cap_2':
          return item.capacities[2];
        default:
          return item[property];
      }
    };
  }

  private getUserRoles(): void {
    combineLatest([
      this.authService.isUserEmployee$(),
      this.authService.isUserAdmin$()
    ])
      .pipe(takeUntil(this.ngUnsubscribe$))
      .subscribe({
        next: res => {
          const valBefore = this.isUserAdminOrEmployee;
          this.isUserEmployee = res[0];
          this.isUserAdmin = res[1];
          this.isUserAdminOrEmployee = res[0] || res[1];
          if (this.isUserAdminOrEmployee && !valBefore) { // new value is true and before was false
            this.fstRowHeaderColumns.push('creator_h', 'action_h');
            this.sndRowHeaderColumns.push('creator', 'action');
          } else if (!this.isUserAdminOrEmployee && valBefore) {  // new value is false and before was true
            this.fstRowHeaderColumns = this.fstRowHeaderColumns.slice(0, -2);
            this.sndRowHeaderColumns = this.sndRowHeaderColumns.slice(0, -2);
          }
        },
        error: err => console.log(err)
      });
  }
}
