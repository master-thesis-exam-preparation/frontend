import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NewRoomComponent } from './components/new-room/new-room.component';
import { RoomListComponent } from './components/room-list/room-list.component';
import { AuthGuard } from '@core/services/guards/auth.guard';
import { Role } from '@core/role.enum';
import { RoomDetailsComponent } from './components/room-details/room-details.component';
import { RoomEditComponent } from './components/room-edit/room-edit.component';
import { UnsavedChangesGuard } from '@core/services/guards/unsaved-changes.guard';


const roomRoutes: Routes = [
  {
    path: 'new',
    component: NewRoomComponent,
    canActivate: [AuthGuard],
    canDeactivate: [UnsavedChangesGuard],
    data: {
      roles: [Role.ADMIN, Role.EMPLOYEE]
    }
  },
  {
    path: ':id/edit',
    component: RoomEditComponent,
    canActivate: [AuthGuard],
    canDeactivate: [UnsavedChangesGuard],
    data: {
      roles: [Role.ADMIN, Role.EMPLOYEE]
    }
  },
  {
    path: ':id',
    component: RoomDetailsComponent
  },
  {
    path: '',
    component: RoomListComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(roomRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class RoomRoutingModule {
}
