import { Component, Input } from '@angular/core';
import { AuthService, SnackBarService } from '@core/services';

/**
 * Component for displaying the bar with menu at the top of the page.
 */
@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: []
})
export class MenuComponent {

  /**
   * Determines whether current user is logged-in or not.
   */
  @Input() isUserLoggedIn: boolean;

  menuOpen = false;

  constructor(private authService: AuthService,
              private snackBarService: SnackBarService) {
  }

  /**
   * Sends request to log-out current user from the application.
   */
  logout(): void {
    this.authService.logout$()
      .subscribe(message => {
        this.snackBarService.openSuccessSnackBar(message);
      });
  }
}
