import { Component, OnDestroy, OnInit } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { MenuItemsService } from '@core/services';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

/**
 * Component for displaying the name of the page at the top of it.
 */
@Component({
  selector: 'app-breadcrumb-nav',
  templateUrl: './breadcrumb-nav.component.html',
  styleUrls: ['./breadcrumb-nav.component.scss']
})
export class BreadcrumbNavComponent implements OnInit, OnDestroy {

  private ngUnsubscribe$ = new Subject();
  menuName: string;

  constructor(private router: Router,
              private menuItemsService: MenuItemsService) {
    this.menuName = this.menuItemsService.getMenuName(this.router.url);
  }

  /**
   * Subscribes to router events. Gets name of the page from the route url after NavigationEnd event.
   */
  ngOnInit(): void {
    this.router.events
      .pipe(takeUntil(this.ngUnsubscribe$))
      .subscribe(event => {
        if (event instanceof NavigationEnd) {
          this.menuName = this.menuItemsService.getMenuName(this.router.url);
        }
      });
  }

  /**
   * Unsubscribes from everything before destroying the component.
   */
  ngOnDestroy(): void {
    this.ngUnsubscribe$.next();
    this.ngUnsubscribe$.complete();
  }

}
