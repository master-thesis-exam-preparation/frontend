import { ChangeDetectorRef, Component, EventEmitter, Input, OnDestroy, Output } from '@angular/core';
import { MediaMatcher } from '@angular/cdk/layout';
import { MenuCategory, MenuItemsService } from '@core/services';
import { LoggedUserInfo } from '@core/logged-user-info';

/**
 * Component for displaying the sidebar menu at the left side of the page.
 */
@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: []
})
export class SidebarComponent implements OnDestroy {

  /**
   * Information about currently logged-in user.
   */
  @Input() loggedUserInfo: LoggedUserInfo;

  /**
   * Outputs a hide event every time the sidebar is hidden.
   */
  @Output() hide: EventEmitter<boolean> = new EventEmitter<boolean>();

  mobileQuery: MediaQueryList;
  menuItems: MenuCategory[];

  private readonly mobileQueryListener: () => void;

  constructor(changeDetectorRef: ChangeDetectorRef,
              media: MediaMatcher,
              private menuItemsService: MenuItemsService) {
    this.mobileQuery = media.matchMedia('(min-width: 1370px)');
    this.mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addEventListener('change', this.mobileQueryListener);

    this.menuItems = this.menuItemsService.getMenu();
  }

  /**
   * Stops listening for changes before destroying the component.
   */
  ngOnDestroy(): void {
    this.mobileQuery.removeEventListener('change', this.mobileQueryListener);
  }

  /**
   * Hides sidebar.
   */
  hideSidebar(): void {
    this.hide.emit(true);
  }
}
