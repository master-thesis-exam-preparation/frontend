import { Component, EventEmitter, Input, OnChanges, Output } from '@angular/core';
import { AuthService, CategoryItems, MenuCategory } from '@core/services';

/**
 * Component for displaying the categories and menu items in the sidebar menu on the left side of the page.
 */
@Component({
  selector: 'app-menu-item',
  templateUrl: './menu-item.component.html',
  styleUrls: ['./menu-item.component.scss']
})
export class MenuItemComponent implements OnChanges {
  isVisible: boolean;

  /**
   * Category of menu items.
   */
  @Input() category: MenuCategory;

  /**
   * List of menu item of the category.
   */
  @Input() menuItem: CategoryItems;

  /**
   * Outputs a hide event every time the sidebar is hidden.
   */
  @Output() hide: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor(private authService: AuthService) {
  }

  /**
   * Checks if the categories and menu items should be visible to user when they change.
   */
  ngOnChanges(): void {
    const roles = this.category?.visibleTo || this.menuItem?.visibleTo;
    this.authService.isVisibleToUser$(roles)
      .subscribe(visible => this.isVisible = visible);
  }

  /**
   * Hides sidebar.
   */
  hideSidebar(): void {
    this.hide.emit(true);
  }

}
