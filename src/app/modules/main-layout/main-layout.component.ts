import { MediaMatcher } from '@angular/cdk/layout';
import { AfterViewInit, ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { AuthService } from '@core/services';
import { Subject } from 'rxjs';
import { LoggedUserInfo } from '@core/logged-user-info';
import { takeUntil } from 'rxjs/operators';

/**
 * Main component of all non-authentication pages.
 */
@Component({
  selector: 'app-main-layout',
  templateUrl: 'main-layout.component.html',
  styleUrls: []
})
export class MainLayoutComponent implements OnInit, OnDestroy {
  mobileQuery: MediaQueryList;
  mobileQueryListener: () => void;

  loggedUserInfo: LoggedUserInfo;

  private ngUnsubscribe$ = new Subject();

  constructor(
    private changeDetectorRef: ChangeDetectorRef,
    private media: MediaMatcher,
    private authService: AuthService
  ) {
    this.mobileQuery = this.media.matchMedia('(min-width: 1370px)');
    this.mobileQueryListener = () => this.changeDetectorRef.detectChanges();
    this.mobileQuery.addEventListener('change', this.mobileQueryListener);
  }

  /**
   * Subscribes to subject with information about currently logged-in user.
   */
  ngOnInit(): void {
    this.authService.loggedUserInfo$
      .pipe(takeUntil(this.ngUnsubscribe$))
      .subscribe(
        value => this.loggedUserInfo = value
      );
  }

  /**
   * Unsubscribes from everything and stops listening for changes before destroying the component.
   */
  ngOnDestroy(): void {
    this.mobileQuery.removeEventListener('change', this.mobileQueryListener);
    this.ngUnsubscribe$.next();
    this.ngUnsubscribe$.complete();
  }
}
