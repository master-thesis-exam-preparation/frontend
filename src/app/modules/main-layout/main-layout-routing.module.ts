import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MainLayoutComponent } from './main-layout.component';
import { RoomModule } from './modules/rooms/room.module';

const mainLayoutRoutes: Routes = [
  {
    path: '',
    component: MainLayoutComponent,
    children: [
      {
        path: 'rooms',
        loadChildren: () => RoomModule
      },
      {
        path: 'terms',
        loadChildren: () => import('./modules/term/term.module').then(m => m.TermModule)
      },
      {
        path: 'courses',
        loadChildren: () => import('./modules/course/course.module').then(m => m.CourseModule)
      },
      {
        path: 'assignments',
        loadChildren: () => import('./modules/assignment/assignment.module').then(m => m.AssignmentModule)
      },
      {
        path: 'account',
        loadChildren: () => import('./modules/account/account.module').then(m => m.AccountModule)
      },
      {
        path: '',
        redirectTo: 'terms',
        pathMatch: 'full'
      },
      {
        path: '**',
        redirectTo: 'rooms'
      }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(mainLayoutRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class MainLayoutRoutingModule {
}
