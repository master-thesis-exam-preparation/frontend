import { NgModule } from '@angular/core';
import { MainLayoutComponent } from './main-layout.component';
import { MainLayoutRoutingModule } from './main-layout-routing.module';
import { SharedModule } from '@shared/shared.module';
import { BreadcrumbNavComponent } from './components/breadcrumb-nav/breadcrumb-nav.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { MenuComponent } from './components/menu/menu.component';
import { MenuItemComponent } from './components/sidebar/menu-item/menu-item.component';


@NgModule({
  declarations: [
    MainLayoutComponent,
    MenuComponent,
    SidebarComponent,
    BreadcrumbNavComponent,
    MenuItemComponent
  ],
  imports: [
    SharedModule,
    MainLayoutRoutingModule
  ]
})
export class MainLayoutModule {
}
