import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ConfirmDialogComponent, ConfirmDialogOptionsData } from '@shared/modules/dialog/confirm-dialog/confirm-dialog.component';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { take } from 'rxjs/operators';

/**
 * Service for displaying specified dialog pop up windows.
 */
@Injectable({
  providedIn: 'root'
})
export class DialogService {
  dialogRef: MatDialogRef<any>;

  constructor(private dialog: MatDialog) {
  }

  /**
   * Opens confirm dialog pop up window.
   * @param options  information for confirm dialog
   */
  openConfirmDialog$(options: ConfirmDialogOptionsData): Observable<boolean> {
    this.dialogRef = this.dialog.open(ConfirmDialogComponent, {
      maxWidth: '600px',
      data: options
    });

    return this.confirmed$();
  }

  private confirmed$(): Observable<boolean> {
    return this.dialogRef
      .afterClosed()
      .pipe(
        take(1)
      );
  }
}
