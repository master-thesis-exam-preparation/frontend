import { SnackBarService } from './snack-bar.service';
import { MenuItemsService } from './menu-items.service';
import { AuthService } from './auth.service';

/**
 * List of services in this directory.
 */
export const services: any[] = [
  SnackBarService,
  MenuItemsService,
  AuthService
];

export * from './snack-bar.service';
export * from './menu-items.service';
export * from './auth.service';
