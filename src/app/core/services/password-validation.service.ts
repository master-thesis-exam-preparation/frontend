import { Injectable } from '@angular/core';
import { AbstractControl, ValidationErrors, ValidatorFn } from '@angular/forms';

/**
 * Service for validation of passwords.
 */
@Injectable({
  providedIn: 'root'
})
export class PasswordValidationService {

  constructor() {
  }

  /**
   * Determines whether password and confirm password has exactly same value.
   * @param formGroup  form group with password and confirm password inputs
   */
  passwordMatchValidator(formGroup: AbstractControl): ValidationErrors | null {
    const confirmPasswordControl: AbstractControl = formGroup.get('confirmPassword');
    const password: string = formGroup.get('password').value; // get password from our password form control
    const confirmPassword: string = confirmPasswordControl.value; // get password from our confirmPassword form control
    // compare is the password math
    if (confirmPassword !== '' && password !== confirmPassword) {
      // if they don't match, set an error in our confirmPassword form control
      confirmPasswordControl.setErrors({NoPasswordMatch: true});
      confirmPasswordControl.markAsTouched();
      return ({NoPasswordMatch: true});
    }
    return null;
  }

  /**
   * Determines whether password is strong enough
   * @param regex  regex to test password
   * @param error  error to set when password is not strong enough.
   */
  regexValidator(regex: RegExp, error: ValidationErrors): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } => {
      if (!control.value) {
        // if control is empty return no error
        return null;
      }

      // test the value of the control against the regexp supplied
      const valid = regex.test(control.value);

      // if true, return no error (no error), else return error passed in the second parameter
      return valid ? null : error;
    };
  }
}
