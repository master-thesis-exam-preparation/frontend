import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '@env';
import { RegisterRequestPayload } from '../../modules/authentication/models/register.request.payload';
import { BehaviorSubject, Observable, throwError } from 'rxjs';
import { LoginResponsePayload } from '../../modules/authentication/models/login.response.payload';
import { LoginRequestPayload } from '../../modules/authentication/models/login.request.payload';
import { catchError, map, shareReplay } from 'rxjs/operators';
import { LoggedUserInfo } from '@core/logged-user-info';
import { JwtHelperService } from '@auth0/angular-jwt';
import { MessagePayload } from '@core/services/models/message.payload';
import { Role } from '@core/role.enum';
import { ActivatedRoute, Router } from '@angular/router';
import { ForgetPasswordRequestPayload } from '../../modules/authentication/models/forget-password.request.payload';
import { ResetPasswordRequestPayload } from '../../modules/authentication/models/reset-password.request.payload';
import { DialogService } from '@core/services/dialog.service';
import { ConfirmDialogOptionsData } from '@shared/modules/dialog/confirm-dialog/confirm-dialog.component';

/**
 * Service for sending request related to authentication.
 */
@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private refreshTokenTimeout;
  private userSubject$: BehaviorSubject<LoggedUserInfo>;
  private accessTokenSubject$: BehaviorSubject<string>;

  constructor(private http: HttpClient,
              private route: ActivatedRoute,
              private router: Router,
              private dialogService: DialogService) {
    this.userSubject$ = new BehaviorSubject<LoggedUserInfo>(null);
    this.accessTokenSubject$ = new BehaviorSubject<string>(null);
  }

  private static getDecodedPayload(accessToken: string): any {
    const helper = new JwtHelperService();
    return helper.decodeToken(accessToken);
  }

  private processLoginResponse(loginResponse: LoginResponsePayload): LoggedUserInfo {
    if (loginResponse.accessToken) {
      this.accessTokenSubject$.next(loginResponse.accessToken);
      const payload = AuthService.getDecodedPayload(loginResponse.accessToken);
      if (payload) {
        const userInfo: LoggedUserInfo = payload.loggedUserInfo;
        this.userSubject$.next(userInfo);
        this.startRefreshTokenTimer(payload.exp);
        return userInfo;
      }
    }
    throw new Error('Nebyl vrácen validní token');
  }

  private processLogoutResponse(): void {
    this.stopRefreshTokenTimer();
    this.accessTokenSubject$.next(null);
    this.userSubject$.next(null);
    this.router.navigate(['']);
  }

  /**
   * Gets value of stored JWT access token.
   */
  get loggedUserAccessToken(): string {
    return this.accessTokenSubject$.value;
  }

  /**
   * Gets value of stored currently logged-in user info.
   */
  get loggedUserInfo$(): Observable<LoggedUserInfo> {
    return this.userSubject$.asObservable();
  }

  /**
   * Sends register new user request.
   * @param registerModel  information necessary to register user to application (e-mail address, password, name and degrees)
   */
  register$(registerModel: RegisterRequestPayload): Observable<string> {
    return this.http.post<MessagePayload>(`${environment.backendUrl}/auth/register`, registerModel)
      .pipe(map(value => {
          return value.message;
        })
      );
  }

  /**
   * Sends log-in user request.
   * @param loginModel  information necessary to log-in user to application (e-mail address, password and remember me option value)
   */
  login$(loginModel: LoginRequestPayload): Observable<LoggedUserInfo> {
    return this.http.post<LoginResponsePayload>(`${environment.backendUrl}/auth/login`, loginModel, {withCredentials: true})
      .pipe(
        map(loginResponse => {
          return this.processLoginResponse(loginResponse);
        }),
        shareReplay(1)
      );
  }

  /**
   * Sends log-out user request.
   */
  logout$(): Observable<string> {
    this.processLogoutResponse();
    const returnUrl = this.route.snapshot.queryParamMap.get('returnUrl') || '/';
    this.router.navigate([returnUrl]);
    return this.http.get<MessagePayload>(`${environment.backendUrl}/auth/logout`, {withCredentials: true})
      .pipe(map(value => {
          return value.message;
        })
      );
  }

  /**
   * Sends request to e-mail message to reset password.
   * @param forgetPasswordModel  email address to which the reset token should be sent
   */
  forgetPassword$(forgetPasswordModel: ForgetPasswordRequestPayload): Observable<string> {
    return this.http.post<MessagePayload>(`${environment.backendUrl}/auth/forget-password`, forgetPasswordModel)
      .pipe(map(value => {
          return value.message;
        })
      );
  }

  /**
   * Sends request to set new user password.
   * @param resetPasswordModel  information necessary to reset user password (token for validation and new password value)
   */
  resetPassword$(resetPasswordModel: ResetPasswordRequestPayload): Observable<string> {
    return this.http.post<MessagePayload>(`${environment.backendUrl}/auth/reset-password`, resetPasswordModel)
      .pipe(map(value => {
          return value.message;
        })
      );
  }

  /**
   * Sends request for access token refresh via refresh token cookie.
   */
  refreshToken$(): Observable<LoggedUserInfo> {
    return this.http.get<LoginResponsePayload>(`${environment.backendUrl}/auth/refresh-token`, {withCredentials: true})
      .pipe(
        map(loginResponse => {
          return this.processLoginResponse(loginResponse);
        }),
        catchError(err => {
          this.processLogoutResponse();
          const options: ConfirmDialogOptionsData = {
            title: 'Automatické odhlášení',
            message: `Byl(a) jste automaticky odhlášen(a) z důvodu neaktivity`,
            confirmText: 'OK',
            confirmColor: 'accent'
          };
          this.dialogService.openConfirmDialog$(options);
          return throwError(err);
        }),
        shareReplay(1)
      );
  }

  /**
   * Sends request to log-in user via refresh token cookie.
   */
  logMeIn$(): Observable<LoggedUserInfo> {
    return this.http.get<LoginResponsePayload>(`${environment.backendUrl}/auth/log-me-in`, {withCredentials: true})
      .pipe(
        map(loginResponse => {
          if (loginResponse) {
            return this.processLoginResponse(loginResponse);
          }
        }),
        catchError(err => { // just in case of server error
          this.processLogoutResponse();
          return throwError(err);
        }),
        shareReplay()
      );
  }

  /**
   * Determines whether current user is logged-in.
   */
  isUserLoggedIn$(): Observable<boolean> {
    return new Observable<boolean>(obs => {
      this.userSubject$.subscribe(data => obs.next(!!data));
    });
  }

  /**
   * Determines whether currently logged-in user has role EMPLOYEE.
   */
  isUserEmployee$(): Observable<boolean> {
    return new Observable<boolean>(obs => {
      this.userSubject$.subscribe(data => {
        if (data?.roles.includes(Role.EMPLOYEE)) {
          obs.next(true);
        } else {
          obs.next(false);
        }
      });
    });
  }

  /**
   * Determines whether currently logged-in user has role ADMIN.
   */
  isUserAdmin$(): Observable<boolean> {
    return new Observable<boolean>(obs => {
      this.userSubject$.subscribe(data => {
        if (data?.roles.includes(Role.ADMIN)) {
          obs.next(true);
        } else {
          obs.next(false);
        }
      });
    });
  }

  /**
   * Determines whether is something visible to currently logged-in user based on his role.
   * @param visibleToRoles  list of roles to which should be something visible
   */
  isVisibleToUser$(visibleToRoles: Role[]): Observable<boolean> {
    return new Observable<boolean>(obs => {
      if (!visibleToRoles || visibleToRoles.length < 1) { // resource IS accessible to everyone
        return obs.next(true);
      }

      this.userSubject$
        // .pipe(take(1))
        .subscribe(data => {
          if (data?.roles) { // user IS logged in and resource IS NOT accessible to everyone
            return obs.next(data.roles.some(role => visibleToRoles.includes(role)));
          } else { // user IS NOT logged in and resource IS NOT accessible to everyone
            return obs.next(false);
          }
        });
    });
  }

  private startRefreshTokenTimer(expTime: number): void {
    // set a timeout to refresh the token a minute before it expires
    const expires = new Date(expTime * 1000);
    const timeout = expires.getTime() - Date.now() - (60 * 1000);
    this.refreshTokenTimeout = setTimeout(() => this.refreshToken$().subscribe(), timeout);
  }

  private stopRefreshTokenTimer(): void {
    clearTimeout(this.refreshTokenTimeout);
  }
}
