import { MatStepperIntl } from '@angular/material/stepper';
import { Injectable } from '@angular/core';

/**
 * Service for translating stepper text to czech.
 */
@Injectable({
  providedIn: 'root'
})
export class CustomMatStepperIntl extends MatStepperIntl {
  constructor() {
    super();

    this.getAndInitTranslations();
  }

  /**
   * Sets translations.
   */
  getAndInitTranslations(): void {
    this.optionalLabel = 'Volitelné';
    this.changes.next();
  }
}
