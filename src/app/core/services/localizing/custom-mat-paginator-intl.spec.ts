import { TestBed } from '@angular/core/testing';

import { CustomMatPaginatorIntl } from '@core/services/localizing/custom-mat-paginator-intl';
import { SharedModule } from '@shared/shared.module';

describe('CustomMatPaginatorIntl', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [SharedModule]
  }));

  it('should be created', () => {
    const intl: CustomMatPaginatorIntl = TestBed.inject(CustomMatPaginatorIntl);
    expect(intl).toBeTruthy();
  });
});
