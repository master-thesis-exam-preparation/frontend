import { TestBed } from '@angular/core/testing';
import { SharedModule } from '@shared/shared.module';
import { CustomMatStepperIntl } from '@core/services/localizing/custom-mat-stepper-intl';

describe('CustomMatStepperIntl', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [SharedModule]
  }));

  it('should be created', () => {
    const intl: CustomMatStepperIntl = TestBed.inject(CustomMatStepperIntl);
    expect(intl).toBeTruthy();
  });
});
