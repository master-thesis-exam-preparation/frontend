import { MatPaginatorIntl } from '@angular/material/paginator';
import { Injectable } from '@angular/core';

/**
 * Service for translating paginator text in tables to czech.
 */
@Injectable({
  providedIn: 'root'
})
export class CustomMatPaginatorIntl extends MatPaginatorIntl {
  constructor() {
    super();

    this.getAndInitTranslations();
  }

  /**
   * Sets translations.
   */
  getAndInitTranslations(): void {
    this.firstPageLabel = 'První strana';
    this.lastPageLabel = 'Poslední strana';
    this.itemsPerPageLabel = 'Záznamů na stranu';
    this.nextPageLabel = 'Další strana';
    this.previousPageLabel = 'Předchozí strana';
    this.changes.next();
  }

  /**
   * Sets translation for range part of the paginator.
   * @param page  page number
   * @param pageSize  page size
   * @param length  length
   */
  getRangeLabel = (page: number, pageSize: number, length: number) => {
    if (length === 0 || pageSize === 0) {
      return `0 z ${length}`;
    }
    length = Math.max(length, 0);
    const startIndex = page * pageSize;
    const endIndex = startIndex < length ? Math.min(startIndex + pageSize, length) : startIndex + pageSize;
    return `${startIndex + 1} - ${endIndex} z ${length}`;
  }
}
