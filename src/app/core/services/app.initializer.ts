import { AuthService } from './auth.service';
import { Observable } from 'rxjs';
import { LoggedUserInfo } from '@core/logged-user-info';
import { APP_INITIALIZER, FactoryProvider } from '@angular/core';

/**
 * Tries to log-in user after each page refresh or on application startup.
 * @param authService  authentication service
 */
export function appInitializer(authService: AuthService): () => Observable<LoggedUserInfo> {
    // attempt to refresh token on app start up to auto authenticate
    return () => authService.logMeIn$();
}

/**
 * Provider with initializer for the parent module.
 */
export const appInitializerProvider: FactoryProvider = {
    provide: APP_INITIALIZER,
    useFactory: appInitializer,
    deps: [AuthService],
    multi: true
};
