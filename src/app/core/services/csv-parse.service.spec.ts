import { TestBed } from '@angular/core/testing';

import { CsvParseService } from './csv-parse.service';
import { SharedModule } from '@shared/shared.module';

describe('CsvParseService', () => {
  let service: CsvParseService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [SharedModule]
    });
    service = TestBed.inject(CsvParseService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
