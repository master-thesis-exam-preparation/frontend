import { TestBed } from '@angular/core/testing';

import { JwtTokenInterceptor } from './jwt-token.interceptor';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { SharedModule } from '@shared/shared.module';

describe('JwtTokenInterceptor', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule, RouterTestingModule, SharedModule],
    providers: [
      JwtTokenInterceptor,
      {
        provide: ActivatedRoute,
        useValue: {
          snapshot: {
            paramMap: {
              get(): number {
                return 1;
              }
            }
          }
        }
      }
    ]
  }));

  it('should be created', () => {
    const interceptor: JwtTokenInterceptor = TestBed.inject(JwtTokenInterceptor);
    expect(interceptor).toBeTruthy();
  });
});
