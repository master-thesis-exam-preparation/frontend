import { Injectable } from '@angular/core';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { SnackBarService } from '@core/services';
import { catchError, tap } from 'rxjs/operators';
import { ErrorPayload } from '@core/services/models/error.payload';

/**
 * Catches all HTTP responses with other than 200 OK and displays their error message in snack bar.
 */
@Injectable()
export class ErrorInterceptor implements HttpInterceptor {

  constructor(private snackBarService: SnackBarService) {
  }

  /**
   * Intercepts responses with other than 200 OK and displays their error message in snack bar.
   * @param request  http request
   * @param next  call next request in queue
   */
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(request)
      .pipe(
        tap((response: HttpResponse<any>) => {
          if (response.status === 302) {
            this.snackBarService.openInfoSnackBar(response.body.message);
          }
        }),
        catchError(err => {
          const error = (err?.error?.message) || err.statusText;
          if (err.status === 0) {
            this.snackBarService.openDangerSnackBar('Aplikační server není dostupný.\nVyčkejte, nebo kontaktujte administrátora.');
          } else {
            const errorPayload: ErrorPayload = err.error;
            this.snackBarService.openDangerSnackBar(errorPayload.message);
          }
          return throwError(error);
        })
      );
  }
}
