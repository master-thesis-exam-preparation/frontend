import { Injectable } from '@angular/core';
import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

/**
 * Catches all responses that contains error message and their payload is Binary large object.
 */
@Injectable()
export class BlobErrorInterceptor implements HttpInterceptor {

  /**
   * Intercepts responses that contains error message and their payload is Binary large object. Then reads this error message.
   * @param request  http request
   * @param next  call next request in queue
   */
  public intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(request)
      .pipe(
        catchError(err => {
          if (err instanceof HttpErrorResponse && err.error instanceof Blob && err.error.type === 'application/json') {
            // https://github.com/angular/angular/issues/19888
            // When request of type Blob, the error is also in Blob instead of object of the json data
            return new Promise<any>((resolve, reject) => {
              const reader = new FileReader();
              reader.onload = (e: Event) => {
                try {
                  const errmsg = JSON.parse((e.target as any).result);
                  reject(new HttpErrorResponse({
                    error: errmsg,
                    headers: err.headers,
                    status: err.status,
                    statusText: err.statusText,
                    url: err.url
                  }));
                } catch (e) {
                  reject(err);
                }
              };
              reader.onerror = (e) => {
                reject(err);
              };
              reader.readAsText(err.error);
            });
          }
          return throwError(err);
        })
      );
  }
}
