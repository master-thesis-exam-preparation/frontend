import { TestBed } from '@angular/core/testing';

import { BlobErrorInterceptor } from './blob-error.interceptor';
import { SharedModule } from '@shared/shared.module';

describe('BlobErrorInterceptor', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [SharedModule],
    providers: [
      BlobErrorInterceptor
    ]
  }));

  it('should be created', () => {
    const interceptor: BlobErrorInterceptor = TestBed.inject(BlobErrorInterceptor);
    expect(interceptor).toBeTruthy();
  });
});
