import { Injectable } from '@angular/core';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '@env';
import { AuthService } from '@core/services';

/**
 * Http interceptor that catches every request and adds JWT token to its header.
 */
@Injectable()
export class JwtTokenInterceptor implements HttpInterceptor {
  constructor(private authService: AuthService) {
  }

  /**
   * Intercepts request and add JWT token from subject to its header
   * @param request  http request
   * @param next  call next request in queue
   */
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const excludedUrl = [
      '/auth/*'
    ];

    if (excludedUrl.some(regex => new RegExp('^' + regex).test(new URL(request.url).pathname))) {
      return next.handle(request);
    }

    const jwtToken = this.authService.loggedUserAccessToken;
    const isApiUrl = request.url.startsWith(environment.backendUrl);
    if (jwtToken && isApiUrl) {
      request = request.clone({
        setHeaders: {
          Authorization: 'Bearer ' + jwtToken
        }
      });
    }

    return next.handle(request);
  }
}
