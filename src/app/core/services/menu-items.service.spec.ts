import { TestBed } from '@angular/core/testing';

import { MenuItemsService } from '@core/services/menu-items.service';
import { SharedModule } from '@shared/shared.module';

describe('MenuItemsService', () => {
  let service: MenuItemsService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [SharedModule]
    });
    service = TestBed.inject(MenuItemsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
