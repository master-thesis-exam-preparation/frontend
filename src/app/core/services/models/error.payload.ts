import { MessagePayload } from '@core/services/models/message.payload';

/**
 * Used to store information about the occurred error.
 */
export interface ErrorPayload extends MessagePayload {
  timestamp: string;
  statusCode: number;
  status: string;
}
