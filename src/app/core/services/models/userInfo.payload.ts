/**
 * Used to store information about the e-mail address of the logged-in user and his name with degrees.
 */
export interface UserInfoPayload {
  email: string;
  nameWithDegrees: string;
}
