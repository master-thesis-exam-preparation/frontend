/**
 * Used to store message. Usually message is text response for user request.
 */
export interface MessagePayload {
  message: string;
}
