import { Injectable } from '@angular/core';
import { CanDeactivate, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { DialogService } from '@core/services/dialog.service';
import { tap } from 'rxjs/operators';
import { ConfirmDialogOptionsData } from '@shared/modules/dialog/confirm-dialog/confirm-dialog.component';

/**
 * Router guard that determines if user can leave page with unsaved changes.
 */
@Injectable({
  providedIn: 'root'
})
export class UnsavedChangesGuard implements CanDeactivate<unknown> {

  canBeDeactivated = true;

  constructor(private dialogService: DialogService) {
  }

  /**
   * If on page are some unsaved changes confirm dialog will appear. The user leaves the page only if he confirms that he
   * wants to leave it without saving.
   */
  canDeactivate(): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

    if (!this.canBeDeactivated) {
      const options: ConfirmDialogOptionsData = {
        title: 'Neuložené změny',
        message: `Opravdu chcete opustit tuto stránku"? Veškerá neuložená data budou ztracena.`,
        cancelText: 'ZRUŠIT',
        cancelColor: 'primary',
        confirmText: 'OPUSTIT',
        confirmColor: 'accent'
      };
      return this.dialogService.openConfirmDialog$(options)
        .pipe(
          tap(x => this.canBeDeactivated = !!x) // set value back to true if component was deactivated
        );
    } else {
      return true;
    }
  }
}
