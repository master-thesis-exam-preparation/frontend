import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, UrlTree } from '@angular/router';
import { combineLatest, Observable } from 'rxjs';
import { CourseService } from '../../../modules/main-layout/modules/course/services/course.service';
import { SnackBarService } from '@core/services';
import { take } from 'rxjs/operators';
import { RoomService } from '../../../modules/main-layout/modules/rooms/services/room.service';

/**
 * Router guard that determines if the new term can be created.
 */
@Injectable({
  providedIn: 'root'
})
export class TermDataGuard implements CanActivate {

  constructor(private courseService: CourseService,
              private roomService: RoomService,
              private snackBarService: SnackBarService) {
  }

  /**
   * Determines if the new term can be created. If there are no rooms or courses, access to new term form will be denied.
   * @param route  route to check
   * @param state  state of the router
   */
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return new Observable<boolean>(obs => {
      combineLatest([
        this.courseService.getCurrentAcademicYearCoursesInfo$(),
        this.roomService.getAllRoomsInfo$()
      ])
        .pipe(
          take(1)
        )
        .subscribe({
          next: res => {
            if (res[0].length === 0) {
              this.snackBarService.openDangerSnackBar('Nelze naplánovat termín zkoušky — pro tento akademický rok ' +
                'není vytvořen žádný předmět\nKontaktujte správce pro přidání předmětu.');
              return obs.next(false);
            } else if (res[1].length === 0) {
              this.snackBarService.openDangerSnackBar('Nelze naplánovat termín zkoušky — není vytvořena žádná místnost, ' +
                've které by se zkouška mohlo konat.\nVytvořte novou místnost.');
              return obs.next(false);
            } else {
              return obs.next(true);
            }
          },
          error: err => console.log(err)
        });
    });
  }
}
