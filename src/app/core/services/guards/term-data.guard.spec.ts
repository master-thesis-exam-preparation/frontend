import { TestBed } from '@angular/core/testing';

import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { TermDataGuard } from '@core/services/guards/term-data.guard';
import { SharedModule } from '@shared/shared.module';

describe('TermDataGuard', () => {
  let guard: TermDataGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule, HttpClientTestingModule, SharedModule]
    });
    guard = TestBed.inject(TermDataGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
