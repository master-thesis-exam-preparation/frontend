import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { combineLatest, Observable } from 'rxjs';
import { AuthService } from '@core/services/auth.service';
import { SnackBarService } from '@core/services/snack-bar.service';
import { take } from 'rxjs/operators';

/**
 * Router guard that determines whether user with certain role can access certain route.
 */
@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(private router: Router,
              private authService: AuthService,
              private snackBarService: SnackBarService) {
  }

  /**
   * Determines whether user with certain role can access route.
   * @param route  route to check
   * @param state  state of the router
   */
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return new Observable<boolean>(obs => {
      combineLatest([
        this.authService.isUserLoggedIn$(),
        this.authService.isVisibleToUser$(route.data.roles)
      ]).pipe(
        take(1)
      ).subscribe({
        next: res => {
          if (res[0]) {
            // check if route is restricted by role
            const hasRoleAccessToRoute = route.data?.roles && res[1] || false;
            if (!hasRoleAccessToRoute) {
              // role not authorised so redirect to home page
              this.router.navigate(['/']);
              this.snackBarService.openDangerSnackBar('K této stránce nemáte povolen přístup');
              return obs.next(false);
            }

            // authorised so return true
            return obs.next(true);
          } else {
            // not logged in so redirect to login page with the return url
            // after sign in user will be redirected back to the last visited page
            this.router.navigate(['/auth/login'], {
              queryParams: {
                returnUrl: state.url
              }
            });
            return obs.next(false);
          }
        }
      });
    });
  }
}
