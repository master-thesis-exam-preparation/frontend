import { Injectable } from '@angular/core';
import { MatSnackBar, MatSnackBarConfig, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';

/**
 * Service for displaying messages in bubbles in right bottom corner of the page.
 */
@Injectable({
  providedIn: 'root'
})
export class SnackBarService {

  private processingMessage = false;
  private messageQueue: [string, MatSnackBarConfig][] = [];

  // TODO Possible global settings
  private duration = 4000;
  private horizontalPosition: MatSnackBarHorizontalPosition = 'right';
  private verticalPosition: MatSnackBarVerticalPosition = 'bottom';

  private successConfig: MatSnackBarConfig = {
    duration: this.duration,
    horizontalPosition: this.horizontalPosition,
    verticalPosition: this.verticalPosition,
    panelClass: ['bg-success', 'text-white', 'white-space-wrap']
  };

  private dangerConfig: MatSnackBarConfig = {
    duration: 4000,
    horizontalPosition: this.horizontalPosition,
    verticalPosition: this.verticalPosition,
    panelClass: ['bg-danger', 'text-white', 'white-space-wrap']
  };

  private infoConfig: MatSnackBarConfig = {
    duration: this.duration,
    horizontalPosition: this.horizontalPosition,
    verticalPosition: this.verticalPosition,
    panelClass: ['bg-info', 'text-white', 'white-space-wrap']
  };

  constructor(private snackBar: MatSnackBar) {
  }

  private displaySnackbar(): void {
    const nextMessage = this.getNextMessage();

    if (!nextMessage) {
      this.processingMessage = false;
      return;
    }

    this.processingMessage = true;

    this.snackBar.open(nextMessage[0], undefined, nextMessage[1])
      .afterDismissed()
      .subscribe(() => {
        this.displaySnackbar();
      });
  }

  private getNextMessage(): [string, MatSnackBarConfig] | undefined {
    return this.messageQueue.length ? this.messageQueue.shift() : undefined;
  }

  private addMessageToQueue(message: string, config: MatSnackBarConfig): void {
    this.messageQueue.push([message, config]);
    if (!this.processingMessage) {
      this.displaySnackbar();
    }
  }

  /**
   * Displays bubble with green color for success messages.
   * @param message  message to be displayed.
   */
  openSuccessSnackBar(message: string): void {
    this.addMessageToQueue(message, this.successConfig);
  }

  /**
   * Displays bubble with red color for error messages.
   * @param message  message to be displayed.
   */
  openDangerSnackBar(message: string): void {
    this.addMessageToQueue(message, this.dangerConfig);
  }

  /**
   * Displays bubble light blue color for information messages.
   * @param message  message to be displayed.
   */
  openInfoSnackBar(message: string): void {
    this.addMessageToQueue(message, this.infoConfig);
  }
}
