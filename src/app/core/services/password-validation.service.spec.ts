import { TestBed } from '@angular/core/testing';

import { PasswordValidationService } from './password-validation.service';
import { SharedModule } from '@shared/shared.module';

describe('PasswordValidationService', () => {
  let service: PasswordValidationService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [SharedModule]
    });
    service = TestBed.inject(PasswordValidationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
