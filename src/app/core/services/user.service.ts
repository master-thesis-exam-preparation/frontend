import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '@env';
import { UserInfoPayload } from '@core/services/models/userInfo.payload';

/**
 * Service for sending request related to users.
 */
@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) {
  }

  /**
   * Sends get list of all employees request.
   */
  getAllEmployeesList$(): Observable<UserInfoPayload[]> {
    return this.http.get<UserInfoPayload[]>(`${environment.backendUrl}/internal/employees`);
  }

}
