import { Injectable } from '@angular/core';
import { Role } from '@core/role.enum';

/**
 * Used to store information about page name and regex of path on which they should be displayed in breadcrumb menu.
 */
export interface BreadcrumbName {
  name: string;
  routeRegex: string;
}

/**
 * Used to store information about menu categories for the left side menu.
 */
export interface MenuCategory {
  categoryName: string;
  visibleTo?: Role[];
  menuItems?: CategoryItems[];
}

/**
 * Used to store information about items of the left side menu categories.
 */
export interface CategoryItems {
  menuName: string;
  routerLink: string;
  icon?: string;
  visibleTo?: Role[];
}

/**
 * List of breadcrumb page names.
 */
const breadcrumbNames: BreadcrumbName[] = [
  {
    name: 'Detail místnosti',
    routeRegex: '^/rooms/\\d$'
  },
  {
    name: 'Úprava místnosti',
    routeRegex: '^/rooms/\\d/edit$'
  },
  {
    name: 'Detail termínu zkoušky',
    routeRegex: '^/terms/\\d$'
  },
  {
    name: 'Úprava termínu zkoušky',
    routeRegex: '^/terms/\\d/edit$'
  },
  {
    name: 'Detail šablony zadání',
    routeRegex: '^/assignments/\\d$'
  },
  {
    name: 'Úprava šablony zadání',
    routeRegex: '^/assignments/\\d/edit$'
  },
];

/**
 * List of the left side menu categories with their items.
 */
const menu: MenuCategory[] = [
  {
    categoryName: 'Termíny zkoušek',
    menuItems: [
      {
        menuName: 'Seznam termínů zkoušek',
        routerLink: '/terms',
        icon: 'event'
      },
      {
        menuName: 'Nový termín zkoušky',
        routerLink: '/terms/new',
        icon: 'edit_calendar',
        visibleTo: [Role.EMPLOYEE, Role.ADMIN]
      }
    ]
  },
  {
    categoryName: 'Místnosti',
    menuItems: [
      {
        menuName: 'Seznam místností',
        routerLink: '/rooms',
        icon: 'meeting_room'
      },
      {
        menuName: 'Nová místnost',
        routerLink: '/rooms/new',
        icon: 'add_box',
        visibleTo: [Role.EMPLOYEE, Role.ADMIN]
      }
    ]
  },
  {
    categoryName: 'Šablony zadání',
    menuItems: [
      {
        menuName: 'Seznam šablon zadání',
        routerLink: '/assignments',
        icon: 'description',
        visibleTo: [Role.EMPLOYEE, Role.ADMIN]
      },
      {
        menuName: 'Nová šablona zadání',
        routerLink: '/assignments/new',
        icon: 'note_add',
        visibleTo: [Role.EMPLOYEE, Role.ADMIN]
      }
    ]
  },
  {
    categoryName: 'Předměty',
    menuItems: [
      {
        menuName: 'Správa předmětů',
        routerLink: '/courses',
        icon: 'list',
        visibleTo: [Role.ADMIN]
      },
      {
        menuName: 'Importování předmětů',
        routerLink: '/courses/new',
        icon: 'post_add',
        visibleTo: [Role.ADMIN]
      }
    ]
  },
  {
    categoryName: 'Správa účtu',
    menuItems: [
      {
        menuName: 'Změnit heslo',
        routerLink: '/account/change-password',
        icon: 'password',
        visibleTo: [Role.STUDENT, Role.EMPLOYEE, Role.ADMIN]
      },
      {
        menuName: 'Změnit uživatelské údaje',
        routerLink: '/account/change-user-details',
        icon: 'person',
        visibleTo: [Role.STUDENT, Role.EMPLOYEE, Role.ADMIN]
      },
      {
        menuName: 'Spravovat uživatelké účty',
        routerLink: '/account/manage-accounts',
        icon: 'manage_accounts',
        visibleTo: [Role.ADMIN]
      }
    ]
  }
];

/**
 * Service for displaying page names and for displaying menu items for specific roles of users.
 */
@Injectable({
  providedIn: 'root'
})
export class MenuItemsService {
  constructor() {
  }

  /**
   * Hide all menu items and categories that are not visible for corresponding roles.
   */
  getMenu(): MenuCategory[] {
    for (const category of menu) {
      if (!category.visibleTo || category.visibleTo.length < 1) {
        // category is visible to everyone
        category.visibleTo = [];
        for (const menuItem of category.menuItems) {
          if (menuItem.visibleTo?.length > 0) {
            // if all menu items have roles, concatenate them and set them to category
            category.visibleTo = [...new Set<Role>([...category.visibleTo, ...menuItem.visibleTo])];
          } else {
            // menu item is visible to everyone, category should be visible to everyone too
            category.visibleTo = [];
            break;
          }
        }
      }
    }
    return menu;
  }

  /**
   * Display page name in breadcrumb for current page.
   * @param url  url of current page
   */
  getMenuName(url: string): string {
    for (const category of menu) {
      for (const menuItem of category.menuItems) {
        if (menuItem.routerLink === url) {
          return menuItem.menuName;
        }
      }
    }

    for (const breadcrumbName of breadcrumbNames) {
      if (url.match(breadcrumbName.routeRegex)) {
        return breadcrumbName.name;
      }
    }
    return '';
  }
}
