import { HTTP_INTERCEPTORS } from '@angular/common/http';

import { ErrorInterceptor } from '@core/services/http-interceptors/error.interceptor';
import { JwtTokenInterceptor } from '@core/services/http-interceptors/jwt-token.interceptor';
import { MatPaginatorIntl } from '@angular/material/paginator';
import { CustomMatPaginatorIntl } from '@core/services/localizing/custom-mat-paginator-intl';
import { MatStepperIntl } from '@angular/material/stepper';
import { CustomMatStepperIntl } from '@core/services/localizing/custom-mat-stepper-intl';
import { BlobErrorInterceptor } from '@core/services/http-interceptors/blob-error.interceptor';

/** Http interceptor providers in outside-in order */
export const providers = [
  {
    provide: HTTP_INTERCEPTORS,
    useClass: JwtTokenInterceptor,
    multi: true
  },
  {
    provide: HTTP_INTERCEPTORS,
    useClass: ErrorInterceptor,
    multi: true
  },
  {
    provide: HTTP_INTERCEPTORS,
    useClass: BlobErrorInterceptor,
    multi: true
  },
  {
    provide: MatPaginatorIntl,
    useClass: CustomMatPaginatorIntl
  },
  {
    provide: MatStepperIntl,
    useClass: CustomMatStepperIntl
  }
];
