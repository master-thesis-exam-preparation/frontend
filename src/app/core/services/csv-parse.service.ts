import { Injectable } from '@angular/core';
import { SnackBarService } from '@core/services/snack-bar.service';
import { Observable, Subscriber } from 'rxjs';
import { AbstractControl, ValidationErrors } from '@angular/forms';

/**
 * Service for parsing uploaded CSV files.
 */
@Injectable({
  providedIn: 'root'
})
export class CsvParseService {

  constructor(private snackBarService: SnackBarService) {
  }

  /**
   * Determines whether a pair of two inputs contains the same value.
   * @param firstColumn  first input
   * @param secondColumn  second input
   */
  static sameNumberValidatorTwo(firstColumn: AbstractControl, secondColumn: AbstractControl): ValidationErrors | null {
    if (firstColumn && secondColumn && firstColumn.value && secondColumn.value
      && firstColumn.value === secondColumn.value) {
      firstColumn.setErrors({SameNumber: true});
      firstColumn.markAsTouched();
      secondColumn.setErrors({SameNumber: true});
      secondColumn.markAsTouched();
      return ({SameNumber: true});
    } else {
      if (firstColumn.hasError('SameNumber')) {
        delete firstColumn.errors.SameNumber;
        firstColumn.updateValueAndValidity();
      } else if (secondColumn.hasError('SameNumber')) {
        delete secondColumn.errors.SameNumber;
        secondColumn.updateValueAndValidity();
      }
      return null;
    }
  }

  /**
   * Determines whether some pair of the input triple contains the same value.
   * @param firstColumn  first input
   * @param secondColumn  second input
   * @param thirdColumn  third input
   */
  static sameNumberValidatorThree(
    firstColumn: AbstractControl,
    secondColumn: AbstractControl,
    thirdColumn: AbstractControl): ValidationErrors | null {
    if (firstColumn && secondColumn && thirdColumn && firstColumn.value && secondColumn.value && thirdColumn.value) {
      if (firstColumn.value === secondColumn.value || firstColumn.value === thirdColumn.value || secondColumn.value === thirdColumn.value) {
        firstColumn.setErrors({SameNumber: true});
        firstColumn.markAsTouched();
        secondColumn.setErrors({SameNumber: true});
        secondColumn.markAsTouched();
        thirdColumn.setErrors({SameNumber: true});
        thirdColumn.markAsTouched();
        return ({SameNumber: true});
      }
    }
    if (firstColumn.hasError('SameNumber')) {
      delete firstColumn.errors.SameNumber;
      firstColumn.updateValueAndValidity();
    } else if (secondColumn.hasError('SameNumber')) {
      delete secondColumn.errors.SameNumber;
      secondColumn.updateValueAndValidity();
    } else if (thirdColumn.hasError('SameNumber')) {
      delete thirdColumn.errors.SameNumber;
      thirdColumn.updateValueAndValidity();
    }
    return null;
  }

  /**
   * Determines whether all object attributes are set.
   * @param obj  object to check
   */
  static isAllAttributesSet(obj: any): boolean {
    return !Object.values(obj)
      .some(val => !val);
  }

  /**
   * Splits input text file to lines.
   * @param csvText  input CSV file
   */
  static splitTextToLines(csvText: string): string[] {
    if (csvText) {
      return csvText.split(/\r?\n/)
        .map(line => {
          if (line.charAt(0) === '"' && line.charAt(line.length - 1) === '"') {
            line = line.substr(1, (line.length - 2));
          }
          return line.trim();
        })
        .filter(Boolean); // remove empty lines
    } else {
      return [];
    }
  }

  /**
   * Reads CSV file content with provided encoding.
   * @param csvFile  uploaded CSV file
   * @param encoding  selected encoding value
   */
  readFile(csvFile: File, encoding: string): Observable<string> {
    return new Observable((observer: Subscriber<string>): void => {
      let reader: FileReader = new FileReader();
      reader.onload = () => {
        observer.next(reader.result as string);
        observer.complete();
      };
      reader.onerror = (error) => {
        this.snackBarService.openDangerSnackBar('Chyba při čtení souboru: ' + error);
        observer.error(error);
      };
      reader.onloadend = () => {
        reader = null;
        observer.complete();
      };
      reader.readAsText(csvFile, encoding);
    });
  }

  /**
   * Catches TAB key press in coursesCsv text area.
   * @param event  TAB key press event
   * @param control  text area
   */
  onTab(event: any, control: AbstractControl): void {
    event.preventDefault();
    const element = event.target;
    const start = element.selectionStart;
    const end = element.selectionEnd;
    control.setValue(element.value.substring(0, start) + '\t' + element.value.substring(end));
    element.selectionStart = element.selectionEnd = start + 1;
    control.updateValueAndValidity();
  }
}
