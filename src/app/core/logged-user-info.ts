import { Role } from '@core/role.enum';

/**
 * Used to store information about currently logged-in user.
 */
export interface LoggedUserInfo {
  userId: number;
  nameWithDegrees: string;
  email: string;
  roles: Array<Role>;
  locked: boolean;
}
