import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { providers } from './services/providers';


@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ],
  providers: [
    providers
  ]
})
export class CoreModule {
}
