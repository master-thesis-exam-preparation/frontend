import { Component } from '@angular/core';

/**
 * Main application component. All other components are inserted into this one.
 */
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {}
