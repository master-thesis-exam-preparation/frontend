import { ClickStopPropagationDirective } from '@shared/directives/click-stop-propagation.directive';

describe('ClickStopPropagationDirective', () => {
  it('should create an instance', () => {
    const directive = new ClickStopPropagationDirective();
    expect(directive).toBeTruthy();
  });
});
