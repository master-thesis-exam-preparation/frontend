import { Directive, HostListener } from '@angular/core';

/**
 * Directive which captures all click events and stops their propagation.
 */
@Directive({
  selector: '[appClickStopPropagation]'
})
export class ClickStopPropagationDirective {

  constructor() {
  }

  /**
   * Listens for mouse click event.
   * @param event  mouse click event
   */
  @HostListener('click', ['$event'])
  public onClick(event: any): void {
    event.stopPropagation();
  }

}
