import { ClickStopPropagationDirective } from '@shared/directives/click-stop-propagation.directive';

/**
 * List of directives in this directory.
 */
export const directives: any[] = [
  ClickStopPropagationDirective
];

export * from '@shared/directives/click-stop-propagation.directive';
