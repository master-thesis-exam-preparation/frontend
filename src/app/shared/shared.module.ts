import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from './modules/material/material.module';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { FlexLayoutModule } from '@angular/flex-layout';
import { LoadSpinnerModule } from './modules/load-spinner/load-spinner.module';
import * as fromComponents from './components';
import * as fromDirectives from './directives';
import { AngularFittextModule } from 'angular-fittext';
import { RoomPlanComponent, RoomPlanLegendComponent } from '@shared/components';
import { DialogModule } from '@shared/modules/dialog/dialog.module';
import { FilesizePipe } from './pipes/filesize.pipe';
import { GenerateAssignmentComponent } from '@shared/components/generate-assignment/generate-assignment.component';
import { DownloadOptionsDialogComponent } from './components/generate-assignment/download-options-dialog/download-options-dialog.component';

@NgModule({
  declarations: [
    ...fromComponents.components,
    ...fromDirectives.directives,
    RoomPlanComponent,
    RoomPlanLegendComponent,
    FilesizePipe,
    GenerateAssignmentComponent,
    DownloadOptionsDialogComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    LoadSpinnerModule,
    ReactiveFormsModule,
    HttpClientModule,
    FlexLayoutModule,
    AngularFittextModule,
    DialogModule
  ],
  exports: [
    CommonModule,
    MaterialModule,
    LoadSpinnerModule,
    ReactiveFormsModule,
    HttpClientModule,
    FlexLayoutModule,
    AngularFittextModule,
    DialogModule,
    ...fromComponents.components,
    ...fromDirectives.directives,
    FilesizePipe,
    GenerateAssignmentComponent
  ]
})
export class SharedModule {
}
