import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GenerateAssignmentComponent } from './generate-assignment.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { SharedModule } from '@shared/shared.module';

describe('GenerateAssignmentComponent', () => {
  let component: GenerateAssignmentComponent;
  let fixture: ComponentFixture<GenerateAssignmentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [GenerateAssignmentComponent],
      imports: [HttpClientTestingModule, SharedModule]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GenerateAssignmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
