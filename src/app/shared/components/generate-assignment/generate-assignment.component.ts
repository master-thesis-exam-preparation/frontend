import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { SnackBarService } from '@core/services';
import { AssignmentService } from '../../../modules/main-layout/modules/assignment/services/assignment.service';
import { TermService } from '../../../modules/main-layout/modules/term/services/term.service';
import { saveAs } from 'file-saver';
import { take } from 'rxjs/operators';
import { MatDialog } from '@angular/material/dialog';
import { DownloadOptionsDialogComponent } from '@shared/components/generate-assignment/download-options-dialog/download-options-dialog.component';
import { DownloadOptions } from '@shared/components/generate-assignment/models/download-options.payload';
import { DialogService } from '@core/services/dialog.service';
import { ConfirmDialogOptionsData } from '@shared/modules/dialog/confirm-dialog/confirm-dialog.component';

/**
 * Component for displaying the button do generate assignments or download generated assignments.
 */
@Component({
  selector: 'app-generate-assignment',
  templateUrl: './generate-assignment.component.html',
  styleUrls: ['./generate-assignment.component.scss']
})
export class GenerateAssignmentComponent {

  /**
   * Tooltip text to display on generate button hover.
   */
  @Input() generateTooltipText: string;

  /**
   * Tooltip text to display on download button hover.
   */
  @Input() downloadTooltipText: string;

  /**
   * Determines whether button should have text or just icon.
   */
  @Input() onlyIcon = false;

  /**
   * Determines whether button should have icon before text or not.
   */
  @Input() showIcon = false;

  /**
   * Determines whether download button should be displayed.
   */
  @Input() showDownload = false;

  /**
   * Determines whether generating in progress text should be displayed.
   */
  @Input() generatingInProgress = false;

  /**
   * Determines whether button to generate assignment should be disabled.
   */
  @Input() disableGenerateButton = false;

  /**
   * Unique ID of the term for which should be assignments generated/downloaded.
   */
  @Input() termId: number = null;

  /**
   * Unique ID of the term run for which should be assignments generated/downloaded.
   */
  @Input() termRunId: number = null;

  /**
   * Room abbreviation of the room for which should be assignments generated/downloaded.
   */
  @Input() roomAbbreviation: string = null;

  /**
   * Unique ID of the test schedule for which should be assignment generated/downloaded.
   */
  @Input() testScheduleId: number = null;

  /**
   * Outputs an event everytime new generating is started.
   */
  @Output() generatingStarted: EventEmitter<boolean> = new EventEmitter<boolean>();

  /**
   * Outputs an event everytime running generating is canceled.
   */
  @Output() generatingCanceled: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor(private snackBarService: SnackBarService,
              private assignmentService: AssignmentService,
              private termService: TermService,
              private dialog: MatDialog,
              private dialogService: DialogService) {
  }


  /**
   * Starts generating specified test schedule assignments.
   */
  generateAssignment(): void {
    this.assignmentService.generateAssignment$(this.termId, this.termRunId, this.roomAbbreviation, this.testScheduleId)
      .subscribe({
        next: message => {
          this.generatingStarted.emit(true);
          this.snackBarService.openInfoSnackBar(message);
        }
      });
  }

  /**
   * Shows confirmation dialog before canceling generating the assignments.
   */
  cancelAssignmentGenerating(): void {
    const options: ConfirmDialogOptionsData = {
      title: 'Zrušit generování?',
      message: `Opravdu chcete zrušit generování zbývajících zadání pro tento termín?`,
      cancelText: 'NE',
      cancelColor: 'primary',
      confirmText: 'ANO',
      confirmColor: 'warn'
    };
    this.dialogService.openConfirmDialog$(options)
      .subscribe(confirmed => {
        if (confirmed) {
          this.assignmentService.cancelAssignmentGenerating$(this.termId)
            .subscribe({
              next: message => {
                this.generatingCanceled.emit(true);
                this.snackBarService.openInfoSnackBar(message);
              }
            });
        }
      });
  }

  /**
   * Displays dialog pop up window to specify download options.
   */
  downloadPdf(): void {
    if (!this.testScheduleId) {
      this.dialog.open(DownloadOptionsDialogComponent, {
        maxWidth: '600px'
      }).afterClosed()
        .pipe(take(1))
        .subscribe({
          next: (downloadOptions: DownloadOptions) => {
            if (!!downloadOptions) { // not empty or undefined
              this.downloadPdfFiles(downloadOptions.pack, downloadOptions.doubleSided);
            }
          }
        });
    } else {
      this.downloadPdfFiles(false, false);
    }
  }

  private downloadPdfFiles(pack: boolean, doubleSided: boolean): void {
    this.termService.downloadPdfFiles$(this.termId, this.termRunId, this.roomAbbreviation, this.testScheduleId, pack, doubleSided)
      .subscribe({
        next: response => {
          saveAs(response[0], response[1]);
        }
      });
  }
}
