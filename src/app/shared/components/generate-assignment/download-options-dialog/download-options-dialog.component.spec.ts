import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DownloadOptionsDialogComponent } from './download-options-dialog.component';
import { FormBuilder } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { SharedModule } from '@shared/shared.module';

describe('DownloadOptionsDialogComponent', () => {
  let component: DownloadOptionsDialogComponent;
  let fixture: ComponentFixture<DownloadOptionsDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [DownloadOptionsDialogComponent],
      imports: [SharedModule],
      providers: [
        FormBuilder,
        {
          provide: MatDialogRef,
          useValue: {
            pack: true,
            doubleSided: true
          }
        }
      ]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DownloadOptionsDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
