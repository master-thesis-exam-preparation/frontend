import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { DownloadOptions } from '@shared/components/generate-assignment/models/download-options.payload';

/**
 * Component for displaying the popup window with form to specify download options.
 */
@Component({
  selector: 'app-download-options-dialog',
  templateUrl: './download-options-dialog.component.html',
  styleUrls: ['./download-options-dialog.component.scss']
})
export class DownloadOptionsDialogComponent implements OnInit {

  downloadOptionsForm: FormGroup;

  constructor(private fb: FormBuilder,
              private dialogRef: MatDialogRef<DownloadOptionsDialogComponent>) {
  }

  /**
   * Gets value of the pack checkbox in the form.
   */
  get pack(): AbstractControl {
    return this.downloadOptionsForm.get('pack');
  }

  /**
   * Gets value of the pack doubleSided in the form.
   */
  get doubleSided(): AbstractControl {
    return this.downloadOptionsForm.get('doubleSided');
  }

  /**
   * Sets validators to the form checkboxes.
   */
  ngOnInit(): void {
    this.downloadOptionsForm = this.fb.group({
      pack: false,
      doubleSided: false
    });

    this.pack.valueChanges
      .subscribe({
        next: value => {
          if (!value) {
            this.doubleSided.setValue(false);
          }
        }
      });

    this.doubleSided.valueChanges
      .subscribe({
        next: value => {
          if (value) {
            this.pack.setValue(true);
          }
        }
      });
  }

  /**
   * Submit the form and send the download assignments request.
   */
  onSubmit(): void {
    if (this.downloadOptionsForm.valid) {
      const formValues = this.downloadOptionsForm.value;
      const options: DownloadOptions = {
        pack: formValues.pack,
        doubleSided: formValues.doubleSided
      };
      this.dialogRef.close(options);
    }
  }
}
