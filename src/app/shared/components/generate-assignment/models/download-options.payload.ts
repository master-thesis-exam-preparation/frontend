/**
 * Used to store selected assignment download options.
 */
export interface DownloadOptions {
  pack: boolean;
  doubleSided: boolean;
}
