import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SharedModule } from '@shared/shared.module';
import { RoomPlanLegendComponent } from '@shared/components';

describe('RoomPlanLegendComponent', () => {
  let component: RoomPlanLegendComponent;
  let fixture: ComponentFixture<RoomPlanLegendComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [RoomPlanLegendComponent],
      imports: [SharedModule]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RoomPlanLegendComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
