import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { CellType } from '../../../modules/main-layout/modules/rooms/cell-type.enum';

/**
 * Component for displaying the legend of the room plan.
 */
@Component({
  selector: 'app-room-plan-legend',
  templateUrl: './room-plan-legend.component.html',
  styleUrls: ['./room-plan-legend.component.scss']
})
export class RoomPlanLegendComponent implements OnInit {

  /**
   * Display 'blocked seat' legend item.
   */
  @Input() withBlocked: boolean;

  /**
   * Display 'selected seat' legend item.
   */
  @Input() withSelected: boolean;

  /**
   * Display 'your seat' legend item.
   */
  @Input() withYourSeat: boolean;

  /**
   * Determines whether legend items should be clickable.
   */
  @Input() clickable = true;

  /**
   * Determines which legend items should be selected.
   */
  @Input() defaultSelect: CellType = CellType.AISLE;

  /**
   * On each legend value change fire new event with new selected cell type.
   */
  @Output() legendValueChange = new EventEmitter<CellType>();

  constructor() {
  }

  /**
   * Do something on the component creation.
   */
  ngOnInit(): void {
    if (!this.clickable) {
      this.defaultSelect = null;
    }
  }

  /**
   * Fires new event on each legend value change.
   * @param cellType  newly selected cell type value from legend
   */
  onValChange(cellType: CellType): void {
    this.legendValueChange.emit(cellType);
  }
}
