import { RoomPlanComponent } from '@shared/components/room-plan/room-plan.component';
import { RoomPlanLegendComponent } from '@shared/components/room-plan-legend/room-plan-legend.component';

/**
 * List of all components from this directory.
 */
export const components: any[] = [
  RoomPlanComponent,
  RoomPlanLegendComponent
];

export * from '@shared/components/room-plan/room-plan.component';
export * from '@shared/components/room-plan-legend/room-plan-legend.component';
