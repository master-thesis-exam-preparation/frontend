import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SharedModule } from '@shared/shared.module';
import { RoomPlanComponent } from '@shared/components';

describe('RoomPlanComponent', () => {
  let component: RoomPlanComponent;
  let fixture: ComponentFixture<RoomPlanComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [RoomPlanComponent],
      imports: [SharedModule]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RoomPlanComponent);
    component = fixture.componentInstance;
    component.inputCellMatrixSetter = [[]];
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
