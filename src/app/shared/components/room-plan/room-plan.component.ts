import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { CellType } from '../../../modules/main-layout/modules/rooms/cell-type.enum';
import { CellPayload } from '../../../modules/main-layout/modules/rooms/models/cell.payload';
import { SeatingPlanCell } from '../../../modules/main-layout/modules/term/models/seating-plan-cell.payload';

/**
 * Component for displaying the room plan of the room.
 */
@Component({
  selector: 'app-room-plan[inputCellMatrix]',
  templateUrl: './room-plan.component.html',
  styleUrls: ['./room-plan.component.scss']
})
export class RoomPlanComponent implements OnInit {

  /**
   * Sets cells to room plan and calculate room dimensions from given array.
   * @param value  2D list of room plan cells
   */
  @Input('inputCellMatrix') set inputCellMatrixSetter(value: SeatingPlanCell[][]) {
    this.numberOfRows = +value.length;
    this.numberOfColumns = +value[0].length;
    this.inputCellMatrix = value;
  }

  /**
   * Determines whether it is possible to click on the cells.
   * @param value  true if cell should be clickable, false otherwise
   */
  @Input('clickable') set clickableSetter(value: boolean) {
    this.clickable = value;
  }

  /**
   * Determines whether information about students will be visible on mouse hover over cell.
   */
  @Input() showHoverInfo: boolean;

  /**
   * Determines whether room plan seats should include seat numbers.
   */
  @Input() showCellNumbers: boolean;

  /**
   * Determines which room plan seats should be marked by green color as selected.
   * @param value  list of all selected seats
   */
  @Input('selectedSeats') set selectedSeatsSetter(value: CellPayload[]) {
    this.selectedSeats = value;
    const selectedSeatsId = this.selectedSeats.map(seat => seat.cellId);
    this.inputCellMatrix.forEach(row => {
      row.forEach(cell => {
        if (selectedSeatsId.includes(cell.cellId)) {
          cell.cellType = CellType.SELECTED;
        }
      });
    });
  }

  /**
   * On each change fire new event with list of selected cells.
   */
  @Output() selectedSeatsChange: EventEmitter<CellPayload[]> = new EventEmitter<CellPayload[]>();

  inputCellMatrix: SeatingPlanCell[][];
  numberOfRows: number;
  numberOfColumns: number;
  columnNumbers: number[];
  rowNumbers: number[];

  private clickable = false;
  private selectedSeats: CellPayload[] = [];

  constructor() {
  }

  /**
   * Do something on the component creation.
   */
  ngOnInit(): void {
    if (!this.inputCellMatrix) {
      throw new TypeError('The input \'cellMatrix\' is required');
    }

    this.setRowAndColumnNumbers();
  }

  /**
   * Switches cell type on cell click.
   * @param cell  the cell that was clicked on
   */
  onCellClick(cell: SeatingPlanCell): void {
    if (this.clickable || cell.cellType === CellType.SELECTED) {
      if (cell.cellType === CellType.SEAT && !cell.seatNumber) {
        cell.cellType = CellType.SELECTED;
        this.selectedSeats.push(cell);
        this.selectedSeatsChange.emit(this.selectedSeats);
      } else if (cell.cellType === CellType.SELECTED) {
        this.selectedSeats = this.selectedSeats.filter(selectedCell => selectedCell.cellId !== cell.cellId);
        cell.cellType = CellType.SEAT;
        this.selectedSeatsChange.emit(this.selectedSeats);
      }
    }
  }

  private setRowAndColumnNumbers(): void {
    // row numbers
    this.rowNumbers = this.createNumbering(
      this.inputCellMatrix.map(row =>
        row.some(cell =>
          (cell.cellType === CellType.SEAT || cell.cellType === CellType.SELECTED || cell.cellType === CellType.BLOCKED)
        )
      )
    ).reverse(); // for easier rendering

    // column numbers
    this.columnNumbers = this.createNumbering(
      this.inputCellMatrix[0].map((_, cellIndex) =>
        this.inputCellMatrix.map(row =>
          row[cellIndex]  // transpose the matrix
        )
      ).map(row =>
        row.some(cell =>
          (cell.cellType === CellType.SEAT || cell.cellType === CellType.SELECTED || cell.cellType === CellType.BLOCKED)
        )
      )
    );
  }

  private createNumbering(isSeatInArray: boolean[]): any[] {
    let currentNumber = 0;
    return isSeatInArray.map(bool => {
      if (bool) {
        currentNumber++;
        return currentNumber;
      } else {
        return '';
      }
    });
  }
}
