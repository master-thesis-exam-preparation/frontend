import { Component, OnDestroy } from '@angular/core';
import { NavigationCancel, NavigationEnd, NavigationError, NavigationStart, Router } from '@angular/router';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

/**
 * Component for displaying the loading spinner while waiting for a router.
 */
@Component({
  selector: 'app-load-spinner',
  templateUrl: './load-spinner.component.html',
  styleUrls: ['./load-spinner.component.scss']
})
export class LoadSpinnerComponent implements OnDestroy {
  private ngUnsubscribe$ = new Subject();
  public isSpinnerVisible = true;

  constructor(private router: Router) {
    this.router.events
      .pipe(takeUntil(this.ngUnsubscribe$))
      .subscribe(
        event => {
          if (event instanceof NavigationStart) {
            this.isSpinnerVisible = true;
          } else if (
            event instanceof NavigationEnd ||
            event instanceof NavigationCancel ||
            event instanceof NavigationError
          ) {
            this.isSpinnerVisible = false;
          }
        },
        () => {
          this.isSpinnerVisible = false;
        }
      );
  }

  /**
   * Unsubscribes from everything and hides spinner before destroying the component.
   */
  ngOnDestroy(): void {
    this.isSpinnerVisible = false;
    this.ngUnsubscribe$.next();
    this.ngUnsubscribe$.complete();
  }
}
