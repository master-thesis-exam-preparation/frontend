import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ThemePalette } from '@angular/material/core';

/**
 * Used to store information for confirm dialog.
 */
export interface ConfirmDialogOptionsData {
  cancelText?: string;
  cancelColor?: ThemePalette;
  confirmText: string;
  confirmColor: ThemePalette;
  message: string;
  title: string;
}

/**
 * Component for displaying the confirmation popup window.
 */
@Component({
  selector: 'app-confirm-dialog',
  templateUrl: './confirm-dialog.component.html',
  styleUrls: ['./confirm-dialog.component.scss']
})
export class ConfirmDialogComponent {

  /**
   * Creates new confirm dialog pop up window.
   * @param options  information and options to display in the dialog
   */
  constructor(@Inject(MAT_DIALOG_DATA) public options: ConfirmDialogOptionsData) {
  }

}
