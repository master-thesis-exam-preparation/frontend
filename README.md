# Application for exam preparation - frontend part

## Master Thesis

**Author**: Bc. Tomáš Líbal

**Supervisor:** Ing. Jaroslav Dytrych, Ph.D.

Brno, 2021

[Final Thesis text](https://www.vut.cz/studenti/zav-prace/detail/137577)

## Abstract
This thesis deals with the issue of preparation of final exams at the Faculty of Information
Technology of Brno University of Technology. Result of this thesis is a web application that allows teachers to create and manage room schemes
and terms of individual exams.

An important part of the application is also the automatic placement of students in the rooms and the generation of individual exam assignments
for printing based on the given template and the method of placement. 

The application provides students with a clear view of individual exam terms and details about them.


## Info
This project part was generated with [Angular CLI](https://github.com/angular/angular-cli) version 12.0.2.

The design template was taken from [WrapPixel](https://www.wrappixel.com/templates/materialpro-angular-lite/).

### Requirements
  - [Node.js](https://nodejs.org/en/) - version 12.18.2 or higher
  - npm (part of Node.js) - version 6.14.5 or higher

## How to start the application
Run `npm install` to install all required dependencies.

### Start development server
Run `ng serve` for a dev server. Navigate to http://localhost:48800/. The app will automatically reload if you change any of the source files.

### Build
Create the directory called `www` next to the `frontend` directory (next to the directory with this file).  

Run `npm run-script build` to build the project with production settings into the `www` directory.

## Running unit tests
Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Documentation
Documentation can be found inside `documentation` directory. Just open `index.html` file.

## Configuration
Application configuration can be changed in `src/environments/*` files.
  - `environment.ts` - local server configuration
  - `environment.prod.ts` - production server configuration

Please specify:
  - `backendUrl` - the URL and port of the server part of the application
